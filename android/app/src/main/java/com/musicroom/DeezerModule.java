package com.musicroom;

import android.os.Bundle;

import com.deezer.sdk.model.Permissions;
import com.deezer.sdk.model.User;
import com.deezer.sdk.network.connect.DeezerConnect;
import com.deezer.sdk.network.connect.event.DialogListener;
import com.facebook.react.bridge.Arguments;
import com.facebook.react.bridge.ReactApplicationContext;
import com.facebook.react.bridge.ReactContext;
import com.facebook.react.bridge.ReactContextBaseJavaModule;
import com.facebook.react.bridge.ReactMethod;
import com.facebook.react.bridge.WritableMap;
import com.facebook.react.modules.core.DeviceEventManagerModule;

public class DeezerModule extends ReactContextBaseJavaModule {

    private static final String applicationID = "327502";
    private static final String E_AUTH_ERROR = "E_AUTH_ERROR";

    DeezerConnect deezerConnect = new DeezerConnect(null, applicationID);

    String[] permissions = new String[] {
            Permissions.BASIC_ACCESS,
            Permissions.MANAGE_LIBRARY,
            Permissions.LISTENING_HISTORY };

    // The listener for authentication events
    DialogListener listener = new DialogListener() {
        public void onComplete(Bundle values) {
            User user = deezerConnect.getCurrentUser();
            WritableMap result = Arguments.createMap();

            String birthday = user.getBirthday().toString();
            String nickname = user.getName();
            String firstName = user.getFirstName();
            String lastName = user.getLastName();
            String profileLink = user.getLink();
            Long userId = user.getId();

            result.putString("UserId", userId.toString());
            result.putString("Birthday", birthday);
            result.putString("Nickname", nickname);
            result.putString("FirstName", firstName);
            result.putString("LastName", lastName);
            result.putString("Link", profileLink);

            ReactContext context = getReactApplicationContext();
            context.getJSModule(DeviceEventManagerModule.RCTDeviceEventEmitter.class).emit(
                "deezerLoginFinished",
                result
            );
        }

        public void onCancel() {}

        public void onException(Exception e) {}
    };

    public DeezerModule(ReactApplicationContext reactContext) { super(reactContext); }

    @Override
    public String getName() {
        return "DeezerModule";
    }

    @ReactMethod
    public void authorize() {
        deezerConnect.authorize(getCurrentActivity(), permissions, listener);
    }
}
