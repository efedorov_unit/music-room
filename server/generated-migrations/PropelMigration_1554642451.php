<?php

use Propel\Generator\Manager\MigrationManager;

/**
 * Data object containing the SQL and PHP code to migrate the database
 * up to version 1554642451.
 * Generated on 2019-04-07 13:07:31 by efedorov
 */
class PropelMigration_1554642451
{
    public $comment = '';

    public function preUp(MigrationManager $manager)
    {
        // add the pre-migration code here
    }

    public function postUp(MigrationManager $manager)
    {
        // add the post-migration code here
    }

    public function preDown(MigrationManager $manager)
    {
        // add the pre-migration code here
    }

    public function postDown(MigrationManager $manager)
    {
        // add the post-migration code here
    }

    /**
     * Get the SQL statements for the Up migration
     *
     * @return array list of the SQL strings to execute for the Up migration
     *               the keys being the datasources
     */
    public function getUpSQL()
    {
        return array (
  'engine' => '
# This is a fix for InnoDB in MySQL >= 4.1.x
# It "suspends judgement" for fkey relationships until are tables are set.
SET FOREIGN_KEY_CHECKS = 0;

CREATE TABLE `users`
(
    `id` INTEGER(10) NOT NULL AUTO_INCREMENT,
    `nickname` VARCHAR(128) DEFAULT \'\' NOT NULL,
    `email` VARCHAR(128) DEFAULT \'\' NOT NULL,
    `password` VARCHAR(64) DEFAULT \'\' NOT NULL,
    `created_at` DATETIME,
    `updated_at` DATETIME,
    PRIMARY KEY (`id`),
    UNIQUE INDEX `email` (`email`)
) ENGINE=InnoDB CHARACTER SET=\'utf8\';

CREATE TABLE `facebook_accounts`
(
    `id` INTEGER(10) NOT NULL AUTO_INCREMENT,
    `user_id` int(11) unsigned NOT NULL,
    `nickname` VARCHAR(128) DEFAULT \'\' NOT NULL,
    `first_name` VARCHAR(128),
    `last_name` VARCHAR(128),
    `email` VARCHAR(128) DEFAULT \'\' NOT NULL,
    `birthday` DATETIME NOT NULL,
    `link` VARCHAR(255) DEFAULT \'\',
    `created_at` DATETIME,
    `updated_at` DATETIME,
    PRIMARY KEY (`id`,`user_id`),
    INDEX `facebook_accounts_fi_69bd79` (`user_id`)
) ENGINE=InnoDB CHARACTER SET=\'utf8\';

CREATE TABLE `deezer_accounts`
(
    `id` INTEGER(10) NOT NULL AUTO_INCREMENT,
    `user_id` int(11) unsigned NOT NULL,
    `nickname` VARCHAR(128) DEFAULT \'\' NOT NULL,
    `first_name` VARCHAR(128),
    `last_name` VARCHAR(128),
    `email` VARCHAR(128) DEFAULT \'\' NOT NULL,
    `birthday` DATETIME NOT NULL,
    `link` VARCHAR(255) DEFAULT \'\',
    `created_at` DATETIME,
    `updated_at` DATETIME,
    PRIMARY KEY (`id`,`user_id`),
    INDEX `deezer_accounts_fi_69bd79` (`user_id`)
) ENGINE=InnoDB CHARACTER SET=\'utf8\';

# This restores the fkey checks, after having unset them earlier
SET FOREIGN_KEY_CHECKS = 1;
',
);
    }

    /**
     * Get the SQL statements for the Down migration
     *
     * @return array list of the SQL strings to execute for the Down migration
     *               the keys being the datasources
     */
    public function getDownSQL()
    {
        return array (
  'engine' => '
# This is a fix for InnoDB in MySQL >= 4.1.x
# It "suspends judgement" for fkey relationships until are tables are set.
SET FOREIGN_KEY_CHECKS = 0;

DROP TABLE IF EXISTS `users`;

DROP TABLE IF EXISTS `facebook_accounts`;

DROP TABLE IF EXISTS `deezer_accounts`;

# This restores the fkey checks, after having unset them earlier
SET FOREIGN_KEY_CHECKS = 1;
',
);
    }

}