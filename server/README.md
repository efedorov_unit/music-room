## Install project ##

1. Install libs

```bash
    composer install
````

2. Create config

```bash
    cp etc/app-conf.php.sample etc/app-conf.php
    cp etc/propel.php.sample etc/propel.php
```

3. Run project

```bash
    php -S 0.0.0.0:8000 -t public/
```

4. Open 'http://localhost:8000' on your web browser


------------------------------------------------------------------


## Propel configuration ##

1. Create propel schema.xml

```bash
    php vendor/bin/propel database:reverse --output-dir deploy 'mysql:host=<HOST>;dbname=<DBNAME>;user=<USER>;password=<PASS>'
```

2. Add model namespace if need. Open schema.xml config and add namespace="<NAMESPACE>" attribute to the database tag

3. Build model

```bash
    php vendor/bin/propel model:build --schema-dir ./etc --output-dir ./lib --config-dir ./etc -vv
```

4. Build sql schema from xml schema (if need)

```bash
    ./vendor/bin/propel sql:build --config-dir ./etc --schema-dir ./etc
```

5. Create migration file

```bash
    ./vendor/bin/propel diff --config-dir ./etc --schema-dir ./etc
```

6. Execute all migration files

```bash
    ./vendor/bin/propel migrate --config-dir ./etc
```

7. Rollback migration

```bash
    ./vendor/bin/propel migration:down --config-dir ./etc
```
