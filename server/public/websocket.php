<?php
use Ratchet\Server\IoServer;
use Service\SocketRoom\Room;
use Ratchet\Http\HttpServer;
use Ratchet\WebSocket\WsServer;

require dirname(__DIR__) . '/vendor/autoload.php';

$server = IoServer::factory(
    new HttpServer(
        new WsServer(
            new Room()
        )
    ),
    8080
);

$server->run();
