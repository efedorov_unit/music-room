<?php

namespace Controller\API;

use Psr\Http\Message\RequestInterface as ReqInt;
use Psr\Http\Message\ResponseInterface as ResInt;

class User extends \Controller\Base
{
    public function index(ReqInt $req, ResInt $res, array $args) : ResInt
    {
        $self = $this;

        return $this->run(function () use ($self) {
            return $self->action('\Service\User\Index')->run([]);
        }, $res);
    }

    public function create(ReqInt $req, ResInt $res, array $args) : ResInt
    {
        $self = $this;
        $data = $req->getParsedBody();

        return $this->run(function () use ($self, $data) {
            return $self->action('\Service\User\Create')->run($data);
        }, $res);
    }

    public function createFb(ReqInt $req, ResInt $res, array $args) : ResInt
    {
        $self = $this;
        $data = $req->getParsedBody();

        return $this->run(function () use ($self, $data) {
            return $self->action('\Service\User\CreateFb')->run($data);
        }, $res);
    }

    public function musicPrefs(ReqInt $req, ResInt $res, array $args) : ResInt
    {
        $self = $this;
        $data = $req->getParsedBody();

        return $this->run(function () use ($self, $data) {
            return $self->action('\Service\User\MusicPrefs')->run($data);
        }, $res);
    }

    public function createDeezer(ReqInt $req, ResInt $res, array $args) : ResInt
    {
        $self = $this;
        $data = $req->getParsedBody();

        return $this->run(function () use ($self, $data) {
            return $self->action('\Service\User\CreateDeezer')->run($data);
        }, $res);
    }

    public function linkWithFb(ReqInt $req, ResInt $res, array $args) : ResInt
    {
        $self = $this;
        $data = $req->getParsedBody();

        return $this->run(function () use ($self, $data) {
            return $self->action('\Service\User\LinkWithFb')->run($data);
        }, $res);
    }

    public function changePass(ReqInt $req, ResInt $res, array $args) : ResInt
    {
        $self = $this;
        $data = $req->getParsedBody();

        return $this->run(function () use ($self, $data) {
            return $self->action('\Service\User\ChangePass')->run($data);
        }, $res);
    }

    public function getUsersList(ReqInt $req, ResInt $res, array $args) : ResInt
    {
        $self = $this;

        return $this->run(function () use ($self) {
            return $self->action('\Service\User\GetUsersList')->run([]);
        }, $res);
    }
}
