<?php

namespace Controller\API;

use Psr\Http\Message\RequestInterface as ReqInt;
use Psr\Http\Message\ResponseInterface as ResInt;

class MusicRoom extends \Controller\Base
{
    public function index(ReqInt $req, ResInt $res, array $args) : ResInt
    {
        $self = $this;

        return $this->run(function () use ($self) {
            return $self->action('\Service\MusicRoom\Index')->run([]);
        }, $res);
    }

    public function showRooms(ReqInt $req, ResInt $res, array $args) : ResInt
    {
        $self = $this;

        return $this->run(function () use ($self) {
            return $self->action('\Service\MusicRoom\SelectRoom')->run([]);
        }, $res);
    }

    public function show(ReqInt $req, ResInt $res, array $args) : ResInt
    {
        $self = $this;
        $data = $req->getQueryParams();

        return $this->run(function () use ($self, $data) {
            return $self->action('\Service\MusicRoom\Show')->run($data);
        }, $res);
    }

    public function create(ReqInt $req, ResInt $res, array $args) : ResInt
    {
        $self = $this;
        $data = $req->getParsedBody();

        return $this->run(function () use ($self, $data) {
            return $self->action('\Service\MusicRoom\Create')->run($data);
        }, $res);
    }

    public function update(ReqInt $req, ResInt $res, array $args) : ResInt
    {
        $self = $this;
        $data = $req->getParsedBody();

        return $this->run(function () use ($self, $data) {
            return $self->action('\Service\MusicRoom\Update')->run($data);
        }, $res);
    }

    public function addTrack(ReqInt $req, ResInt $res, array $args) : ResInt
    {
        $self = $this;
        $data = $req->getParsedBody();

        return $this->run(function () use ($self, $data) {
            return $self->action('\Service\MusicRoom\AddTrack')->run($data);
        }, $res);
    }

    public function remove(ReqInt $req, ResInt $res, array $args) : ResInt
    {
        $self = $this;
        $data = $req->getParsedBody();

        return $this->run(function () use ($self, $data) {
            return $self->action('\Service\MusicRoom\Remove')->run($data);
        }, $res);
    }
}
