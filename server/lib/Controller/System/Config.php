<?php

namespace Controller\System;

use Controller\Base;
use Psr\Http\Message\RequestInterface as ReqInt;
use Psr\Http\Message\ResponseInterface as ResInt;

class Config extends Base
{
    public function index(ReqInt $req, ResInt $res, array $args)
    {
        $self = $this;

        return $this->run(function () use ($self) {
            return $self->action('\Service\System\Config\Index')->run([]);
        }, $res);
    }
}
