<?php

namespace Engine;

trait Utils
{
    public static $totalCount = 0;
    public static $filteredCount = 0;

    /**
     * Object searchByFields($fields, $search) - get searchable object by fields
     * @param array  $fields - filter fields
     * @param string $search - search query
     *
     * @return $this
     */
    public function searchByFields($fields, $search)
    {
        if ($search === '' || !$fields) {
            return $this;
        }

        for ($i=0; $i < count($fields); $i++) {
            $filter = 'filterBy'.$fields[$i];
            $this->$filter("%$search%");

            if ($i < count($fields) - 1) {
                $this->_or();
            }
        }

        return $this;
    }

    /**
     * totalCount() - count objects before filtering
     * @return $this
     */
    public function totalCount()
    {
        self::$totalCount = $this->count();

        return $this;
    }

    /**
     * filteredCount() - count objects after filtering
     * @return $this
     */
    public function filteredCount()
    {
        self::$filteredCount = $this->count();

        return $this;
    }

    /**
     * limitIfPositive() - apply limit to query if limit is positive number
     * @param $limit
     *
     * @return $this
     */
    public function limitIfPositive($limit)
    {
        if ($limit > 0) {
            $this->limit($limit);
        }

        return $this;
    }
}
