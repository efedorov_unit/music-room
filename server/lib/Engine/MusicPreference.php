<?php

namespace Engine;

use Engine\Base\MusicPreference as BaseMusicPreference;

/**
 * Skeleton subclass for representing a row from the 'music_preferences' table.
 *
 *
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 *
 */
class MusicPreference extends BaseMusicPreference
{

}
