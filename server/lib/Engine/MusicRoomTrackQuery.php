<?php

namespace Engine;

use Engine\Base\MusicRoomTrackQuery as BaseMusicRoomTrackQuery;

/**
 * Skeleton subclass for performing query and update operations on the 'music_room_tracks' table.
 *
 *
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 *
 */
class MusicRoomTrackQuery extends BaseMusicRoomTrackQuery
{
    public function withFields()
    {
        return $this
            ->withColumn('MusicRoomTrack.TrackId', 'TrackId')
            ->withColumn('MusicRoomTrack.Link', 'Link')
            ->withColumn('MusicRoomTrack.Preview', 'Preview')
            ->withColumn('MusicRoomTrack.Title', 'Title')
            ->withColumn('MusicRoomTrack.Image', 'Image');
    }
}
