<?php

namespace Engine;

use Engine\Base\MusicRoom as BaseMusicRoom;

/**
 * Skeleton subclass for representing a row from the 'music_rooms' table.
 *
 *
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 *
 */
class MusicRoom extends BaseMusicRoom
{

}
