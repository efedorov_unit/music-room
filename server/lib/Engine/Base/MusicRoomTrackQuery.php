<?php

namespace Engine\Base;

use \Exception;
use \PDO;
use Engine\MusicRoomTrack as ChildMusicRoomTrack;
use Engine\MusicRoomTrackQuery as ChildMusicRoomTrackQuery;
use Engine\Map\MusicRoomTrackTableMap;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\ActiveQuery\ModelJoin;
use Propel\Runtime\Collection\ObjectCollection;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\Exception\PropelException;

/**
 * Base class that represents a query for the 'music_room_tracks' table.
 *
 *
 *
 * @method     ChildMusicRoomTrackQuery orderByRoomId($order = Criteria::ASC) Order by the music_room_id column
 * @method     ChildMusicRoomTrackQuery orderByTrackId($order = Criteria::ASC) Order by the track_id column
 * @method     ChildMusicRoomTrackQuery orderByLink($order = Criteria::ASC) Order by the link column
 * @method     ChildMusicRoomTrackQuery orderByPreview($order = Criteria::ASC) Order by the preview column
 * @method     ChildMusicRoomTrackQuery orderByTitle($order = Criteria::ASC) Order by the title column
 * @method     ChildMusicRoomTrackQuery orderByImage($order = Criteria::ASC) Order by the image column
 *
 * @method     ChildMusicRoomTrackQuery groupByRoomId() Group by the music_room_id column
 * @method     ChildMusicRoomTrackQuery groupByTrackId() Group by the track_id column
 * @method     ChildMusicRoomTrackQuery groupByLink() Group by the link column
 * @method     ChildMusicRoomTrackQuery groupByPreview() Group by the preview column
 * @method     ChildMusicRoomTrackQuery groupByTitle() Group by the title column
 * @method     ChildMusicRoomTrackQuery groupByImage() Group by the image column
 *
 * @method     ChildMusicRoomTrackQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method     ChildMusicRoomTrackQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method     ChildMusicRoomTrackQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method     ChildMusicRoomTrackQuery leftJoinWith($relation) Adds a LEFT JOIN clause and with to the query
 * @method     ChildMusicRoomTrackQuery rightJoinWith($relation) Adds a RIGHT JOIN clause and with to the query
 * @method     ChildMusicRoomTrackQuery innerJoinWith($relation) Adds a INNER JOIN clause and with to the query
 *
 * @method     ChildMusicRoomTrackQuery leftJoinMusicRoom($relationAlias = null) Adds a LEFT JOIN clause to the query using the MusicRoom relation
 * @method     ChildMusicRoomTrackQuery rightJoinMusicRoom($relationAlias = null) Adds a RIGHT JOIN clause to the query using the MusicRoom relation
 * @method     ChildMusicRoomTrackQuery innerJoinMusicRoom($relationAlias = null) Adds a INNER JOIN clause to the query using the MusicRoom relation
 *
 * @method     ChildMusicRoomTrackQuery joinWithMusicRoom($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the MusicRoom relation
 *
 * @method     ChildMusicRoomTrackQuery leftJoinWithMusicRoom() Adds a LEFT JOIN clause and with to the query using the MusicRoom relation
 * @method     ChildMusicRoomTrackQuery rightJoinWithMusicRoom() Adds a RIGHT JOIN clause and with to the query using the MusicRoom relation
 * @method     ChildMusicRoomTrackQuery innerJoinWithMusicRoom() Adds a INNER JOIN clause and with to the query using the MusicRoom relation
 *
 * @method     \Engine\MusicRoomQuery endUse() Finalizes a secondary criteria and merges it with its primary Criteria
 *
 * @method     ChildMusicRoomTrack findOne(ConnectionInterface $con = null) Return the first ChildMusicRoomTrack matching the query
 * @method     ChildMusicRoomTrack findOneOrCreate(ConnectionInterface $con = null) Return the first ChildMusicRoomTrack matching the query, or a new ChildMusicRoomTrack object populated from the query conditions when no match is found
 *
 * @method     ChildMusicRoomTrack findOneByRoomId(int $music_room_id) Return the first ChildMusicRoomTrack filtered by the music_room_id column
 * @method     ChildMusicRoomTrack findOneByTrackId(int $track_id) Return the first ChildMusicRoomTrack filtered by the track_id column
 * @method     ChildMusicRoomTrack findOneByLink(string $link) Return the first ChildMusicRoomTrack filtered by the link column
 * @method     ChildMusicRoomTrack findOneByPreview(string $preview) Return the first ChildMusicRoomTrack filtered by the preview column
 * @method     ChildMusicRoomTrack findOneByTitle(string $title) Return the first ChildMusicRoomTrack filtered by the title column
 * @method     ChildMusicRoomTrack findOneByImage(string $image) Return the first ChildMusicRoomTrack filtered by the image column *

 * @method     ChildMusicRoomTrack requirePk($key, ConnectionInterface $con = null) Return the ChildMusicRoomTrack by primary key and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildMusicRoomTrack requireOne(ConnectionInterface $con = null) Return the first ChildMusicRoomTrack matching the query and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildMusicRoomTrack requireOneByRoomId(int $music_room_id) Return the first ChildMusicRoomTrack filtered by the music_room_id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildMusicRoomTrack requireOneByTrackId(int $track_id) Return the first ChildMusicRoomTrack filtered by the track_id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildMusicRoomTrack requireOneByLink(string $link) Return the first ChildMusicRoomTrack filtered by the link column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildMusicRoomTrack requireOneByPreview(string $preview) Return the first ChildMusicRoomTrack filtered by the preview column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildMusicRoomTrack requireOneByTitle(string $title) Return the first ChildMusicRoomTrack filtered by the title column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildMusicRoomTrack requireOneByImage(string $image) Return the first ChildMusicRoomTrack filtered by the image column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildMusicRoomTrack[]|ObjectCollection find(ConnectionInterface $con = null) Return ChildMusicRoomTrack objects based on current ModelCriteria
 * @method     ChildMusicRoomTrack[]|ObjectCollection findByRoomId(int $music_room_id) Return ChildMusicRoomTrack objects filtered by the music_room_id column
 * @method     ChildMusicRoomTrack[]|ObjectCollection findByTrackId(int $track_id) Return ChildMusicRoomTrack objects filtered by the track_id column
 * @method     ChildMusicRoomTrack[]|ObjectCollection findByLink(string $link) Return ChildMusicRoomTrack objects filtered by the link column
 * @method     ChildMusicRoomTrack[]|ObjectCollection findByPreview(string $preview) Return ChildMusicRoomTrack objects filtered by the preview column
 * @method     ChildMusicRoomTrack[]|ObjectCollection findByTitle(string $title) Return ChildMusicRoomTrack objects filtered by the title column
 * @method     ChildMusicRoomTrack[]|ObjectCollection findByImage(string $image) Return ChildMusicRoomTrack objects filtered by the image column
 * @method     ChildMusicRoomTrack[]|\Propel\Runtime\Util\PropelModelPager paginate($page = 1, $maxPerPage = 10, ConnectionInterface $con = null) Issue a SELECT query based on the current ModelCriteria and uses a page and a maximum number of results per page to compute an offset and a limit
 *
 */
abstract class MusicRoomTrackQuery extends ModelCriteria
{
    protected $entityNotFoundExceptionClass = '\\Propel\\Runtime\\Exception\\EntityNotFoundException';

    /**
     * Initializes internal state of \Engine\Base\MusicRoomTrackQuery object.
     *
     * @param     string $dbName The database name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = 'engine', $modelName = '\\Engine\\MusicRoomTrack', $modelAlias = null)
    {
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new ChildMusicRoomTrackQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param     Criteria $criteria Optional Criteria to build the query from
     *
     * @return ChildMusicRoomTrackQuery
     */
    public static function create($modelAlias = null, Criteria $criteria = null)
    {
        if ($criteria instanceof ChildMusicRoomTrackQuery) {
            return $criteria;
        }
        $query = new ChildMusicRoomTrackQuery();
        if (null !== $modelAlias) {
            $query->setModelAlias($modelAlias);
        }
        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj = $c->findPk(array(12, 34), $con);
     * </code>
     *
     * @param array[$music_room_id, $track_id] $key Primary key to use for the query
     * @param ConnectionInterface $con an optional connection object
     *
     * @return ChildMusicRoomTrack|array|mixed the result, formatted by the current formatter
     */
    public function findPk($key, ConnectionInterface $con = null)
    {
        if ($key === null) {
            return null;
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getReadConnection(MusicRoomTrackTableMap::DATABASE_NAME);
        }

        $this->basePreSelect($con);

        if (
            $this->formatter || $this->modelAlias || $this->with || $this->select
            || $this->selectColumns || $this->asColumns || $this->selectModifiers
            || $this->map || $this->having || $this->joins
        ) {
            return $this->findPkComplex($key, $con);
        }

        if ((null !== ($obj = MusicRoomTrackTableMap::getInstanceFromPool(serialize([(null === $key[0] || is_scalar($key[0]) || is_callable([$key[0], '__toString']) ? (string) $key[0] : $key[0]), (null === $key[1] || is_scalar($key[1]) || is_callable([$key[1], '__toString']) ? (string) $key[1] : $key[1])]))))) {
            // the object is already in the instance pool
            return $obj;
        }

        return $this->findPkSimple($key, $con);
    }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildMusicRoomTrack A model object, or null if the key is not found
     */
    protected function findPkSimple($key, ConnectionInterface $con)
    {
        $sql = 'SELECT music_room_id, track_id, link, preview, title, image FROM music_room_tracks WHERE music_room_id = :p0 AND track_id = :p1';
        try {
            $stmt = $con->prepare($sql);
            $stmt->bindValue(':p0', $key[0], PDO::PARAM_INT);
            $stmt->bindValue(':p1', $key[1], PDO::PARAM_INT);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), 0, $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(\PDO::FETCH_NUM)) {
            /** @var ChildMusicRoomTrack $obj */
            $obj = new ChildMusicRoomTrack();
            $obj->hydrate($row);
            MusicRoomTrackTableMap::addInstanceToPool($obj, serialize([(null === $key[0] || is_scalar($key[0]) || is_callable([$key[0], '__toString']) ? (string) $key[0] : $key[0]), (null === $key[1] || is_scalar($key[1]) || is_callable([$key[1], '__toString']) ? (string) $key[1] : $key[1])]));
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @return ChildMusicRoomTrack|array|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, ConnectionInterface $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($dataFetcher);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(array(12, 56), array(832, 123), array(123, 456)), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     ConnectionInterface $con an optional connection object
     *
     * @return ObjectCollection|array|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getReadConnection($this->getDbName());
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($dataFetcher);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return $this|ChildMusicRoomTrackQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {
        $this->addUsingAlias(MusicRoomTrackTableMap::COL_MUSIC_ROOM_ID, $key[0], Criteria::EQUAL);
        $this->addUsingAlias(MusicRoomTrackTableMap::COL_TRACK_ID, $key[1], Criteria::EQUAL);

        return $this;
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return $this|ChildMusicRoomTrackQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {
        if (empty($keys)) {
            return $this->add(null, '1<>1', Criteria::CUSTOM);
        }
        foreach ($keys as $key) {
            $cton0 = $this->getNewCriterion(MusicRoomTrackTableMap::COL_MUSIC_ROOM_ID, $key[0], Criteria::EQUAL);
            $cton1 = $this->getNewCriterion(MusicRoomTrackTableMap::COL_TRACK_ID, $key[1], Criteria::EQUAL);
            $cton0->addAnd($cton1);
            $this->addOr($cton0);
        }

        return $this;
    }

    /**
     * Filter the query on the music_room_id column
     *
     * Example usage:
     * <code>
     * $query->filterByRoomId(1234); // WHERE music_room_id = 1234
     * $query->filterByRoomId(array(12, 34)); // WHERE music_room_id IN (12, 34)
     * $query->filterByRoomId(array('min' => 12)); // WHERE music_room_id > 12
     * </code>
     *
     * @see       filterByMusicRoom()
     *
     * @param     mixed $roomId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildMusicRoomTrackQuery The current query, for fluid interface
     */
    public function filterByRoomId($roomId = null, $comparison = null)
    {
        if (is_array($roomId)) {
            $useMinMax = false;
            if (isset($roomId['min'])) {
                $this->addUsingAlias(MusicRoomTrackTableMap::COL_MUSIC_ROOM_ID, $roomId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($roomId['max'])) {
                $this->addUsingAlias(MusicRoomTrackTableMap::COL_MUSIC_ROOM_ID, $roomId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(MusicRoomTrackTableMap::COL_MUSIC_ROOM_ID, $roomId, $comparison);
    }

    /**
     * Filter the query on the track_id column
     *
     * Example usage:
     * <code>
     * $query->filterByTrackId(1234); // WHERE track_id = 1234
     * $query->filterByTrackId(array(12, 34)); // WHERE track_id IN (12, 34)
     * $query->filterByTrackId(array('min' => 12)); // WHERE track_id > 12
     * </code>
     *
     * @param     mixed $trackId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildMusicRoomTrackQuery The current query, for fluid interface
     */
    public function filterByTrackId($trackId = null, $comparison = null)
    {
        if (is_array($trackId)) {
            $useMinMax = false;
            if (isset($trackId['min'])) {
                $this->addUsingAlias(MusicRoomTrackTableMap::COL_TRACK_ID, $trackId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($trackId['max'])) {
                $this->addUsingAlias(MusicRoomTrackTableMap::COL_TRACK_ID, $trackId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(MusicRoomTrackTableMap::COL_TRACK_ID, $trackId, $comparison);
    }

    /**
     * Filter the query on the link column
     *
     * Example usage:
     * <code>
     * $query->filterByLink('fooValue');   // WHERE link = 'fooValue'
     * $query->filterByLink('%fooValue%', Criteria::LIKE); // WHERE link LIKE '%fooValue%'
     * </code>
     *
     * @param     string $link The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildMusicRoomTrackQuery The current query, for fluid interface
     */
    public function filterByLink($link = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($link)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(MusicRoomTrackTableMap::COL_LINK, $link, $comparison);
    }

    /**
     * Filter the query on the preview column
     *
     * Example usage:
     * <code>
     * $query->filterByPreview('fooValue');   // WHERE preview = 'fooValue'
     * $query->filterByPreview('%fooValue%', Criteria::LIKE); // WHERE preview LIKE '%fooValue%'
     * </code>
     *
     * @param     string $preview The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildMusicRoomTrackQuery The current query, for fluid interface
     */
    public function filterByPreview($preview = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($preview)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(MusicRoomTrackTableMap::COL_PREVIEW, $preview, $comparison);
    }

    /**
     * Filter the query on the title column
     *
     * Example usage:
     * <code>
     * $query->filterByTitle('fooValue');   // WHERE title = 'fooValue'
     * $query->filterByTitle('%fooValue%', Criteria::LIKE); // WHERE title LIKE '%fooValue%'
     * </code>
     *
     * @param     string $title The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildMusicRoomTrackQuery The current query, for fluid interface
     */
    public function filterByTitle($title = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($title)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(MusicRoomTrackTableMap::COL_TITLE, $title, $comparison);
    }

    /**
     * Filter the query on the image column
     *
     * Example usage:
     * <code>
     * $query->filterByImage('fooValue');   // WHERE image = 'fooValue'
     * $query->filterByImage('%fooValue%', Criteria::LIKE); // WHERE image LIKE '%fooValue%'
     * </code>
     *
     * @param     string $image The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildMusicRoomTrackQuery The current query, for fluid interface
     */
    public function filterByImage($image = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($image)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(MusicRoomTrackTableMap::COL_IMAGE, $image, $comparison);
    }

    /**
     * Filter the query by a related \Engine\MusicRoom object
     *
     * @param \Engine\MusicRoom|ObjectCollection $musicRoom The related object(s) to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildMusicRoomTrackQuery The current query, for fluid interface
     */
    public function filterByMusicRoom($musicRoom, $comparison = null)
    {
        if ($musicRoom instanceof \Engine\MusicRoom) {
            return $this
                ->addUsingAlias(MusicRoomTrackTableMap::COL_MUSIC_ROOM_ID, $musicRoom->getId(), $comparison);
        } elseif ($musicRoom instanceof ObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(MusicRoomTrackTableMap::COL_MUSIC_ROOM_ID, $musicRoom->toKeyValue('PrimaryKey', 'Id'), $comparison);
        } else {
            throw new PropelException('filterByMusicRoom() only accepts arguments of type \Engine\MusicRoom or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the MusicRoom relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildMusicRoomTrackQuery The current query, for fluid interface
     */
    public function joinMusicRoom($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('MusicRoom');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'MusicRoom');
        }

        return $this;
    }

    /**
     * Use the MusicRoom relation MusicRoom object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \Engine\MusicRoomQuery A secondary query class using the current class as primary query
     */
    public function useMusicRoomQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinMusicRoom($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'MusicRoom', '\Engine\MusicRoomQuery');
    }

    /**
     * Exclude object from result
     *
     * @param   ChildMusicRoomTrack $musicRoomTrack Object to remove from the list of results
     *
     * @return $this|ChildMusicRoomTrackQuery The current query, for fluid interface
     */
    public function prune($musicRoomTrack = null)
    {
        if ($musicRoomTrack) {
            $this->addCond('pruneCond0', $this->getAliasedColName(MusicRoomTrackTableMap::COL_MUSIC_ROOM_ID), $musicRoomTrack->getRoomId(), Criteria::NOT_EQUAL);
            $this->addCond('pruneCond1', $this->getAliasedColName(MusicRoomTrackTableMap::COL_TRACK_ID), $musicRoomTrack->getTrackId(), Criteria::NOT_EQUAL);
            $this->combine(array('pruneCond0', 'pruneCond1'), Criteria::LOGICAL_OR);
        }

        return $this;
    }

    /**
     * Deletes all rows from the music_room_tracks table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public function doDeleteAll(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(MusicRoomTrackTableMap::DATABASE_NAME);
        }

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con) {
            $affectedRows = 0; // initialize var to track total num of affected rows
            $affectedRows += parent::doDeleteAll($con);
            // Because this db requires some delete cascade/set null emulation, we have to
            // clear the cached instance *after* the emulation has happened (since
            // instances get re-added by the select statement contained therein).
            MusicRoomTrackTableMap::clearInstancePool();
            MusicRoomTrackTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

    /**
     * Performs a DELETE on the database based on the current ModelCriteria
     *
     * @param ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public function delete(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(MusicRoomTrackTableMap::DATABASE_NAME);
        }

        $criteria = $this;

        // Set the correct dbName
        $criteria->setDbName(MusicRoomTrackTableMap::DATABASE_NAME);

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con, $criteria) {
            $affectedRows = 0; // initialize var to track total num of affected rows

            MusicRoomTrackTableMap::removeInstanceFromPool($criteria);

            $affectedRows += ModelCriteria::delete($con);
            MusicRoomTrackTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

} // MusicRoomTrackQuery
