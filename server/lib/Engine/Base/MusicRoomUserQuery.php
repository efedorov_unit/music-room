<?php

namespace Engine\Base;

use \Exception;
use \PDO;
use Engine\MusicRoomUser as ChildMusicRoomUser;
use Engine\MusicRoomUserQuery as ChildMusicRoomUserQuery;
use Engine\Map\MusicRoomUserTableMap;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\ActiveQuery\ModelJoin;
use Propel\Runtime\Collection\ObjectCollection;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\Exception\PropelException;

/**
 * Base class that represents a query for the 'music_room_users' table.
 *
 *
 *
 * @method     ChildMusicRoomUserQuery orderByMusicRoomId($order = Criteria::ASC) Order by the music_room_id column
 * @method     ChildMusicRoomUserQuery orderByUserId($order = Criteria::ASC) Order by the user_id column
 * @method     ChildMusicRoomUserQuery orderByOwner($order = Criteria::ASC) Order by the owner column
 *
 * @method     ChildMusicRoomUserQuery groupByMusicRoomId() Group by the music_room_id column
 * @method     ChildMusicRoomUserQuery groupByUserId() Group by the user_id column
 * @method     ChildMusicRoomUserQuery groupByOwner() Group by the owner column
 *
 * @method     ChildMusicRoomUserQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method     ChildMusicRoomUserQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method     ChildMusicRoomUserQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method     ChildMusicRoomUserQuery leftJoinWith($relation) Adds a LEFT JOIN clause and with to the query
 * @method     ChildMusicRoomUserQuery rightJoinWith($relation) Adds a RIGHT JOIN clause and with to the query
 * @method     ChildMusicRoomUserQuery innerJoinWith($relation) Adds a INNER JOIN clause and with to the query
 *
 * @method     ChildMusicRoomUserQuery leftJoinMusicRoom($relationAlias = null) Adds a LEFT JOIN clause to the query using the MusicRoom relation
 * @method     ChildMusicRoomUserQuery rightJoinMusicRoom($relationAlias = null) Adds a RIGHT JOIN clause to the query using the MusicRoom relation
 * @method     ChildMusicRoomUserQuery innerJoinMusicRoom($relationAlias = null) Adds a INNER JOIN clause to the query using the MusicRoom relation
 *
 * @method     ChildMusicRoomUserQuery joinWithMusicRoom($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the MusicRoom relation
 *
 * @method     ChildMusicRoomUserQuery leftJoinWithMusicRoom() Adds a LEFT JOIN clause and with to the query using the MusicRoom relation
 * @method     ChildMusicRoomUserQuery rightJoinWithMusicRoom() Adds a RIGHT JOIN clause and with to the query using the MusicRoom relation
 * @method     ChildMusicRoomUserQuery innerJoinWithMusicRoom() Adds a INNER JOIN clause and with to the query using the MusicRoom relation
 *
 * @method     ChildMusicRoomUserQuery leftJoinUser($relationAlias = null) Adds a LEFT JOIN clause to the query using the User relation
 * @method     ChildMusicRoomUserQuery rightJoinUser($relationAlias = null) Adds a RIGHT JOIN clause to the query using the User relation
 * @method     ChildMusicRoomUserQuery innerJoinUser($relationAlias = null) Adds a INNER JOIN clause to the query using the User relation
 *
 * @method     ChildMusicRoomUserQuery joinWithUser($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the User relation
 *
 * @method     ChildMusicRoomUserQuery leftJoinWithUser() Adds a LEFT JOIN clause and with to the query using the User relation
 * @method     ChildMusicRoomUserQuery rightJoinWithUser() Adds a RIGHT JOIN clause and with to the query using the User relation
 * @method     ChildMusicRoomUserQuery innerJoinWithUser() Adds a INNER JOIN clause and with to the query using the User relation
 *
 * @method     \Engine\MusicRoomQuery|\Engine\UserQuery endUse() Finalizes a secondary criteria and merges it with its primary Criteria
 *
 * @method     ChildMusicRoomUser findOne(ConnectionInterface $con = null) Return the first ChildMusicRoomUser matching the query
 * @method     ChildMusicRoomUser findOneOrCreate(ConnectionInterface $con = null) Return the first ChildMusicRoomUser matching the query, or a new ChildMusicRoomUser object populated from the query conditions when no match is found
 *
 * @method     ChildMusicRoomUser findOneByMusicRoomId(int $music_room_id) Return the first ChildMusicRoomUser filtered by the music_room_id column
 * @method     ChildMusicRoomUser findOneByUserId(int $user_id) Return the first ChildMusicRoomUser filtered by the user_id column
 * @method     ChildMusicRoomUser findOneByOwner(int $owner) Return the first ChildMusicRoomUser filtered by the owner column *

 * @method     ChildMusicRoomUser requirePk($key, ConnectionInterface $con = null) Return the ChildMusicRoomUser by primary key and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildMusicRoomUser requireOne(ConnectionInterface $con = null) Return the first ChildMusicRoomUser matching the query and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildMusicRoomUser requireOneByMusicRoomId(int $music_room_id) Return the first ChildMusicRoomUser filtered by the music_room_id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildMusicRoomUser requireOneByUserId(int $user_id) Return the first ChildMusicRoomUser filtered by the user_id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildMusicRoomUser requireOneByOwner(int $owner) Return the first ChildMusicRoomUser filtered by the owner column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildMusicRoomUser[]|ObjectCollection find(ConnectionInterface $con = null) Return ChildMusicRoomUser objects based on current ModelCriteria
 * @method     ChildMusicRoomUser[]|ObjectCollection findByMusicRoomId(int $music_room_id) Return ChildMusicRoomUser objects filtered by the music_room_id column
 * @method     ChildMusicRoomUser[]|ObjectCollection findByUserId(int $user_id) Return ChildMusicRoomUser objects filtered by the user_id column
 * @method     ChildMusicRoomUser[]|ObjectCollection findByOwner(int $owner) Return ChildMusicRoomUser objects filtered by the owner column
 * @method     ChildMusicRoomUser[]|\Propel\Runtime\Util\PropelModelPager paginate($page = 1, $maxPerPage = 10, ConnectionInterface $con = null) Issue a SELECT query based on the current ModelCriteria and uses a page and a maximum number of results per page to compute an offset and a limit
 *
 */
abstract class MusicRoomUserQuery extends ModelCriteria
{
    protected $entityNotFoundExceptionClass = '\\Propel\\Runtime\\Exception\\EntityNotFoundException';

    /**
     * Initializes internal state of \Engine\Base\MusicRoomUserQuery object.
     *
     * @param     string $dbName The database name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = 'engine', $modelName = '\\Engine\\MusicRoomUser', $modelAlias = null)
    {
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new ChildMusicRoomUserQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param     Criteria $criteria Optional Criteria to build the query from
     *
     * @return ChildMusicRoomUserQuery
     */
    public static function create($modelAlias = null, Criteria $criteria = null)
    {
        if ($criteria instanceof ChildMusicRoomUserQuery) {
            return $criteria;
        }
        $query = new ChildMusicRoomUserQuery();
        if (null !== $modelAlias) {
            $query->setModelAlias($modelAlias);
        }
        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj = $c->findPk(array(12, 34), $con);
     * </code>
     *
     * @param array[$music_room_id, $user_id] $key Primary key to use for the query
     * @param ConnectionInterface $con an optional connection object
     *
     * @return ChildMusicRoomUser|array|mixed the result, formatted by the current formatter
     */
    public function findPk($key, ConnectionInterface $con = null)
    {
        if ($key === null) {
            return null;
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getReadConnection(MusicRoomUserTableMap::DATABASE_NAME);
        }

        $this->basePreSelect($con);

        if (
            $this->formatter || $this->modelAlias || $this->with || $this->select
            || $this->selectColumns || $this->asColumns || $this->selectModifiers
            || $this->map || $this->having || $this->joins
        ) {
            return $this->findPkComplex($key, $con);
        }

        if ((null !== ($obj = MusicRoomUserTableMap::getInstanceFromPool(serialize([(null === $key[0] || is_scalar($key[0]) || is_callable([$key[0], '__toString']) ? (string) $key[0] : $key[0]), (null === $key[1] || is_scalar($key[1]) || is_callable([$key[1], '__toString']) ? (string) $key[1] : $key[1])]))))) {
            // the object is already in the instance pool
            return $obj;
        }

        return $this->findPkSimple($key, $con);
    }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildMusicRoomUser A model object, or null if the key is not found
     */
    protected function findPkSimple($key, ConnectionInterface $con)
    {
        $sql = 'SELECT music_room_id, user_id, owner FROM music_room_users WHERE music_room_id = :p0 AND user_id = :p1';
        try {
            $stmt = $con->prepare($sql);
            $stmt->bindValue(':p0', $key[0], PDO::PARAM_INT);
            $stmt->bindValue(':p1', $key[1], PDO::PARAM_INT);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), 0, $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(\PDO::FETCH_NUM)) {
            /** @var ChildMusicRoomUser $obj */
            $obj = new ChildMusicRoomUser();
            $obj->hydrate($row);
            MusicRoomUserTableMap::addInstanceToPool($obj, serialize([(null === $key[0] || is_scalar($key[0]) || is_callable([$key[0], '__toString']) ? (string) $key[0] : $key[0]), (null === $key[1] || is_scalar($key[1]) || is_callable([$key[1], '__toString']) ? (string) $key[1] : $key[1])]));
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @return ChildMusicRoomUser|array|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, ConnectionInterface $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($dataFetcher);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(array(12, 56), array(832, 123), array(123, 456)), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     ConnectionInterface $con an optional connection object
     *
     * @return ObjectCollection|array|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getReadConnection($this->getDbName());
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($dataFetcher);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return $this|ChildMusicRoomUserQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {
        $this->addUsingAlias(MusicRoomUserTableMap::COL_MUSIC_ROOM_ID, $key[0], Criteria::EQUAL);
        $this->addUsingAlias(MusicRoomUserTableMap::COL_USER_ID, $key[1], Criteria::EQUAL);

        return $this;
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return $this|ChildMusicRoomUserQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {
        if (empty($keys)) {
            return $this->add(null, '1<>1', Criteria::CUSTOM);
        }
        foreach ($keys as $key) {
            $cton0 = $this->getNewCriterion(MusicRoomUserTableMap::COL_MUSIC_ROOM_ID, $key[0], Criteria::EQUAL);
            $cton1 = $this->getNewCriterion(MusicRoomUserTableMap::COL_USER_ID, $key[1], Criteria::EQUAL);
            $cton0->addAnd($cton1);
            $this->addOr($cton0);
        }

        return $this;
    }

    /**
     * Filter the query on the music_room_id column
     *
     * Example usage:
     * <code>
     * $query->filterByMusicRoomId(1234); // WHERE music_room_id = 1234
     * $query->filterByMusicRoomId(array(12, 34)); // WHERE music_room_id IN (12, 34)
     * $query->filterByMusicRoomId(array('min' => 12)); // WHERE music_room_id > 12
     * </code>
     *
     * @see       filterByMusicRoom()
     *
     * @param     mixed $musicRoomId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildMusicRoomUserQuery The current query, for fluid interface
     */
    public function filterByMusicRoomId($musicRoomId = null, $comparison = null)
    {
        if (is_array($musicRoomId)) {
            $useMinMax = false;
            if (isset($musicRoomId['min'])) {
                $this->addUsingAlias(MusicRoomUserTableMap::COL_MUSIC_ROOM_ID, $musicRoomId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($musicRoomId['max'])) {
                $this->addUsingAlias(MusicRoomUserTableMap::COL_MUSIC_ROOM_ID, $musicRoomId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(MusicRoomUserTableMap::COL_MUSIC_ROOM_ID, $musicRoomId, $comparison);
    }

    /**
     * Filter the query on the user_id column
     *
     * Example usage:
     * <code>
     * $query->filterByUserId(1234); // WHERE user_id = 1234
     * $query->filterByUserId(array(12, 34)); // WHERE user_id IN (12, 34)
     * $query->filterByUserId(array('min' => 12)); // WHERE user_id > 12
     * </code>
     *
     * @see       filterByUser()
     *
     * @param     mixed $userId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildMusicRoomUserQuery The current query, for fluid interface
     */
    public function filterByUserId($userId = null, $comparison = null)
    {
        if (is_array($userId)) {
            $useMinMax = false;
            if (isset($userId['min'])) {
                $this->addUsingAlias(MusicRoomUserTableMap::COL_USER_ID, $userId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($userId['max'])) {
                $this->addUsingAlias(MusicRoomUserTableMap::COL_USER_ID, $userId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(MusicRoomUserTableMap::COL_USER_ID, $userId, $comparison);
    }

    /**
     * Filter the query on the owner column
     *
     * Example usage:
     * <code>
     * $query->filterByOwner(1234); // WHERE owner = 1234
     * $query->filterByOwner(array(12, 34)); // WHERE owner IN (12, 34)
     * $query->filterByOwner(array('min' => 12)); // WHERE owner > 12
     * </code>
     *
     * @param     mixed $owner The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildMusicRoomUserQuery The current query, for fluid interface
     */
    public function filterByOwner($owner = null, $comparison = null)
    {
        if (is_array($owner)) {
            $useMinMax = false;
            if (isset($owner['min'])) {
                $this->addUsingAlias(MusicRoomUserTableMap::COL_OWNER, $owner['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($owner['max'])) {
                $this->addUsingAlias(MusicRoomUserTableMap::COL_OWNER, $owner['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(MusicRoomUserTableMap::COL_OWNER, $owner, $comparison);
    }

    /**
     * Filter the query by a related \Engine\MusicRoom object
     *
     * @param \Engine\MusicRoom|ObjectCollection $musicRoom The related object(s) to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildMusicRoomUserQuery The current query, for fluid interface
     */
    public function filterByMusicRoom($musicRoom, $comparison = null)
    {
        if ($musicRoom instanceof \Engine\MusicRoom) {
            return $this
                ->addUsingAlias(MusicRoomUserTableMap::COL_MUSIC_ROOM_ID, $musicRoom->getId(), $comparison);
        } elseif ($musicRoom instanceof ObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(MusicRoomUserTableMap::COL_MUSIC_ROOM_ID, $musicRoom->toKeyValue('PrimaryKey', 'Id'), $comparison);
        } else {
            throw new PropelException('filterByMusicRoom() only accepts arguments of type \Engine\MusicRoom or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the MusicRoom relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildMusicRoomUserQuery The current query, for fluid interface
     */
    public function joinMusicRoom($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('MusicRoom');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'MusicRoom');
        }

        return $this;
    }

    /**
     * Use the MusicRoom relation MusicRoom object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \Engine\MusicRoomQuery A secondary query class using the current class as primary query
     */
    public function useMusicRoomQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinMusicRoom($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'MusicRoom', '\Engine\MusicRoomQuery');
    }

    /**
     * Filter the query by a related \Engine\User object
     *
     * @param \Engine\User|ObjectCollection $user The related object(s) to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildMusicRoomUserQuery The current query, for fluid interface
     */
    public function filterByUser($user, $comparison = null)
    {
        if ($user instanceof \Engine\User) {
            return $this
                ->addUsingAlias(MusicRoomUserTableMap::COL_USER_ID, $user->getId(), $comparison);
        } elseif ($user instanceof ObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(MusicRoomUserTableMap::COL_USER_ID, $user->toKeyValue('PrimaryKey', 'Id'), $comparison);
        } else {
            throw new PropelException('filterByUser() only accepts arguments of type \Engine\User or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the User relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildMusicRoomUserQuery The current query, for fluid interface
     */
    public function joinUser($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('User');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'User');
        }

        return $this;
    }

    /**
     * Use the User relation User object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \Engine\UserQuery A secondary query class using the current class as primary query
     */
    public function useUserQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinUser($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'User', '\Engine\UserQuery');
    }

    /**
     * Exclude object from result
     *
     * @param   ChildMusicRoomUser $musicRoomUser Object to remove from the list of results
     *
     * @return $this|ChildMusicRoomUserQuery The current query, for fluid interface
     */
    public function prune($musicRoomUser = null)
    {
        if ($musicRoomUser) {
            $this->addCond('pruneCond0', $this->getAliasedColName(MusicRoomUserTableMap::COL_MUSIC_ROOM_ID), $musicRoomUser->getMusicRoomId(), Criteria::NOT_EQUAL);
            $this->addCond('pruneCond1', $this->getAliasedColName(MusicRoomUserTableMap::COL_USER_ID), $musicRoomUser->getUserId(), Criteria::NOT_EQUAL);
            $this->combine(array('pruneCond0', 'pruneCond1'), Criteria::LOGICAL_OR);
        }

        return $this;
    }

    /**
     * Deletes all rows from the music_room_users table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public function doDeleteAll(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(MusicRoomUserTableMap::DATABASE_NAME);
        }

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con) {
            $affectedRows = 0; // initialize var to track total num of affected rows
            $affectedRows += parent::doDeleteAll($con);
            // Because this db requires some delete cascade/set null emulation, we have to
            // clear the cached instance *after* the emulation has happened (since
            // instances get re-added by the select statement contained therein).
            MusicRoomUserTableMap::clearInstancePool();
            MusicRoomUserTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

    /**
     * Performs a DELETE on the database based on the current ModelCriteria
     *
     * @param ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public function delete(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(MusicRoomUserTableMap::DATABASE_NAME);
        }

        $criteria = $this;

        // Set the correct dbName
        $criteria->setDbName(MusicRoomUserTableMap::DATABASE_NAME);

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con, $criteria) {
            $affectedRows = 0; // initialize var to track total num of affected rows

            MusicRoomUserTableMap::removeInstanceFromPool($criteria);

            $affectedRows += ModelCriteria::delete($con);
            MusicRoomUserTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

} // MusicRoomUserQuery
