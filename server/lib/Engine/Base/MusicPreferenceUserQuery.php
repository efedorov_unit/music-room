<?php

namespace Engine\Base;

use \Exception;
use \PDO;
use Engine\MusicPreferenceUser as ChildMusicPreferenceUser;
use Engine\MusicPreferenceUserQuery as ChildMusicPreferenceUserQuery;
use Engine\Map\MusicPreferenceUserTableMap;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\ActiveQuery\ModelJoin;
use Propel\Runtime\Collection\ObjectCollection;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\Exception\PropelException;

/**
 * Base class that represents a query for the 'music_preferences_users' table.
 *
 *
 *
 * @method     ChildMusicPreferenceUserQuery orderByPreferenceId($order = Criteria::ASC) Order by the preference_id column
 * @method     ChildMusicPreferenceUserQuery orderByUserId($order = Criteria::ASC) Order by the user_id column
 *
 * @method     ChildMusicPreferenceUserQuery groupByPreferenceId() Group by the preference_id column
 * @method     ChildMusicPreferenceUserQuery groupByUserId() Group by the user_id column
 *
 * @method     ChildMusicPreferenceUserQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method     ChildMusicPreferenceUserQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method     ChildMusicPreferenceUserQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method     ChildMusicPreferenceUserQuery leftJoinWith($relation) Adds a LEFT JOIN clause and with to the query
 * @method     ChildMusicPreferenceUserQuery rightJoinWith($relation) Adds a RIGHT JOIN clause and with to the query
 * @method     ChildMusicPreferenceUserQuery innerJoinWith($relation) Adds a INNER JOIN clause and with to the query
 *
 * @method     ChildMusicPreferenceUserQuery leftJoinMusicPreference($relationAlias = null) Adds a LEFT JOIN clause to the query using the MusicPreference relation
 * @method     ChildMusicPreferenceUserQuery rightJoinMusicPreference($relationAlias = null) Adds a RIGHT JOIN clause to the query using the MusicPreference relation
 * @method     ChildMusicPreferenceUserQuery innerJoinMusicPreference($relationAlias = null) Adds a INNER JOIN clause to the query using the MusicPreference relation
 *
 * @method     ChildMusicPreferenceUserQuery joinWithMusicPreference($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the MusicPreference relation
 *
 * @method     ChildMusicPreferenceUserQuery leftJoinWithMusicPreference() Adds a LEFT JOIN clause and with to the query using the MusicPreference relation
 * @method     ChildMusicPreferenceUserQuery rightJoinWithMusicPreference() Adds a RIGHT JOIN clause and with to the query using the MusicPreference relation
 * @method     ChildMusicPreferenceUserQuery innerJoinWithMusicPreference() Adds a INNER JOIN clause and with to the query using the MusicPreference relation
 *
 * @method     ChildMusicPreferenceUserQuery leftJoinUser($relationAlias = null) Adds a LEFT JOIN clause to the query using the User relation
 * @method     ChildMusicPreferenceUserQuery rightJoinUser($relationAlias = null) Adds a RIGHT JOIN clause to the query using the User relation
 * @method     ChildMusicPreferenceUserQuery innerJoinUser($relationAlias = null) Adds a INNER JOIN clause to the query using the User relation
 *
 * @method     ChildMusicPreferenceUserQuery joinWithUser($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the User relation
 *
 * @method     ChildMusicPreferenceUserQuery leftJoinWithUser() Adds a LEFT JOIN clause and with to the query using the User relation
 * @method     ChildMusicPreferenceUserQuery rightJoinWithUser() Adds a RIGHT JOIN clause and with to the query using the User relation
 * @method     ChildMusicPreferenceUserQuery innerJoinWithUser() Adds a INNER JOIN clause and with to the query using the User relation
 *
 * @method     \Engine\MusicPreferenceQuery|\Engine\UserQuery endUse() Finalizes a secondary criteria and merges it with its primary Criteria
 *
 * @method     ChildMusicPreferenceUser findOne(ConnectionInterface $con = null) Return the first ChildMusicPreferenceUser matching the query
 * @method     ChildMusicPreferenceUser findOneOrCreate(ConnectionInterface $con = null) Return the first ChildMusicPreferenceUser matching the query, or a new ChildMusicPreferenceUser object populated from the query conditions when no match is found
 *
 * @method     ChildMusicPreferenceUser findOneByPreferenceId(int $preference_id) Return the first ChildMusicPreferenceUser filtered by the preference_id column
 * @method     ChildMusicPreferenceUser findOneByUserId(int $user_id) Return the first ChildMusicPreferenceUser filtered by the user_id column *

 * @method     ChildMusicPreferenceUser requirePk($key, ConnectionInterface $con = null) Return the ChildMusicPreferenceUser by primary key and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildMusicPreferenceUser requireOne(ConnectionInterface $con = null) Return the first ChildMusicPreferenceUser matching the query and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildMusicPreferenceUser requireOneByPreferenceId(int $preference_id) Return the first ChildMusicPreferenceUser filtered by the preference_id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildMusicPreferenceUser requireOneByUserId(int $user_id) Return the first ChildMusicPreferenceUser filtered by the user_id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildMusicPreferenceUser[]|ObjectCollection find(ConnectionInterface $con = null) Return ChildMusicPreferenceUser objects based on current ModelCriteria
 * @method     ChildMusicPreferenceUser[]|ObjectCollection findByPreferenceId(int $preference_id) Return ChildMusicPreferenceUser objects filtered by the preference_id column
 * @method     ChildMusicPreferenceUser[]|ObjectCollection findByUserId(int $user_id) Return ChildMusicPreferenceUser objects filtered by the user_id column
 * @method     ChildMusicPreferenceUser[]|\Propel\Runtime\Util\PropelModelPager paginate($page = 1, $maxPerPage = 10, ConnectionInterface $con = null) Issue a SELECT query based on the current ModelCriteria and uses a page and a maximum number of results per page to compute an offset and a limit
 *
 */
abstract class MusicPreferenceUserQuery extends ModelCriteria
{
    protected $entityNotFoundExceptionClass = '\\Propel\\Runtime\\Exception\\EntityNotFoundException';

    /**
     * Initializes internal state of \Engine\Base\MusicPreferenceUserQuery object.
     *
     * @param     string $dbName The database name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = 'engine', $modelName = '\\Engine\\MusicPreferenceUser', $modelAlias = null)
    {
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new ChildMusicPreferenceUserQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param     Criteria $criteria Optional Criteria to build the query from
     *
     * @return ChildMusicPreferenceUserQuery
     */
    public static function create($modelAlias = null, Criteria $criteria = null)
    {
        if ($criteria instanceof ChildMusicPreferenceUserQuery) {
            return $criteria;
        }
        $query = new ChildMusicPreferenceUserQuery();
        if (null !== $modelAlias) {
            $query->setModelAlias($modelAlias);
        }
        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj = $c->findPk(array(12, 34), $con);
     * </code>
     *
     * @param array[$preference_id, $user_id] $key Primary key to use for the query
     * @param ConnectionInterface $con an optional connection object
     *
     * @return ChildMusicPreferenceUser|array|mixed the result, formatted by the current formatter
     */
    public function findPk($key, ConnectionInterface $con = null)
    {
        if ($key === null) {
            return null;
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getReadConnection(MusicPreferenceUserTableMap::DATABASE_NAME);
        }

        $this->basePreSelect($con);

        if (
            $this->formatter || $this->modelAlias || $this->with || $this->select
            || $this->selectColumns || $this->asColumns || $this->selectModifiers
            || $this->map || $this->having || $this->joins
        ) {
            return $this->findPkComplex($key, $con);
        }

        if ((null !== ($obj = MusicPreferenceUserTableMap::getInstanceFromPool(serialize([(null === $key[0] || is_scalar($key[0]) || is_callable([$key[0], '__toString']) ? (string) $key[0] : $key[0]), (null === $key[1] || is_scalar($key[1]) || is_callable([$key[1], '__toString']) ? (string) $key[1] : $key[1])]))))) {
            // the object is already in the instance pool
            return $obj;
        }

        return $this->findPkSimple($key, $con);
    }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildMusicPreferenceUser A model object, or null if the key is not found
     */
    protected function findPkSimple($key, ConnectionInterface $con)
    {
        $sql = 'SELECT preference_id, user_id FROM music_preferences_users WHERE preference_id = :p0 AND user_id = :p1';
        try {
            $stmt = $con->prepare($sql);
            $stmt->bindValue(':p0', $key[0], PDO::PARAM_INT);
            $stmt->bindValue(':p1', $key[1], PDO::PARAM_INT);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), 0, $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(\PDO::FETCH_NUM)) {
            /** @var ChildMusicPreferenceUser $obj */
            $obj = new ChildMusicPreferenceUser();
            $obj->hydrate($row);
            MusicPreferenceUserTableMap::addInstanceToPool($obj, serialize([(null === $key[0] || is_scalar($key[0]) || is_callable([$key[0], '__toString']) ? (string) $key[0] : $key[0]), (null === $key[1] || is_scalar($key[1]) || is_callable([$key[1], '__toString']) ? (string) $key[1] : $key[1])]));
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @return ChildMusicPreferenceUser|array|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, ConnectionInterface $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($dataFetcher);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(array(12, 56), array(832, 123), array(123, 456)), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     ConnectionInterface $con an optional connection object
     *
     * @return ObjectCollection|array|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getReadConnection($this->getDbName());
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($dataFetcher);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return $this|ChildMusicPreferenceUserQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {
        $this->addUsingAlias(MusicPreferenceUserTableMap::COL_PREFERENCE_ID, $key[0], Criteria::EQUAL);
        $this->addUsingAlias(MusicPreferenceUserTableMap::COL_USER_ID, $key[1], Criteria::EQUAL);

        return $this;
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return $this|ChildMusicPreferenceUserQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {
        if (empty($keys)) {
            return $this->add(null, '1<>1', Criteria::CUSTOM);
        }
        foreach ($keys as $key) {
            $cton0 = $this->getNewCriterion(MusicPreferenceUserTableMap::COL_PREFERENCE_ID, $key[0], Criteria::EQUAL);
            $cton1 = $this->getNewCriterion(MusicPreferenceUserTableMap::COL_USER_ID, $key[1], Criteria::EQUAL);
            $cton0->addAnd($cton1);
            $this->addOr($cton0);
        }

        return $this;
    }

    /**
     * Filter the query on the preference_id column
     *
     * Example usage:
     * <code>
     * $query->filterByPreferenceId(1234); // WHERE preference_id = 1234
     * $query->filterByPreferenceId(array(12, 34)); // WHERE preference_id IN (12, 34)
     * $query->filterByPreferenceId(array('min' => 12)); // WHERE preference_id > 12
     * </code>
     *
     * @see       filterByMusicPreference()
     *
     * @param     mixed $preferenceId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildMusicPreferenceUserQuery The current query, for fluid interface
     */
    public function filterByPreferenceId($preferenceId = null, $comparison = null)
    {
        if (is_array($preferenceId)) {
            $useMinMax = false;
            if (isset($preferenceId['min'])) {
                $this->addUsingAlias(MusicPreferenceUserTableMap::COL_PREFERENCE_ID, $preferenceId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($preferenceId['max'])) {
                $this->addUsingAlias(MusicPreferenceUserTableMap::COL_PREFERENCE_ID, $preferenceId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(MusicPreferenceUserTableMap::COL_PREFERENCE_ID, $preferenceId, $comparison);
    }

    /**
     * Filter the query on the user_id column
     *
     * Example usage:
     * <code>
     * $query->filterByUserId(1234); // WHERE user_id = 1234
     * $query->filterByUserId(array(12, 34)); // WHERE user_id IN (12, 34)
     * $query->filterByUserId(array('min' => 12)); // WHERE user_id > 12
     * </code>
     *
     * @see       filterByUser()
     *
     * @param     mixed $userId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildMusicPreferenceUserQuery The current query, for fluid interface
     */
    public function filterByUserId($userId = null, $comparison = null)
    {
        if (is_array($userId)) {
            $useMinMax = false;
            if (isset($userId['min'])) {
                $this->addUsingAlias(MusicPreferenceUserTableMap::COL_USER_ID, $userId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($userId['max'])) {
                $this->addUsingAlias(MusicPreferenceUserTableMap::COL_USER_ID, $userId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(MusicPreferenceUserTableMap::COL_USER_ID, $userId, $comparison);
    }

    /**
     * Filter the query by a related \Engine\MusicPreference object
     *
     * @param \Engine\MusicPreference|ObjectCollection $musicPreference The related object(s) to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildMusicPreferenceUserQuery The current query, for fluid interface
     */
    public function filterByMusicPreference($musicPreference, $comparison = null)
    {
        if ($musicPreference instanceof \Engine\MusicPreference) {
            return $this
                ->addUsingAlias(MusicPreferenceUserTableMap::COL_PREFERENCE_ID, $musicPreference->getId(), $comparison);
        } elseif ($musicPreference instanceof ObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(MusicPreferenceUserTableMap::COL_PREFERENCE_ID, $musicPreference->toKeyValue('PrimaryKey', 'Id'), $comparison);
        } else {
            throw new PropelException('filterByMusicPreference() only accepts arguments of type \Engine\MusicPreference or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the MusicPreference relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildMusicPreferenceUserQuery The current query, for fluid interface
     */
    public function joinMusicPreference($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('MusicPreference');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'MusicPreference');
        }

        return $this;
    }

    /**
     * Use the MusicPreference relation MusicPreference object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \Engine\MusicPreferenceQuery A secondary query class using the current class as primary query
     */
    public function useMusicPreferenceQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinMusicPreference($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'MusicPreference', '\Engine\MusicPreferenceQuery');
    }

    /**
     * Filter the query by a related \Engine\User object
     *
     * @param \Engine\User|ObjectCollection $user The related object(s) to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildMusicPreferenceUserQuery The current query, for fluid interface
     */
    public function filterByUser($user, $comparison = null)
    {
        if ($user instanceof \Engine\User) {
            return $this
                ->addUsingAlias(MusicPreferenceUserTableMap::COL_USER_ID, $user->getId(), $comparison);
        } elseif ($user instanceof ObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(MusicPreferenceUserTableMap::COL_USER_ID, $user->toKeyValue('PrimaryKey', 'Id'), $comparison);
        } else {
            throw new PropelException('filterByUser() only accepts arguments of type \Engine\User or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the User relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildMusicPreferenceUserQuery The current query, for fluid interface
     */
    public function joinUser($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('User');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'User');
        }

        return $this;
    }

    /**
     * Use the User relation User object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \Engine\UserQuery A secondary query class using the current class as primary query
     */
    public function useUserQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinUser($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'User', '\Engine\UserQuery');
    }

    /**
     * Exclude object from result
     *
     * @param   ChildMusicPreferenceUser $musicPreferenceUser Object to remove from the list of results
     *
     * @return $this|ChildMusicPreferenceUserQuery The current query, for fluid interface
     */
    public function prune($musicPreferenceUser = null)
    {
        if ($musicPreferenceUser) {
            $this->addCond('pruneCond0', $this->getAliasedColName(MusicPreferenceUserTableMap::COL_PREFERENCE_ID), $musicPreferenceUser->getPreferenceId(), Criteria::NOT_EQUAL);
            $this->addCond('pruneCond1', $this->getAliasedColName(MusicPreferenceUserTableMap::COL_USER_ID), $musicPreferenceUser->getUserId(), Criteria::NOT_EQUAL);
            $this->combine(array('pruneCond0', 'pruneCond1'), Criteria::LOGICAL_OR);
        }

        return $this;
    }

    /**
     * Deletes all rows from the music_preferences_users table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public function doDeleteAll(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(MusicPreferenceUserTableMap::DATABASE_NAME);
        }

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con) {
            $affectedRows = 0; // initialize var to track total num of affected rows
            $affectedRows += parent::doDeleteAll($con);
            // Because this db requires some delete cascade/set null emulation, we have to
            // clear the cached instance *after* the emulation has happened (since
            // instances get re-added by the select statement contained therein).
            MusicPreferenceUserTableMap::clearInstancePool();
            MusicPreferenceUserTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

    /**
     * Performs a DELETE on the database based on the current ModelCriteria
     *
     * @param ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public function delete(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(MusicPreferenceUserTableMap::DATABASE_NAME);
        }

        $criteria = $this;

        // Set the correct dbName
        $criteria->setDbName(MusicPreferenceUserTableMap::DATABASE_NAME);

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con, $criteria) {
            $affectedRows = 0; // initialize var to track total num of affected rows

            MusicPreferenceUserTableMap::removeInstanceFromPool($criteria);

            $affectedRows += ModelCriteria::delete($con);
            MusicPreferenceUserTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

} // MusicPreferenceUserQuery
