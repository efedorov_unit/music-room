<?php

namespace Engine;

use Engine\Base\MusicRoomTrack as BaseMusicRoomTrack;

/**
 * Skeleton subclass for representing a row from the 'music_room_tracks' table.
 *
 *
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 *
 */
class MusicRoomTrack extends BaseMusicRoomTrack
{

}
