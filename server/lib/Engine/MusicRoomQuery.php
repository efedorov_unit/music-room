<?php

namespace Engine;

use Engine\Base\MusicRoomQuery as BaseMusicRoomQuery;

/**
 * Skeleton subclass for performing query and update operations on the 'music_rooms' table.
 *
 *
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 *
 */
class MusicRoomQuery extends BaseMusicRoomQuery
{

}
