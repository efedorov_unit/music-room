<?php
namespace Engine\X;

use Exception;

class Lang extends Exception
{
    protected $type = 'LANG_NOT_SET';

    public function getError()
    {
        return [
            'Error' => [
                'Type' => $this->type,
                'Message' => "Lang was not set in the \\Engine\\Context. "
                    . "Add \\Engine::\\Context::setLang(\$lang) in your code.",
            ]
        ];
    }
}
