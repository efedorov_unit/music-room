<?php

namespace Engine;

use Engine\Base\FacebookAccountQuery as BaseFacebookAccountQuery;

/**
 * Skeleton subclass for performing query and update operations on the 'facebook_accounts' table.
 *
 *
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 *
 */
class FacebookAccountQuery extends BaseFacebookAccountQuery
{

}
