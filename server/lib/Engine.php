<?php

namespace

{
    class Engine
    {
        public static function create($config)
        {
            $serviceContainer = \Propel\Runtime\Propel::getServiceContainer();
            $serviceContainer->checkVersion('2.0.0-dev');
            $serviceContainer->setAdapterClass('engine', 'mysql');
            $manager = new \Propel\Runtime\Connection\ConnectionManagerSingle();
            $manager->setConfiguration(array (
                'classname'  => $config['classname'],
                'dsn'        => $config['dsn'],
                'user'       => $config['user'],
                'password'   => $config['password'],
                'attributes' => $config['attributes'],
            ));
            $manager->setName('engine');
            $serviceContainer->setConnectionManager('engine', $manager);
            $serviceContainer->setDefaultDatasource('engine');
        }
    }
}
