<?php

namespace

{
    use Monolog\Handler\StreamHandler;
    use Monolog\Logger;
    use Propel\Runtime\Propel;
    use Psr\Http\Message\RequestInterface as ReqInt;
    use Psr\Http\Message\ResponseInterface as ResInt;

    class AppFactory
    {
        public static function create($config)
        {
            // Create monolog logger and store logger in container
            $container = self::containerization($config);

            // Prepare app
            $app = new \Slim\App($container);

            if ($config['debugMode']) {
                $app->add(new \Tuupola\Middleware\Cors([
                    'headers.allow' => ['content-type', 'Authorization']
                ]));
                $con = Propel::getWriteConnection(Engine\Map\UserTableMap::DATABASE_NAME);
                $con->useDebug(true);
            }

            $app->add(new Middleware\SessionCookies(['lifetime' => '1 day']));
            $app->add(new Middleware\TrailingSlash);

            $isAuth   = new Middleware\IsAuth;
            $isAuthJWT = new \Slim\Middleware\JwtAuthentication([
                'secret'    => $config['jwt_key'],
                'algorithm' => ['HS512'],
                'attribute' => 'session',
                'header'    => 'HTTP_AUTHORIZATION',
                'regexp'    => '/(.*)/',
                'relaxed'   => ['localhost', '127.0.0.1', ''],
                'secure'    => false
            ]);
            $parseJwtSession = new Middleware\JwtSession([
                'secret'    => $config['jwt_key'],
                'algorithm' => ['HS512'],
                'attribute' => 'session',
                'header'    => 'HTTP_AUTHORIZATION'
            ]);

            $app->group('/api/v1', function () use ($container, $isAuth, $isAuthJWT) {
                $this->group('/sessions', function () use ($container, $isAuthJWT) {
                    $session = new Controller\API\Session($container);

                    $this->post('/', [$session, 'create']);
                    $this->delete('/', [$session, 'delete']);
                });
                $this->group('/user', function () use ($container, $isAuthJWT) {
                    $user = new Controller\API\User($container);

                    $this->get('/', [$user, 'index']);
                    $this->post('/', [$user, 'create']);
                    $this->get('/all/', [$user, 'getUsersList']);
                    $this->post('/fb/', [$user, 'createFb']);
                    $this->post('/deezer/', [$user, 'createDeezer']);
                    $this->post('/link_fb/', [$user, 'linkWithFb']);
                    $this->post('/music_pref/', [$user, 'musicPrefs']);
                    $this->post('/password/', [$user, 'changePass']);
                });
                $this->group('/music_room', function () use ($container, $isAuthJWT) {
                    $musicRoom = new Controller\API\MusicRoom($container);

                    $this->get('/', [$musicRoom, 'index']);
                    $this->get('/show_rooms/', [$musicRoom, 'showRooms']);
                    $this->get('/show/', [$musicRoom, 'show']);
                    $this->post('/', [$musicRoom, 'create']);
                    $this->post('/update/', [$musicRoom, 'update']);
                    $this->post('/add_track/', [$musicRoom, 'addTrack']);
                    $this->post('/remove/', [$musicRoom, 'remove']);
                });
            })->add($parseJwtSession);
            
            return $app;
        }

        private static function containerization($config)
        {
            $container = new \Slim\Container;

            $container['log'] = function () use ($config) {
                $log = new \Monolog\Logger($config['monolog']['name']);

                foreach ($config['monolog']['handlers'] as $option) {
                    $handler = new \Monolog\Handler\StreamHandler($option['filepath']);

                    // set format if need
                    if (isset($option['format']) && $option['format']) {
                        $handler->setFormatter(
                            new Monolog\Formatter\LineFormatter($option['format'])
                        );
                    }

                    $log->pushHandler($handler);
                }

                return $log;
            };

            $container['config'] = $config;

            $container['userId'] = function ($c) {
                $userId = \Utils\Session::get('UserId', 0);

                return $userId;
            };

            return $container;
        }
    }
}
