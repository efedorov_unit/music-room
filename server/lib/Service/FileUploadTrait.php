<?php

namespace Service;

trait FileUpload
{
    private function saveFile(
        \Psr\Http\Message\UploadedFileInterface $file,
        $model,
        $baseDir = __DIR__.'/../../public/site/static/images/',
        $depth = 2
    ) {
        $id = $model->getId();
        $fileExtension = pathinfo($file->getClientFilename(), PATHINFO_EXTENSION);
        $fileExtension = $fileExtension ? $fileExtension : explode('/', $file->getClientMediaType())[1];

        $filenameLength = $depth * 3 + 3;

        $filename = sprintf("%0{$filenameLength}d", $id);

        preg_match_all('/\d{1,3}/', $filename, $matches);

        $dirs = $matches[0];
        array_pop($dirs);

        $uploadDir = $baseDir;
        $relatedDir = '';
        foreach ($dirs as $dir) {
            $uploadDir .= "$dir/";
            $relatedDir .= "/$dir";
            if (!file_exists($uploadDir)) {
                mkdir($uploadDir, 0775, true);
            }
        }

        $newFilePath  = "$uploadDir/$filename.$fileExtension";
        $newModelPath = "$relatedDir/$filename.$fileExtension";

        $file->moveTo($newFilePath);
        $model->setPath($newModelPath);
    }
}
