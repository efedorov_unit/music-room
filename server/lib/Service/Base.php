<?php

namespace Service;

use Engine\Context;
use Engine\UserRoleMapQuery;
use Engine\UserQuery;

abstract class Base
{
    use Utils;

    private $log;
    private $config;
    private $userId;

    public function __construct($attrs)
    {
        $this->log = $attrs['log'];
        $this->config = $attrs['config'];
        $this->userId = $attrs['userId'];
    }

    abstract protected function validate(array $params);

    abstract protected function execute(array $params);

    protected function log()
    {
        return $this->log;
    }

    protected function config()
    {
        return $this->config;
    }

    protected function userId()
    {
        return $this->userId;
    }

    final public function run(array $params = [])
    {
        try {
            $validated = $this->validate($params);
            $result = $this->execute($validated);

            return $result;
        } catch (\Exception $e) {
            throw $e;
        }
    }
}
