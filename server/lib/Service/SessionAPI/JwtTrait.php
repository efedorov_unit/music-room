<?php

namespace Service\SessionAPI;

trait Jwt
{
    public function createUserJwt(\Engine\User $user)
    {
        $data = [
            'iat'         => time(),
            'id'          => $user->getId(),
            'name'        => $user->getNickname(),
            'email'       => $user->getEmail()
        ];

        $jwt = \Firebase\JWT\JWT::encode(
            $data,
            $this->config()['jwt_key'],
            'HS512'
        );

        return $jwt;
    }
}
