<?php

namespace Service\SessionAPI;

use Engine\UserQuery;
use Service\X;
use Service\Validator;
use Service\Base;

final class Create extends Base
{
    use Jwt;

    /**
     * @param array $params
     * @return array|bool
     * @throws X
     */
    final protected function validate(array $params)
    {
        $rules = [
            'Email'       => [ 'required', 'email', 'to_lc' ],
            'Password'    => [ 'required' ]
        ];

        return Validator::validate($params, $rules);
    }

    /**
     * @param array $params
     * @return array
     * @throws X
     */
    final protected function execute(array $params)
    {
        $user = UserQuery::create()
            ->filterByEmail($params['Email'])
            ->findOne();

        if (!$user) {
            throw new X(array(
                'Type'    => 'NOT_FOUND',
                'Fields'  => [ 'email' => 'NOT_FOUND' ],
                'Message' => 'User not found'
            ));
        }

        if (!$user->authorize($params['Password'])) {
            throw new X(array(
                'Type'    => 'WRONG_PASSWORD',
                'Fields'  => [ 'password' => 'WRONG_PASSWORD' ],
                'Message' => 'Password is wrong'
            ));
        }

        $jwt = $this->createUserJwt($user);

        return [
            'Status' => 1,
            'JWT' => $jwt
        ];
    }
}
