<?php

namespace Service\MusicRoom;

use Service\X;
use Service\Base;
use Engine\MusicRoom;
use Engine\MusicRoomQuery;
use Propel\Runtime\ActiveQuery\Criteria;
use Engine\MusicRoomUserQuery;

final class SelectRoom extends Base
{
    /**
     * @param array $params
     * @return array|bool
     * @throws X
     */
    final protected function validate(array $params)
    {
        return [];
    }

    /**
     * @param array $params
     * @return array
     * @throws \Propel\Runtime\Exception\PropelException
     */
    final protected function execute(array $params)
    {
        $userId = $this->userId();

        $publicRooms = MusicRoomQuery::create()
            ->filterByType('public')
            ->groupById()
            ->find()
            ->toArray();

        foreach ($publicRooms as &$publicRoom) {
            $isOwner = MusicRoomUserQuery::create()
                ->filterByUserId($userId)
                ->filterByMusicRoomId($publicRoom['Id'])
                ->findOne();

            if (!$isOwner) {
                $publicRoom['IsOwner'] = "0";
                continue;
            }

            $publicRoom['IsOwner'] = $isOwner->getOwner();
        }

        $privateRooms = MusicRoomQuery::create()
            ->filterByType('private')
            ->useMusicRoomUserQuery()
                ->filterByUserId($userId)
                ->withColumn('MusicRoomUser.Owner', 'IsOwner')
            ->endUse()
            ->groupById()
            ->find()
            ->toArray();

        $rooms = array_merge($publicRooms, $privateRooms);

        return [
            'Status' => 1,
            'Rooms' => $rooms
        ];
    }
}
