<?php

namespace Service\MusicRoom;

use Service\X;
use Service\Validator;
use Service\Base;
use Engine\Base\MusicRoomUserQuery;
use Engine\UserQuery;
use Propel\Runtime\ActiveQuery\Criteria;

final class Update extends Base
{
    /**
     * @param array $params
     * @return array|bool
     * @throws X
     */
    final protected function validate(array $params)
    {
        $rules = [
            'UserId' => [ 'required', 'string' ],
            'RoomId' => [ 'required', 'string' ]
        ];

        return Validator::validate($params, $rules);
    }

    /**
     * @param array $params
     * @return array
     * @throws \Propel\Runtime\Exception\PropelException
     */
    final protected function execute(array $params)
    {
        $currUser = $this->userId();

        $mru = MusicRoomUserQuery::create()
            ->filterByMusicRoomId($params['RoomId'])
            ->filterByUserId($params['UserId'])
            ->findOneOrCreate();

        $new = $mru->isNew();

        $mru->fromArray([
            'MusicRoomId' => $params['RoomId'],
            'UserId' => $params['UserId'],
            'Owner' => 0
        ]);

        $new ? $mru->save() : $mru->delete();

        $users = UserQuery::create()
            ->filterById($currUser, Criteria::NOT_EQUAL)
            ->find()
            ->toArray('Id');

        $mruLinks = MusicRoomUserQuery::create()
            ->filterByUserId(array_keys($users))
            ->find()
            ->toArray();

        $jope = [];
        foreach ($users as &$user) {
            $user['Rooms'] = [];
            foreach ($mruLinks as $mruLink) {
                if ($mruLink['UserId'] === $user['Id']) {
                    $user['Rooms'][] = $mruLink['MusicRoomId'];
                }
            }
            $jope[] = $user;
        }

        return [
            'Status' => 1,
            'Users' => $jope
        ];
    }
}
