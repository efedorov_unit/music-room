<?php

namespace Service\MusicRoom;

use Service\X;
use Service\Validator;
use Service\Base;
use Engine\MusicRoom;
use Engine\MusicRoomUser;
use Engine\MusicRoomUserQuery;
use Engine\MusicRoomTrackQuery;

final class Remove extends Base
{
    /**
     * @param array $params
     * @return array|bool
     * @throws X
     */
    final protected function validate(array $params)
    {
        $rules = [
            'RoomId' => [ 'required', 'string' ],
            'TrackId' => [ 'required', 'string' ]
        ];

        return Validator::validate($params, $rules);
    }

    /**
     * @param array $params
     * @return array
     * @throws \Propel\Runtime\Exception\PropelException
     */
    final protected function execute(array $params)
    {
        $userId = $this->userId();

        $room = MusicRoomUserQuery::create()
            ->filterByUserId($userId)
            ->filterByMusicRoomId($params['RoomId'])
            ->filterByOwner(1)
            ->findOne();

        if (!$room) {
            throw new X([
                'Type'    => 'NOT_FOUND',
                'Fields'  => [ 'Room' => 'NOT_FOUND' ],
                'Message' => 'Room not found'
            ]);
        }

        MusicRoomTrackQuery::create()
            ->filterByRoomId($params['RoomId'])
            ->filterByTrackId($params['TrackId'])
            ->delete();

        $tracks = MusicRoomTrackQuery::create()
            ->useMusicRoomQuery()
                ->filterById($params['RoomId'])
            ->endUse()
            ->find()
            ->toArray();

        return [
            'Status' => 1,
            'Tracks' => $tracks
        ];
    }
}
