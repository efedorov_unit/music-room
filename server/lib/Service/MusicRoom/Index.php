<?php

namespace Service\MusicRoom;

use Service\X;
use Service\Base;
use Engine\MusicRoom;
use Engine\MusicRoomQuery;

final class Index extends Base
{
    /**
     * @param array $params
     * @return array|bool
     * @throws X
     */
    final protected function validate(array $params)
    {
        return [];
    }

    /**
     * @param array $params
     * @return array
     * @throws \Propel\Runtime\Exception\PropelException
     */
    final protected function execute(array $params)
    {
        $userId = $this->userId();

        $rooms = MusicRoomQuery::create()
            ->useMusicRoomUserQuery()
                ->filterByUserId($userId)
                ->filterByOwner(1)
            ->endUse()
            ->find()
            ->toArray();

        $rooms = count($rooms) ? $rooms : null;

        return [
            'Status' => 1,
            'Rooms' => $rooms
        ];
    }
}
