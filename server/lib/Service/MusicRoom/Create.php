<?php

namespace Service\MusicRoom;

use Service\X;
use Service\Validator;
use Service\Base;
use Engine\MusicRoom;
use Engine\MusicRoomUser;

final class Create extends Base
{
    /**
     * @param array $params
     * @return array|bool
     * @throws X
     */
    final protected function validate(array $params)
    {
        $rules = [
            'Type' => [ 'required', 'string' ],
            'Name' => [ 'required', 'string' ]
        ];

        return Validator::validate($params, $rules);
    }

    /**
     * @param array $params
     * @return array
     * @throws \Propel\Runtime\Exception\PropelException
     */
    final protected function execute(array $params)
    {
        $userId = $this->userId();
        $mr = new MusicRoom();

        $mr->fromArray($params);
        $mr->save();

        $link = new MusicRoomUser();

        $link->fromArray([
            'MusicRoomId' => $mr->getId(),
            'UserId' => $userId,
            'Owner' => 1
        ]);

        $link->save();

        return [
            'Status' => 1
        ];
    }
}
