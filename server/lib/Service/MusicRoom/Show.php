<?php

namespace Service\MusicRoom;

use Service\X;
use Service\Base;
use Engine\MusicRoomQuery;
use Service\Validator;
use Engine\MusicRoomTrackQuery;

final class Show extends Base
{
    /**
     * @param array $params
     * @return array|bool
     * @throws X
     */
    final protected function validate(array $params)
    {
        $rules = [
            'RoomId' => [ 'required', 'positive_integer' ]
        ];

        return Validator::validate($params, $rules);
    }

    /**
     * @param array $params
     * @return array
     * @throws \Propel\Runtime\Exception\PropelException
     */
    final protected function execute(array $params)
    {
        $userId = $this->userId();

        $tracks = MusicRoomTrackQuery::create()
            ->useMusicRoomQuery()
                ->filterById($params['RoomId'])
            ->endUse()
            ->find()
            ->toArray();

        return [
            'Status' => 1,
            'Tracks' => $tracks
        ];
    }
}
