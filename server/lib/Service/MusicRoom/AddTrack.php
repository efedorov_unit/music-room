<?php

namespace Service\MusicRoom;

use Service\X;
use Service\Validator;
use Service\Base;
use Engine\Base\MusicRoomUserQuery;
use Engine\UserQuery;
use Propel\Runtime\ActiveQuery\Criteria;
use Engine\MusicRoomTrackQuery;

final class AddTrack extends Base
{
    /**
     * @param array $params
     * @return array|bool
     * @throws X
     */
    final protected function validate(array $params)
    {
        $rules = [
            'RoomId' => [ 'required', 'string' ],
            'TrackId' => [ 'required', 'string' ],
            'Link' => [ 'required', 'string' ],
            'Preview' => [ 'required', 'string' ],
            'Title' => [ 'required', 'string' ],
            'Image' => [ 'required', 'string' ]
        ];

        return Validator::validate($params, $rules);
    }

    /**
     * @param array $params
     * @return array
     * @throws \Propel\Runtime\Exception\PropelException
     */
    final protected function execute(array $params)
    {
        $mrt = MusicRoomTrackQuery::create()
            ->filterByTrackId($params['TrackId'])
            ->filterByRoomId($params['TrackId'])
            ->findOneOrCreate();

        $mrt->fromArray($params);
        $mrt->save();

        return [
            'Status' => 1
        ];
    }
}
