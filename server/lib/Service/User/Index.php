<?php

namespace Service\User;

use Service\X;
use Service\Base;
use Engine\Base\UserQuery;
use Engine\FacebookAccountQuery;
use Engine\Base\DeezerAccountQuery;
use Engine\Base\MusicPreferenceUserQuery;

final class Index extends Base
{
    /**
     * @param array $params
     * @return array|bool
     * @throws X
     */
    final protected function validate(array $params)
    {
        return [];
    }

    /**
     * @param array $params
     * @return array
     * @throws \Propel\Runtime\Exception\PropelException
     */
    final protected function execute(array $params)
    {
        $userId = $this->userId();

        if (!$userId) {
            throw new X([
                'Type'    => 'EMPTY_SESSION',
                'Fields'  => [ 'User' => 'NOT_FOUND' ],
                'Message' => 'User not found'
            ]);
        }

        $user = UserQuery::create()
            ->findOneById($userId);

        if (!$user) {
            throw new X([
                'Type'    => 'NOT_FOUND',
                'Fields'  => [ 'User' => 'NOT_FOUND' ],
                'Message' => "User with id: $userId not found"
            ]);
        }

        $fb = FacebookAccountQuery::create()
            ->findOneByUserId($user->getId());

        $fb = $fb ? $fb->toArray() : null;

        $deezer = DeezerAccountQuery::create()
            ->findOneByUserId($user->getId());

        $deezer = $deezer ? $deezer->toArray() : null;

        $mp = MusicPreferenceUserQuery::create()
            ->filterByUserId($user->getId())
            ->useMusicPreferenceQuery()
                ->withColumn('MusicPreference.Slug', 'Slug')
            ->endUse()
            ->select(['Slug'])
            ->find()
            ->toArray();

        $user = $user->toArray();

        unset($user['Password']);
        unset($user['UpdatedAt']);
        unset($user['CreatedAt']);

        return [
            'Status' => 1,
            'User' => $user + ['Preferences' => $mp],
            'FB' => $fb,
            'Deezer' => $deezer
        ];
    }
}
