<?php

namespace Service\User;

use Engine\FacebookAccountQuery;
use Engine\UserQuery;
use Service\SessionAPI\Jwt;
use Service\X;
use Service\Validator;
use Service\Base;
use Engine\Base\FacebookAccountQuery as EngineFacebookAccountQuery;

final class CreateFb extends Base
{
    use Jwt;
    /**
     * @param array $params
     * @return array|bool
     * @throws X
     */
    final protected function validate(array $params)
    {
        $rules = [
            'email'       => [ 'required', 'email', 'to_lc' ],
            'birthday'    => [ 'required', 'string' ],
            'first_name'  => [ 'required', 'string' ],
            'last_name'   => [ 'required', 'string' ],
            'link'   => [ 'required', 'string' ]
        ];

        return Validator::validate($params, $rules);
    }

    /**
     * @param array $params
     * @return array
     * @throws \Propel\Runtime\Exception\PropelException
     */
    final protected function execute(array $params)
    {
        $isNew = false;

        $fbAcc = EngineFacebookAccountQuery::create()
            ->findOneByEmail($params['email']);

        if (!$fbAcc) {
            $user = UserQuery::create()
            ->filterByEmail($params['email'])
            ->findOneOrCreate();

            $isNew = $user->isNew();
        
            if ($isNew) {
                $user->fromArray([
                    'Email' => $params['email'],
                    'Password' => '',
                    'Nickname' => $params['first_name']
                ]);

                $user->save();
            }   
        } else {
            $user = UserQuery::create()
                ->findOneById($fbAcc->getUserId());
        }

        $fb = FacebookAccountQuery::create()
            ->filterByEmail($params['email'])
            ->findOneOrCreate();

        $data = [
            'Email' => $params['email'],
            'Birthday' => strtotime($params['birthday']),
            'Nickname' => $params['first_name'],
            'FirstName' => $params['first_name'],
            'LastName' => $params['last_name'],
            'Link' => $params['link'],
            'UserId' => $user->getId()
        ];

        $fb->fromArray($data);

        $fb->save();

        $jwt = $this->createUserJwt($user);

        return [
            'Status' => 1,
            'JWT' => $jwt,
            'IsNew' => $isNew
        ];
    }
}
