<?php

namespace Service\User;

use Engine\User;
use Service\SessionAPI\Jwt;
use Service\X;
use Service\Validator;
use Service\Base;
use Engine\Base\MusicPreferenceUserQuery;
use Engine\Base\MusicPreferenceQuery;

final class MusicPrefs extends Base
{
    /**
     * @param array $params
     * @return array|bool
     * @throws X
     */
    final protected function validate(array $params)
    {
        $rules = [
            'Pref' => [ 'required', 'string' ]
        ];

        return Validator::validate($params, $rules);
    }

    /**
     * @param array $params
     * @return array
     * @throws \Propel\Runtime\Exception\PropelException
     */
    final protected function execute(array $params)
    {
        $userId = $this->userId();
        $pref = MusicPreferenceQuery::create()
            ->findOneBySlug($params['Pref']);

        $pref = MusicPreferenceUserQuery::create()
            ->filterByUserId($userId)
            ->filterByMusicPreference($pref)
            ->findOneOrCreate();

        $pref->isNew() ? $pref->save() : $pref->delete();

        $mp = MusicPreferenceUserQuery::create()
            ->filterByUserId($userId)
            ->useMusicPreferenceQuery()
                ->withColumn('MusicPreference.Slug', 'Slug')
            ->endUse()
            ->select(['Slug'])
            ->find()
            ->toArray();

        return [
            'Status' => 1,
            'Prefs' => $mp
        ];
    }
}
