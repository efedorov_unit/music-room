<?php

namespace Service\User;

use Engine\User;
use Service\X;
use Service\Validator;
use Service\Base;
use Engine\UserQuery;

final class ChangePass extends Base
{
    /**
     * @param array $params
     * @return array|bool
     * @throws X
     */
    final protected function validate(array $params)
    {
        $rules = [
            'Password' => [ 'required', 'string' ]
        ];

        return Validator::validate($params, $rules);
    }

    /**
     * @param array $params
     * @return array
     * @throws \Propel\Runtime\Exception\PropelException
     */
    final protected function execute(array $params)
    {
        $user = UserQuery::create()
            ->findOneById($this->userId());

        $user->setPassword(password_hash($params['Password'], PASSWORD_DEFAULT));
        $user->save();

        return [
            'Status' => 1
        ];
    }
}
