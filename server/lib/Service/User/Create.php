<?php

namespace Service\User;

use Engine\User;
use Service\SessionAPI\Jwt;
use Service\X;
use Service\Validator;
use Service\Base;

final class Create extends Base
{
    use Jwt;
    /**
     * @param array $params
     * @return array|bool
     * @throws X
     */
    final protected function validate(array $params)
    {
        $rules = [
            'Email'       => [ 'required', 'email', 'to_lc' ],
            'Nickname'    => [ 'required', 'string' ],
            'Password'    => [ 'required' ]
        ];

        return Validator::validate($params, $rules);
    }

    /**
     * @param array $params
     * @return array
     * @throws \Propel\Runtime\Exception\PropelException
     */
    final protected function execute(array $params)
    {
        $user = new User();

        $user->fromArray([
            'Nickname' => $params['Nickname'],
            'Email' => $params['Email'],
            'Password' => password_hash($params['Password'], PASSWORD_DEFAULT)
        ]);

        $user->save();

        $jwt = $this->createUserJwt($user);

        return [
            'Status' => 1,
            'JWT' => $jwt
        ];
    }
}
