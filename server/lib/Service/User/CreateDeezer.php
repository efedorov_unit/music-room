<?php

namespace Service\User;

use Service\X;
use Service\Validator;
use Service\Base;
use Engine\DeezerAccount;
use Engine\DeezerAccountQuery;

final class CreateDeezer extends Base
{
    /**
     * @param array $params
     * @return array|bool
     * @throws X
     */
    final protected function validate(array $params)
    {
        $rules = [
            'Nickname'    => [ 'required', 'string' ],
            'Birthday'    => [ 'required', 'string' ],
            'FirstName'  => [ 'required', 'string' ],
            'LastName'   => [ 'required', 'string' ],
            'Link'   => [ 'required', 'string' ]
        ];

        return Validator::validate($params, $rules);
    }

    /**
     * @param array $params
     * @return array
     * @throws \Propel\Runtime\Exception\PropelException
     */
    final protected function execute(array $params)
    {
        $userId = $this->userId();
        $params += ['Email' => '', 'UserId' => $userId];

        $deezer = DeezerAccountQuery::create()
            ->filterByUserId($userId)
            ->findOneOrCreate();

        $deezer->fromArray($params);

        $deezer->save();

        return [
            'Status' => 1
        ];
    }
}
