<?php

namespace Service\User;

use Engine\FacebookAccountQuery;
use Service\X;
use Service\Validator;
use Service\Base;

final class LinkWithFb extends Base
{
    /**
     * @param array $params
     * @return array|bool
     * @throws X
     */
    final protected function validate(array $params)
    {
        $rules = [
            'email'       => [ 'required', 'email', 'to_lc' ],
            'birthday'    => [ 'required', 'string' ],
            'first_name'  => [ 'required', 'string' ],
            'last_name'   => [ 'required', 'string' ],
            'link'   => [ 'required', 'string' ]
        ];

        return Validator::validate($params, $rules);
    }

    /**
     * @param array $params
     * @return array
     * @throws \Propel\Runtime\Exception\PropelException
     */
    final protected function execute(array $params)
    {
        $userId = $this->userId();

        $fb = FacebookAccountQuery::create()
            ->filterByUserId($userId)
            ->findOneOrCreate();

        $fb->fromArray([
            'Email' => $params['email'],
            'Birthday' => strtotime($params['birthday']),
            'Nickname' => $params['first_name'],
            'FirstName' => $params['first_name'],
            'LastName' => $params['last_name'],
            'Link' => $params['link'],
            'UserId' => $userId
        ]);

        $fb->save();

        return [
            'Status' => 1
        ];
    }
}
