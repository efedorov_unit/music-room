<?php

namespace Service\User;

use Engine\User;
use Service\X;
use Service\Base;
use Engine\UserQuery;
use Propel\Runtime\ActiveQuery\Criteria;
use Engine\MusicRoomUserQuery;

final class GetUsersList extends Base
{
    /**
     * @param array $params
     * @return array|bool
     * @throws X
     */
    final protected function validate(array $params)
    {
        return [];
    }

    /**
     * @param array $params
     * @return array
     * @throws \Propel\Runtime\Exception\PropelException
     */
    final protected function execute(array $params)
    {
        $userId = $this->userId();

        $users = UserQuery::create()
            ->filterById($userId, Criteria::NOT_EQUAL)
            ->find()
            ->toArray('Id');

        $mruLinks = MusicRoomUserQuery::create()
            ->filterByUserId(array_keys($users))
            ->find()
            ->toArray();

        $jope = [];
        foreach ($users as &$user) {
            $user['Rooms'] = [];
            foreach ($mruLinks as $mruLink) {
                if ($mruLink['UserId'] === $user['Id']) {
                    $user['Rooms'][] = $mruLink['MusicRoomId'];
                }
            }
            $jope[] = $user;
        }

        return [
            'Status' => 1,
            'Users' => $jope
        ];
    }
}
