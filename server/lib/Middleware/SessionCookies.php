<?php

namespace Middleware;

use Psr\Http\Message\RequestInterface as ReqInt;
use Psr\Http\Message\ResponseInterface as ResInt;

final class SessionCookies
{
    private $settings = [
        'name' => 'gold_advert_session',
        'lifetime' => '1 day',
        'path' => '/',
        'domain' => null,
        'secure' => false,
        'httponly' => true,
    ];

    public function __construct(array $settings)
    {
        $this->settings = $settings + $this->settings;
    }

    public function __invoke(ReqInt $req, ResInt $res, callable $next)
    {
        $this->loadSession();

        return $next($req, $res);
    }

    private function preSet(array &$settings)
    {
        $current = session_get_cookie_params();
        foreach ($settings as $key => &$value) {
            switch ($key) {
                case 'lifetime':
                    $value = (int)strtotime($value) ?: $current[$key];
                    break;
                case 'path':
                case 'domain':
                    $value = $value ?: $current[$key];
                    break;
                case 'name':
                    $value = preg_replace('/^[^a-zA-Z0-9\-\_\.]$/', '', $value);
                    break;
                case 'secure':
                case 'httponly':
                    $value = (bool)$value;
                    break;
                default:
                    throw new \Exception("SessionCookies wrong setting name: $key.");
                    break;
            }
        }

        return $settings;
    }

    private function loadSession()
    {
        if (session_status() == PHP_SESSION_ACTIVE) {
            return;
        }
        $settings = $this->preSet($this->settings);

        extract($settings);
        session_set_cookie_params($lifetime, $path, $domain, $secure, $httponly);
        session_name($name);
        session_cache_limiter(false);
        session_start();
    }
}
