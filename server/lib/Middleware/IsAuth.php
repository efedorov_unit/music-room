<?php

namespace Middleware;

use Psr\Http\Message\RequestInterface as ReqInt;
use Psr\Http\Message\ResponseInterface as ResInt;

use Utils\Session;

final class IsAuth
{
    public function __invoke(ReqInt $req, ResInt $res, callable $next)
    {
        if (\Utils\Session::get('UserId')) {
            $res = $next($req, $res);
        } else {
            $res = $res->write(json_encode(['Error' => ['Type' => 'ACCESS_DENIED']]));
        }

        return $res;
    }
}
