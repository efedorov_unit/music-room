<?php

namespace Middleware;

use Psr\Http\Message\RequestInterface as ReqInt;
use Psr\Http\Message\ResponseInterface as ResInt;
use Firebase\JWT\JWT;
use Utils\Session as UtilsSession;

final class JwtSession
{
    private $options = [
        'secret'    => '',
        'algorithm' => ['HS512'],
        'attribute' => 'session',
        'header'    => 'HTTP_AUTHORIZATION',
    ];

    public function __construct($options = [])
    {
        $this->options = $options + $this->options;
    }

    public function __invoke(ReqInt $req, ResInt $res, callable $next)
    {
        try {
            $headers = $req->getHeader($this->options['header']);
            $token = isset($headers[0]) ? $headers[0] : "";

            if ($token) {
                $decoded = JWT::decode(
                    $token,
                    $this->options['secret'],
                    (array) $this->options['algorithm']
                );

                UtilsSession::set('UserId', $decoded->id);

                $req = $req->withAttribute($this->options["attribute"], $decoded);
            }
        } catch (\Exception $exception) {
            // error_log($exception->getMessage());
            // $this->log(LogLevel::WARNING, $exception->getMessage(), [$token]);
            // return false;
        }

        return $next($req, $res);
    }
}
