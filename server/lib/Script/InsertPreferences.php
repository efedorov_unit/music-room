<?php

namespace Script;

use Engine\MusicPreferenceQuery;

class InsertPreferences extends Base
{
    protected $prefs = [
        'pop' => 'Pop',
        'rock' => 'Rock',
        'electro' => 'Electro',
        'rnb' => "R'n'B",
        'metal' => 'Metal',
        'rap' => 'Rap',
        'chill' => 'Chill',
        'jazz' => 'Jazz',
        'indie' => 'Indie',
        'folk' => 'Folk'
    ];

    protected function main()
    {
        foreach($this->prefs as $key => $value) {
            $mp = MusicPreferenceQuery::create()
                ->filterBySlug($key)
                ->findOneOrCreate();

            $mp->fromArray([
                'Slug' => $key,
                'Value' => $value
            ]);

            $mp->save();
        }
    }
}