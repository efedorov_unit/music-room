<?php

namespace Script;

use Service\X;
use Monolog\Logger;
use Monolog\Handler\StreamHandler;

const PID_DIR = '/tmp';

abstract class Base
{
    protected $name   = '';
    protected $log    = null;
    protected $config = [];

    public function __construct($name)
    {
        $this->config = require(__DIR__.'/../../etc/app-conf.php');
        $propel = require(__DIR__.'/../../etc/propel.php');
        \Engine::create($propel['propel']['database']['connections']['engine'], $this->config);
        $this->scriptName = $name;
        $this->log = new Logger($name);
        $this->log->pushHandler(
            new StreamHandler(__DIR__."/../logs/$name.log", Logger::DEBUG)
        );
    }

    final public function run()
    {
        $this->start();

        try {
            $this->main();
        } catch (X $e) {
            $error = json_encode($e->getError());
            $this->log->error(
                "ERROR: Caught Exception in script {$this->name} with error: {$error}"
            );
        } catch (\Exception $e) {
            $this->log->error(
                "ERROR: Caught Exception in script {$this->name} with message: {$e->getMessage()}"
            );
        }

        $this->finish();
    }

    /**
     * Start script create PID file
     *
     * @return void nothing to return
     */
    private function start()
    {
        if (function_exists('posix_kill')) {
            $this->log->info("START {$this->name} script");

            $pidFilePath = PID_DIR."/{$this->name}.pid";

            if (file_exists($pidFilePath)) {
                $pid = file_get_contents($pidFilePath);

                if (posix_kill((int)$pid, 0)) {
                    $this->log->info('Script already running. PID = ' . $pid);
                    exit();
                }

                $this->log->info('WARN: pid file exists, but process stoped: ' . $pid);
            }

            file_put_contents($pidFilePath, posix_getpid());
        }
    }

    /**
     * Finish script and remove PID file
     *
     * @return void nothing to return
     */
    private function finish()
    {
        $this->log->info("FINISH {$this->name} script");

        unlink(PID_DIR."/{$this->name}.pid");
    }

    protected function action($class)
    {
        return new $class([
            'log'    => $this->log,
            'config' => $this->config
        ]);
    }

    /**
     * Entry point for script
     *
     * @return void Nothing to return for cron script
     */
    abstract protected function main();
}
