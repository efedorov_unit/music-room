#import "DeezerManager.h"

#import "DeezerSession.h"

@implementation DeezerManager
@synthesize bridge = _bridge;

RCT_EXPORT_MODULE();

RCT_EXPORT_METHOD(connect:(RCTResponseSenderBlock)callback) {
  _globalBridge = _bridge;
  
  NSMutableArray* permissionsArray = [NSMutableArray array];
  [permissionsArray addObject:DeezerConnectPermissionBasicAccess];
  [permissionsArray addObject:DeezerConnectPermissionListeningHistory];
  
  [DeezerSession sharedSession].initCallback = callback;
  dispatch_async(dispatch_get_main_queue(), ^{
    [[DeezerSession sharedSession] connectToDeezerWithPermissions:permissionsArray];
  });
}

RCT_EXPORT_METHOD(getUser:(RCTResponseSenderBlock)callback) {
  _globalBridge = _bridge;
  
  [DZRUser objectWithIdentifier:@"me" requestManager:[DZRRequestManager defaultManager] callback:^(DZRObject *currentUser, NSError *error) {
    NSMutableArray *user = [NSMutableArray array];
    
        [currentUser
         valuesForKeyPaths:@[@"name", @"id", @"birthday", @"firstname", @"lastname", @"link"]
         withRequestManager:nil
         callback:^(NSDictionary *info, NSError *error) {
           [user addObject:info[@"id"]];
           [user addObject:info[@"name"]];
           [user addObject:info[@"birthday"]];
           [user addObject:info[@"firstname"]];
           [user addObject:info[@"lastname"]];
           [user addObject:info[@"link"]];
           
           callback(@[user]);
         }];
  }];
}

RCT_EXPORT_METHOD(isSessionValid:(RCTResponseSenderBlock)callback) {
  _globalBridge = _bridge;
  
  [[DeezerSession sharedSession] isSessionValid] ? [[DeezerSession sharedSession] getCurrentUserWithCallback:^() {
    callback(@[@YES]);
  }] : callback(@[@NO]);
}

RCT_REMAP_METHOD(getFavoritesTracks,
                 resolver:(RCTPromiseResolveBlock)resolve
                 rejecter:(RCTPromiseRejectBlock)reject) {
  [[DeezerSession sharedSession] getTracksWithResolver:resolve andRejecter:reject];
}

RCT_EXPORT_METHOD(getPlaylists:(RCTPromiseResolveBlock)resolve
                  rejecter:(RCTPromiseRejectBlock)reject) {
  [[DeezerSession sharedSession] getPlaylistsWithResolver:resolve andRejecter:reject];
}

RCT_EXPORT_METHOD(getPlaylistTracks:(NSString *)identifier
                  resolver:(RCTPromiseResolveBlock)resolve
                  rejecter:(RCTPromiseRejectBlock)reject) {
  [[DeezerSession sharedSession] getPlaylistTracks:identifier withResolver:resolve andRejecter:reject];
}

#pragma mark - Singleton methods

static RCTBridge* _globalBridge = nil;

+ (RCTBridge*)globalBridge {
  return _globalBridge;
}

@end
