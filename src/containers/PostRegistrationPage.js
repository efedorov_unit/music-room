import { connect } from 'react-redux';

import actions from './../actions';
import PostRegistrationPage from './../components/PostRegistrationPage';

function mapStateToProps({
    session
}) {
    return {
        actions,
        session
    };
}

export default connect(mapStateToProps)(PostRegistrationPage);
