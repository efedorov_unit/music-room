import { connect } from 'react-redux';

import Registration from './../components/Registration';
import actions from './../actions';

function mapStateToProps({
    session
}) {
    return {
        actions,
        session
    };
}

export default connect(mapStateToProps)(Registration);
