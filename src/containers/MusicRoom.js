import { connect } from 'react-redux';

import MusicRoom from './../components/MusicRoom';
import actions from './../actions';

function mapStateToProps({
    room
}) {
    return {
        actions,
        room
    };
}

export default connect(mapStateToProps)(MusicRoom);
