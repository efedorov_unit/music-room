import { connect } from 'react-redux';

import SearchResults from './../components/SearchResults';
import actions from './../actions';

function mapStateToProps({
    search,
    room
}) {
    return {
        actions,
        search,
        room
    };
}

export default connect(mapStateToProps)(SearchResults);
