import { connect } from 'react-redux';

import Home from './../components/Home';
import actions from './../actions';

function mapStateToProps({
    search,
    mainPage,
    room
}) {
    return {
        actions,
        search,
        mainPage,
        room
    };
}

export default connect(mapStateToProps)(Home);
