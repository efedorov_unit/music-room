import { connect } from 'react-redux';

import Loading from './../components/Loading';
import actions from './../actions';

function mapStateToProps({
}) {
    return {
        actions
    };
}

export default connect(mapStateToProps)(Loading);
