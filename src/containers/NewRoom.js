import { connect } from 'react-redux';

import NewRoom from './../components/NewRoom';
import actions from './../actions';

function mapStateToProps({
}) {
    return {
        actions
    };
}

export default connect(mapStateToProps)(NewRoom);
