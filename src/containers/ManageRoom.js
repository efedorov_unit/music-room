import { connect } from 'react-redux';

import ManageRoom from './../components/ManageRoom';
import actions from './../actions';

function mapStateToProps({
    room
}) {
    return {
        actions,
        room
    };
}

export default connect(mapStateToProps)(ManageRoom);
