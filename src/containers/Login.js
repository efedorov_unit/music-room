import { connect } from 'react-redux';

import Login from './../components/Login';
import actions from './../actions';

function mapStateToProps({
    session
}) {
    return {
        actions,
        session
    };
}

export default connect(mapStateToProps)(Login);
