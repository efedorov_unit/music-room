import { connect } from 'react-redux';

import Profile from './../components/Profile';
import actions from './../actions';

function mapStateToProps({
    user
}) {
    return {
        actions,
        user
    };
}

export default connect(mapStateToProps)(Profile);
