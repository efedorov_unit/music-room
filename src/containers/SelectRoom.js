import { connect } from 'react-redux';

import SelectRoom from './../components/SelectRoom';
import actions from './../actions';

function mapStateToProps({
    room
}) {
    return {
        actions,
        room
    };
}

export default connect(mapStateToProps)(SelectRoom);
