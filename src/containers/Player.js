import { connect } from 'react-redux';

import Player from './../components/Player';
import actions from './../actions';

function mapStateToProps({
    player
}) {
    return {
        actions,
        player
    };
}

export default connect(mapStateToProps)(Player);
