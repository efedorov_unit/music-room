import React, { Component } from 'react';
import { Provider } from 'react-redux';
import { createStackNavigator, createAppContainer } from 'react-navigation';

import LoginScreen from './containers/Login';
import LoadingScreen from './containers/Loading';
import HomeScreen from './containers/Home';
import PlayerScreen from './containers/Player';
import SearchResultScreen from './containers/SearchResults';
import ProfileScreen from './containers/Profile';
import NewRoomScreen from './containers/NewRoom';
import RegistrationScreen from './containers/Registration';
import PostRegistrationPageScreen from './containers/PostRegistrationPage';
import ManageRoomScreen from './containers/ManageRoom';
import SelectRoomScreen from './containers/SelectRoom';
import MusicRoomScreen from './containers/MusicRoom';

import store from './store';

console.disableYellowBox = true;

const RootStack = createStackNavigator(
    {
        Login         : LoginScreen,
        Loading       : LoadingScreen,
        Home          : HomeScreen,
        Player        : PlayerScreen,
        SearchResults : SearchResultScreen,
        Profile       : ProfileScreen,
        NewRoom       : NewRoomScreen,
        Registration  : RegistrationScreen,
        PostRegistration: PostRegistrationPageScreen,
        ManageRoom: ManageRoomScreen,
        SelectRoom: SelectRoomScreen,
        MusicRoom: MusicRoomScreen
    },
    {
        initialRouteName : 'Loading'
    }
);

const AppContainer = createAppContainer(RootStack);

export default class Root extends Component {
    render() {
        return (
            <Provider store={store}>
                <AppContainer />
            </Provider>
        );
    }
}
