import {
    GET_USER_INFO_SUCCESS,
    GET_USER_INFO_ERROR,
    UPDATE_MUSIC_PREFS_SUCCESS,
    UPDATE_MUSIC_PREFS_ERROR
} from './../actions/user';

const initialState = {
    data : {
        Id: 0,
        Email: '',
        Nickname: '',
        Preferences: []
    },
    fbInfo: null,
    deezerInfo : null,
    error : null
};

export default function user(state = initialState, action) {
    switch (action.type) {
        case GET_USER_INFO_SUCCESS:
            return { ...state, ...action };
        case GET_USER_INFO_ERROR:
        case UPDATE_MUSIC_PREFS_ERROR:
            return { ...state, error: action.error };
        case UPDATE_MUSIC_PREFS_SUCCESS:
            return { ...state, data: { ...state.data, Preferences: action.data } };
        default:
            return state;
    }
}
