import { combineReducers } from 'redux';

import init from './init';
import search from './search';
import mainPage from './mainPage';
import session from './session';
import user from './user';
import room from './room';
import player from './player';

export default combineReducers({
    init,
    search,
    mainPage,
    session,
    user,
    room,
    player
});
