import {
    LOGIN_VIA_FB_ERROR,
    LOGIN_VIA_EMAIL_ERROR,
    SESSION_CREATE_SUCCESS,
    LOGOUT_SUCCESS,
    FIRST_FACEBOOK_LOGIN,
    LINK_WITH_DEEZER_SUCCESS,
    LINK_WITH_DEEZER_ERROR,
    LINK_WITH_FACEBOOK_ERROR,
    LINK_WITH_FACEBOOK_SUCCESS
} from './../actions/login';

const initialState = {
    data : null,
    firstLogin: false,
    fbLinked: false,
    deezerLinked: false,
    error : {
        Fields: {}
    }
};

export default function login(state = initialState, action) {
    switch (action.type) {
        case LOGIN_VIA_EMAIL_ERROR:
        case LOGIN_VIA_FB_ERROR:
        case LINK_WITH_DEEZER_ERROR:
        case LINK_WITH_FACEBOOK_ERROR:
            return { ...state, error: action.error };
        case FIRST_FACEBOOK_LOGIN:
            return { ...state, firstLogin: true, fbLinked: true };
        case LINK_WITH_FACEBOOK_SUCCESS:
            return { ...state, fbLinked: true };
        case LINK_WITH_DEEZER_SUCCESS:
            return { ...state, deezerLinked: true };
        case SESSION_CREATE_SUCCESS:
            return { ...state, data: action.data };
        case LOGOUT_SUCCESS:
            return { ...initialState };
        default:
            return state;
    }
}
