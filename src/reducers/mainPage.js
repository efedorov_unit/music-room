import {
    DEEZER_DEFAULT_TRACKS_ERROR,
    DEEZER_DEFAULT_TRACKS_SUCCESS
} from './../actions/search';

const initialState = {
    defaultTracks : [],
    error : null
};

export default function mainPage(state = initialState, action) {
    switch (action.type) {
        case DEEZER_DEFAULT_TRACKS_SUCCESS:
            return { ...state, defaultTracks: action.data };
        case DEEZER_DEFAULT_TRACKS_ERROR:
            return { ...state, error: action.error };
        default:
            return state;
    }
}
