import {
    ADD_TO_QUEUE,
    HANDLE_SEEK,
    HANDLE_BACK,
    HANDLE_FORWARD,
    SET_DURATION,
    SET_TIME,
    HANDLE_PAUSE
} from './../actions/player';

const initialState = {
    tracks : [],
    paused : false,
    totalLength : 0,
    currentPosition : 0,
    selectedTrack : 0
};

export default function player(state = initialState, action) {
    switch (action.type) {
        case ADD_TO_QUEUE:
            return { ...state, tracks: action.tracks, selectedTrack: action.index };
        case HANDLE_SEEK:
        case HANDLE_BACK:
        case HANDLE_FORWARD:
        case SET_DURATION:
        case SET_TIME:
        case HANDLE_PAUSE:
            return { ...state, ...action.params };
        default:
            return state;
    }
}
