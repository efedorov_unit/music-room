import {
    CREATE_NEW_ROOM_ERROR,
    GET_ROOMS_SUCCESS,
    GET_ROOMS_ERROR,
    UPDATE_ROOM_SUCCESS,
    UPDATE_ROOM_ERROR,
    GO_RO_ROOM,
    GET_TRACKS_ROOM_ERROR,
    GET_TRACKS_ROOM_SUCCESS,
    UPDATE_ROWS,
    CLEAR,
    REMOVE_TRACK_ROOM_ERROR,
    REMOVE_TRACK_ROOM_SUCCESS
} from './../actions/room';

import {
    GET_USERS_LIST_ERROR,
    GET_USERS_LIST_SUCCESS
} from './../actions/user';

const initialState = {
    data : null,
    error: null,
    users: [],
    currRoom: 0,
    tracks: []
};

export default function room(state = initialState, action) {
    switch (action.type) {
        case CREATE_NEW_ROOM_ERROR:
        case GET_ROOMS_ERROR:
        case GET_USERS_LIST_ERROR:
        case UPDATE_ROOM_ERROR:
        case GET_TRACKS_ROOM_ERROR:
        case REMOVE_TRACK_ROOM_ERROR:
            return { ...state, error: action.error };
        case GET_ROOMS_SUCCESS:
            return { ...state, data: action.data };
        case GET_USERS_LIST_SUCCESS:
            return { ...state, users: action.users };
        case UPDATE_ROOM_SUCCESS:
            return { ...state, users: action.users };
        case GO_RO_ROOM:
            return { ...state, currRoom: action.roomId };
        case GET_TRACKS_ROOM_SUCCESS:
        case UPDATE_ROWS:
        case REMOVE_TRACK_ROOM_SUCCESS:
            return { ...state, tracks: action.tracks };
        case CLEAR:
            return { ...initialState };
        default:
            return state;
    }
}
