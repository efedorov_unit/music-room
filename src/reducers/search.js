import {
    DEEZER_SEARCH_ERROR,
    DEEZER_SEARCH_SUCCESS,
    SEARCH_TYPE
} from './../actions/search';

const initialState = {
    music : [],
    error : null,
    query : ''
};

export default function search(state = initialState, action) {
    switch (action.type) {
        case SEARCH_TYPE:
            return { ...state, query: action.search };
        case DEEZER_SEARCH_SUCCESS:
            return { ...state, music: action.data };
        case DEEZER_SEARCH_ERROR:
            return { ...state, error: action.error };
        default:
            return state;
    }
}
