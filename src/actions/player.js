export const ADD_TO_QUEUE = 'ADD_TO_QUEUE';
export const HANDLE_SEEK = 'HANDLE_SEEK';
export const HANDLE_BACK = 'HANDLE_BACK';
export const HANDLE_FORWARD = 'HANDLE_FORWARD';
export const SET_DURATION = 'SET_DURATION';
export const SET_TIME = 'SET_TIME';
export const HANDLE_PAUSE = 'HANDLE_PAUSE';

export function playTrack(tracks, index) {
    return async dispatch => {
        dispatch({ type : ADD_TO_QUEUE, tracks, index });
    };
}

export function seek(params) {
    return async dispatch => {
        dispatch({ type : HANDLE_SEEK, params });
    };
}

export function back(params) {
    return async dispatch => {
        dispatch({ type : HANDLE_BACK, params });
    };
}

export function forward(params) {
    return async dispatch => {
        dispatch({ type : HANDLE_FORWARD, params });
    };
}

export function setDuration(params) {
    return async dispatch => {
        dispatch({ type : SET_DURATION, params });
    };
}

export function setTime(params) {
    return async dispatch => {
        dispatch({ type : SET_TIME, params });
    };
}

export function pause(params) {
    return async dispatch => {
        dispatch({ type : HANDLE_PAUSE, params });
    };
}
