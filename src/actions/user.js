import api from './../apiSingleton';

export const GET_USER_INFO_SUCCESS = 'GET_USER_INFO_SUCCESS';
export const GET_USER_INFO_ERROR = 'GET_USER_INFO_ERROR';

export const UPDATE_MUSIC_PREFS_SUCCESS = 'UPDATE_MUSIC_PREFS_SUCCESS';
export const UPDATE_MUSIC_PREFS_ERROR = 'UPDATE_MUSIC_PREFS_ERROR';

export const UPDATE_PASS_ERROR = 'UPDATE_PASS_ERROR';
export const UPDATE_PASS_SUCCESS = 'UPDATE_PASS_SUCCESS';

export const GET_USERS_LIST_ERROR = 'GET_USERS_LIST_ERROR';
export const GET_USERS_LIST_SUCCESS = 'GET_USERS_LIST_SUCCESS';

export function getUser() {
    return async (dispatch, getState) => {
        const { session: { data } } = getState();
        const jwt = data && data.jwt ? data.jwt : '';
        const res = await api.user.index(jwt);

        if (!res.Status) {
            return dispatch({ type: GET_USER_INFO_ERROR, error: res.Message });
        }

        dispatch({
            type : GET_USER_INFO_SUCCESS,
            data : res.User,
            fbInfo : res.FB,
            deezerInfo : res.Deezer
        });
    };
}

export function setPref(pref) {
    return async (dispatch, getState) => {
        const { session: { data } } = getState();
        const jwt = data && data.jwt ? data.jwt : '';
        const res = await api.user.setPref({ Pref: pref }, jwt);

        if (!res.Status) {
            return dispatch({ type: UPDATE_MUSIC_PREFS_ERROR, error: res.Message });
        }

        dispatch({
            type : UPDATE_MUSIC_PREFS_SUCCESS,
            data : res.Prefs
        });
    };
}

export function changePass(pass) {
    return async (dispatch, getState) => {
        const { session: { data } } = getState();
        const jwt = data && data.jwt ? data.jwt : '';

        const res = await api.user.changePass({ Password: pass }, jwt);

        if (!res.Status) {
            dispatch({ type: UPDATE_PASS_ERROR, error: res.Message });

            return false;
        }

        dispatch({ type : UPDATE_PASS_SUCCESS });

        return true;
    };
}

export function getUsers() {
    return async (dispatch, getState) => {
        const { session: { data } } = getState();
        const jwt = data && data.jwt ? data.jwt : '';
        const res = await api.user.getUsers(jwt);

        if (!res.Status) {
            return dispatch({ type: GET_USERS_LIST_ERROR, error: res.Message });
        }

        dispatch({ type : GET_USERS_LIST_SUCCESS, users: res.Users });
    };
}
