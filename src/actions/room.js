import api from './../apiSingleton';

export const CREATE_NEW_ROOM_SUCCESS = 'CREATE_NEW_ROOM_SUCCESS';
export const CREATE_NEW_ROOM_ERROR = 'CREATE_NEW_ROOM_ERROR';

export const GET_ROOMS_SUCCESS = 'GET_ROOMS_SUCCESS';
export const GET_ROOMS_ERROR = 'GET_ROOMS_ERROR';

export const UPDATE_ROOM_SUCCESS = 'UPDATE_ROOM_SUCCESS';
export const UPDATE_ROOM_ERROR = 'UPDATE_ROOM_ERROR';

export const ADD_TRACK_ROOM_ERROR = 'ADD_TRACK_ROOM_ERROR';
export const ADD_TRACK_ROOM_SUCCESS = 'ADD_TRACK_ROOM_SUCCESS';

export const GO_RO_ROOM = 'GO_RO_ROOM';
export const UPDATE_ROWS = 'UPDATE_ROWS';

export const GET_TRACKS_ROOM_ERROR = 'GET_TRACKS_ROOM_ERROR';
export const GET_TRACKS_ROOM_SUCCESS = 'GET_TRACKS_ROOM_SUCCESS';

export const CLEAR = 'CLEAR';

export const REMOVE_TRACK_ROOM_ERROR = 'REMOVE_TRACK_ROOM_ERROR';
export const REMOVE_TRACK_ROOM_SUCCESS = 'REMOVE_TRACK_ROOM_SUCCESS';

export function createRoom(room) {
    return async (dispatch, getState) => {
        const { session: { data } } = getState();
        const jwt = data && data.jwt ? data.jwt : '';
        const res = await api.room.create(room, jwt);

        if (!res.Status) {
            dispatch({ type: CREATE_NEW_ROOM_ERROR, error: res.Message });

            return false;
        }

        dispatch({ type : CREATE_NEW_ROOM_SUCCESS });

        return true;
    };
}

export function getRooms() {
    return async (dispatch, getState) => {
        const { session: { data } } = getState();
        const jwt = data && data.jwt ? data.jwt : '';
        const res = await api.room.index(jwt);

        if (!res.Status) {
            return dispatch({ type: GET_ROOMS_ERROR, error: res.Message });
        }

        dispatch({ type : GET_ROOMS_SUCCESS, data: res.Rooms });
    };
}

export function showRooms() {
    return async (dispatch, getState) => {
        const { session: { data } } = getState();
        const jwt = data && data.jwt ? data.jwt : '';
        const res = await api.room.showRooms(jwt);

        if (!res.Status) {
            return dispatch({ type: GET_ROOMS_ERROR, error: res.Message });
        }

        dispatch({ type : GET_ROOMS_SUCCESS, data: res.Rooms });
    };
}

export function updateRoomForUser(params) {
    return async (dispatch, getState) => {
        const { session: { data } } = getState();
        const jwt = data && data.jwt ? data.jwt : '';
        const res = await api.room.update(params, jwt);

        if (!res.Status) {
            return dispatch({ type: UPDATE_ROOM_ERROR, error: res.Message });
        }

        dispatch({ type : UPDATE_ROOM_SUCCESS, users: res.Users });
    };
}

export function addToRoom(params) {
    return async (dispatch, getState) => {
        const { session: { data } } = getState();
        const jwt = data && data.jwt ? data.jwt : '';
        const res = await api.room.addTrack(params, jwt);

        if (!res.Status) {
            return dispatch({ type: ADD_TRACK_ROOM_ERROR, error: res.Message });
        }

        dispatch({ type : ADD_TRACK_ROOM_SUCCESS });
    };
}

export function getRoomTracks(id) {
    return async (dispatch, getState) => {
        const { session: { data } } = getState();
        const jwt = data && data.jwt ? data.jwt : '';
        const res = await api.room.getTracks({ RoomId: id }, jwt);

        if (!res.Status) {
            return dispatch({ type: GET_TRACKS_ROOM_ERROR, error: res.Message });
        }

        dispatch({ type : GET_TRACKS_ROOM_SUCCESS, tracks: res.Tracks });
    };
}

export function goToRoom(id) {
    return async dispatch => {
        dispatch({ type: GO_RO_ROOM, roomId: id });
    };
}

export function listSort(tracks) {
    return async dispatch => {
        dispatch({ type: UPDATE_ROWS, tracks });
    };
}

export function clear() {
    return async dispatch => {
        dispatch({ type: CLEAR });
    };
}

export function removeTrack(params) {
    return async (dispatch, getState) => {
        const { session: { data } } = getState();
        const jwt = data && data.jwt ? data.jwt : '';
        const res = await api.room.removeTrack(params, jwt);

        if (!res.Status) {
            return dispatch({ type: REMOVE_TRACK_ROOM_ERROR, error: res.Message });
        }

        dispatch({ type : REMOVE_TRACK_ROOM_SUCCESS, tracks: res.Tracks });
    };
}

export function removeTrackByEvent(trackId) {
    return async (dispatch, getState) => {
        const { room: { tracks } } = getState();

        const filtered = tracks.map(track => {
            if (track.TrackId !== trackId) {
                return track;
            }
        }).filter(t => t);

        dispatch({ type : UPDATE_ROWS, tracks: filtered });
    };
}
