import api from './../apiSingleton';
import { saveJwt } from './../utils';

export const REGISTRATION_SUCCESS = 'REGISTRATION_SUCCESS';
export const REGISTRATION_ERROR = 'REGISTRATION_ERROR';

export function registration(user) {
    return async dispatch => {
        const res = await api.registration.index(user);

        if (!res.Status) {
            dispatch({ type: REGISTRATION_ERROR, error: res.Error });
        }

        saveJwt(dispatch, res.JWT);
    };
}
