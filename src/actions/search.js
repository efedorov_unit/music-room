import api from './../apiSingleton';

export const DEEZER_SEARCH_ERROR = 'DEEZER_SEARCH_ERROR';
export const DEEZER_SEARCH_SUCCESS = 'DEEZER_SEARCH_SUCCESS';
export const SEARCH_TYPE = 'SEARCH_TYPE';

export const DEEZER_DEFAULT_TRACKS_ERROR = 'DEEZER_DEFAULT_TRACKS_ERROR';
export const DEEZER_DEFAULT_TRACKS_SUCCESS = 'DEEZER_DEFAULT_TRACKS_SUCCESS';

export function searchCall(search) {
    return async dispatch => {
        const res = await api.apiClient.deezerApi({ service: 'search', params: { q: search, readable: true } });

        if (!res.Status) {
            dispatch({ type: DEEZER_SEARCH_ERROR, error: res.Message });
        }

        dispatch({
            type : DEEZER_SEARCH_SUCCESS,
            data : res.data
        });
    };
}

export function searchQuery(search) {
    return dispatch => {
        dispatch({
            type : SEARCH_TYPE,
            search
        });
    };
}

export function defaultTracks() {
    return async dispatch => {
        const res = await api.apiClient.deezerApi({ service: 'radio', id: '42042', method: 'tracks' });

        if (!res.Status) {
            dispatch({ type: DEEZER_DEFAULT_TRACKS_ERROR, error: res.Message });
        }

        const tracks = res.data.map(track => {
            if (!track.readable) {
                return;
            }

            return track;
        }).filter(track => track);

        dispatch({
            type : DEEZER_DEFAULT_TRACKS_SUCCESS,
            data : tracks
        });
    };
}
