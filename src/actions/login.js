import { AsyncStorage } from 'react-native';

import api from './../apiSingleton';
import { saveJwt } from './../utils';

export const LOGIN_VIA_FB_ERROR = 'LOGIN_VIA_FB_ERROR';
export const LOGIN_VIA_EMAIL_ERROR = 'LOGIN_VIA_EMAIL_ERROR';

export const SESSION_CREATE_SUCCESS = 'SESSION_CREATE_SUCCESS';

export const LOGOUT_SUCCESS = 'LOGOUT_SUCCESS';

export const FIRST_FACEBOOK_LOGIN = 'FIRST_FACEBOOK_LOGIN';

export const LINK_WITH_DEEZER_SUCCESS = 'LINK_WITH_DEEZER_SUCCESS';
export const LINK_WITH_DEEZER_ERROR = 'LINK_WITH_DEEZER_ERROR';

export const LINK_WITH_FACEBOOK_ERROR = 'LINK_WITH_FACEBOOK_ERROR';
export const LINK_WITH_FACEBOOK_SUCCESS = 'LINK_WITH_FACEBOOK_SUCCESS';

export function loginViaFB({ accessToken }) {
    return async dispatch => {
        const fbRes = await api.apiClient.fbUserInfo({ accessToken });

        if (!fbRes.Status) {
            dispatch({ type: LOGIN_VIA_FB_ERROR, error: fbRes });
        }

        const res = await api.registration.createFb(fbRes);

        if (!res.Status) {
            dispatch({ type: LOGIN_VIA_FB_ERROR, error: res.Message });
        }

        if (res.IsNew) {
            dispatch({ type: FIRST_FACEBOOK_LOGIN });
        }

        saveJwt(dispatch, res.JWT);
    };
}

export function loginViaEmail({ email, password }) {
    return async dispatch => {
        const res = await api.login.index({ Email: email, Password: password });

        if (!res.Status) {
            dispatch({ type: LOGIN_VIA_EMAIL_ERROR, error: res.Error });
        }

        saveJwt(dispatch, res.JWT);
    };
}

export function linkWithDeezer(user) {
    return async (dispatch, getState) => {
        const { session: { data } } = getState();
        const jwt = data && data.jwt ? data.jwt : '';
        const res = await api.registration.linkWithDeezer(user, jwt);

        if (!res.Status) {
            dispatch({ type: LINK_WITH_DEEZER_ERROR, error: res.Error });
        }

        dispatch({ type: LINK_WITH_DEEZER_SUCCESS });
    };
}

export function linkWithFB({ accessToken }) {
    return async (dispatch, getState) => {
        const fbRes = await api.apiClient.fbUserInfo({ accessToken });

        if (!fbRes.Status) {
            dispatch({ type: LOGIN_VIA_FB_ERROR, error: fbRes });
        }

        const { session: { data } } = getState();
        const jwt = data && data.jwt ? data.jwt : '';
        const res = await api.registration.linkWithFB(fbRes, jwt);

        if (!res.Status) {
            dispatch({ type: LINK_WITH_FACEBOOK_ERROR, error: res.Error });
        }

        dispatch({ type: LINK_WITH_FACEBOOK_SUCCESS });
    };
}

export function restoreFromJWT(jwt) {
    return async dispatch => {
        saveJwt(dispatch, jwt);
    };
}

export function logout() {
    return async dispatch => {
        await api.login.logout();
        await AsyncStorage.removeItem('JWT');

        dispatch({ type : LOGOUT_SUCCESS });
    };
}
