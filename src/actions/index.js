import * as search from './search';
import * as login from './login';
import * as registration from './registration';
import * as user from './user';
import * as room from './room';
import * as player from './player';

export default {
    ...search,
    ...login,
    ...registration,
    ...user,
    ...room,
    ...player
};
