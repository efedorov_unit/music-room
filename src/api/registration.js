import Base from './base';

export default class Registration extends Base {
    index(params) {
        return this.apiClient.post('user', params);
    }

    createFb(params) {
        return this.apiClient.post('user/fb', params);
    }

    linkWithDeezer(params, jwt = '') {
        return this.apiClient.post('user/deezer', params, jwt);
    }

    linkWithFB(params, jwt = '') {
        return this.apiClient.post('user/link_fb', params, jwt);
    }
}
