import Base from './base';

export default class User extends Base {
    index(jwt = '') {
        return this.apiClient.get('user', {}, {}, jwt);
    }

    setPref(params, jwt = '') {
        return this.apiClient.post('user/music_pref', params, jwt);
    }

    changePass(params, jwt = '') {
        return this.apiClient.post('user/password', params, jwt);
    }

    getUsers(jwt) {
        return this.apiClient.get('user/all', {}, {}, jwt);
    }
}
