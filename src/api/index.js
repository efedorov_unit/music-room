import ApiClient from './apiClient';
import Login from './login';
import Registration from './registration';
import User from './user';
import Room from './room';

export default function ({ apiPrefix }) {
    const api = new ApiClient({ prefix: apiPrefix });

    return {
        apiClient : api,
        login : new Login({ apiClient: api }),
        registration : new Registration({ apiClient: api }),
        user : new User({ apiClient: api }),
        room : new Room({ apiClient: api })
    };
}
