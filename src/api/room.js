import Base from './base';

export default class Room extends Base {
    index(jwt = '') {
        return this.apiClient.get('music_room', {}, {}, jwt);
    }

    showRooms(jwt = '') {
        return this.apiClient.get('music_room/show_rooms', {}, {}, jwt);
    }

    create(params, jwt = '') {
        return this.apiClient.post('music_room', params, jwt);
    }

    update(params, jwt = '') {
        return this.apiClient.post('music_room/update', params, jwt);
    }

    addTrack(params, jwt = '') {
        return this.apiClient.post('music_room/add_track', params, jwt);
    }

    getTracks(params, jwt = '') {
        return this.apiClient.get('music_room/show', {}, params, jwt);
    }

    removeTrack(params, jwt = '') {
        return this.apiClient.post('music_room/remove', params, jwt);
    }
}
