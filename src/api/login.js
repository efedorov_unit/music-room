import Base from './base';

export default class Login extends Base {
    index(params) {
        return this.apiClient.post('sessions', params);
    }

    logout(params) {
        return this.apiClient.delete('sessions', params);
    }
}
