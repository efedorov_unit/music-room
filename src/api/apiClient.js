import queryString from 'query-string';

export default class ApiClient {
    constructor({ prefix = 'api/v1' } = {}) {
        this.prefix = prefix;
    }

    async get(requestUrl, payload = {}, params = {}, jwt = '') {
        const res = await this.request({
            url    : requestUrl,
            method : 'get',
            body   : payload,
            params,
            jwt
        });

        return res;
    }

    put(requestUrl, payload = {}, jwt = '') {
        return this.request({
            url    : requestUrl,
            method : 'put',
            body   : payload,
            jwt
        });
    }

    post(requestUrl, payload = {}, jwt = '') {
        return this.request({
            url    : requestUrl,
            method : 'post',
            body   : payload,
            jwt
        });
    }

    delete(requestUrl, jwt = '') {
        return this.request({
            url    : requestUrl,
            method : 'delete',
            jwt
        });
    }

    async request({ url, method, params = {}, body, jwt }) {
        const urlWithQuery = `${url}?${queryString.stringify(params)}`;
        const init = body instanceof FormData
            ? this._makeFormDataRequest(method, body, jwt)
            : this._makeJsonRequest(method, body, jwt);

        try {
            const requrl = `http://localhost:8000/${this.prefix}/${urlWithQuery}`;
            const res = await fetch(requrl, init);

            if (res.status >= 400) {
                throw new Error('Bad response from server');
            }

            const data = await res.json();

            return data;
        } catch (error) {
            return {
                Status  : 0,
                Message : error.message,
                Error   : {}
            };
        }
    }

    async deezerApi({ service = '', id = '', method = '', params = {} }) {
        const reqUrl = 'https://api.deezer.com/' +
            `${service}${id ? `/${id}` : ''}${method ? `/${method}` : ''}/?${queryString.stringify(params)}`;
        const init = {
            method  : 'GET',
            headers : {
                Accept : 'application/json'
            }
        };

        try {
            const res = await fetch(reqUrl, init);

            if (res.status >= 400) {
                throw new Error('Bad response from server');
            }

            const data = await res.json();

            return data;
        } catch (error) {
            return {
                Status  : 0,
                Message : error.message,
                Error   : {}
            };
        }
    }

    async fbUserInfo({ accessToken = '' }) {
        const reqUrl = 'https://graph.facebook.com/v3.2/me?fields=email,birthday,first_name,last_name,link';
        const init = {
            method  : 'GET',
            headers : {
                Accept : 'application/json',
                Authorization: `Bearer ${accessToken}`
            }
        };

        try {
            const res = await fetch(reqUrl, init);

            if (res.status >= 400) {
                throw new Error('Bad response from server');
            }

            const data = await res.json();

            return data;
        } catch (error) {
            console.error(error);

            return {
                Status  : 0,
                Message : error.message,
                Error   : {}
            };
        }
    }

    _makeJsonRequest(method, body, jwt) {
        const init = {
            method,
            headers : {
                Accept         : 'application/json',
                'Content-type' : 'application/json'
            }
        };

        if (jwt) {
            init.headers.Authorization = jwt;
        }

        if (method !== 'get' && method !== 'head') {
            init.body = this.locale
                ? JSON.stringify({ ...body, Locale: this.locale })
                : JSON.stringify(body);
        }

        return init;
    }

    _makeFormDataRequest(method, body, jwt) {
        const init = {
            method,
            body,
            headers : {
                Accept : 'application/json'
            }
        };

        if (jwt) {
            init.headers.Authorization = jwt;
        }

        return init;
    }
}
