import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { StyleSheet, View, Platform, NativeModules, Dimensions, DeviceEventEmitter } from 'react-native';
import { Text, Button } from 'react-native-elements';
import { LoginButton, AccessToken } from 'react-native-fbsdk';

import DeezerButton from './login/DeezerButton';

const SCREEN_HEIGHT = Dimensions.get('window').height;
const os = Platform.OS;

export default class PostRegistrationPage extends Component {
    static propTypes = {
        actions    : PropTypes.object.isRequired,
        dispatch   : PropTypes.func.isRequired,
        navigation : PropTypes.object.isRequired,
        session    : PropTypes.object.isRequired
    }

    async componentDidMount() {
        const { dispatch, actions } = this.props;

        DeviceEventEmitter.addListener('deezerLoginFinished', async user => {
            user.Birthday = new Date(user.Birthday); // eslint-disable-line

            await dispatch(actions.linkWithDeezer(user));
        });
    }

    componentDidUpdate() {
        const { session: { deezerLinked, fbLinked }, navigation } = this.props;

        if (deezerLinked && fbLinked) {
            navigation.navigate('Home');
        }
    }

    handleFBLogin = async (error) => {
        if (error) {
            this.setState({ currErr: 'FB_AUTH_EXCEPTION' });

            return;
        }
        const { actions, dispatch } = this.props;
        const token = await AccessToken.getCurrentAccessToken();

        await dispatch(actions.linkWithFB({ accessToken: token.accessToken }));
    };

    handleAndroidDeezerAuth = async () => {
        await NativeModules.DeezerModule.authorize();
    }

    handleIOSDeezerAuth = async () => {
        await NativeModules.DeezerManager.connect(this.handleIOSAuthResponse);
    }

    handleIOSAuthResponse = async (res) => {
        if (res) {
            await NativeModules.DeezerManager.getUser(this.getUserInfo);
        }
    }

    handleContinue = () => {
        this.props.navigation.navigate('Home');
    }

    getUserInfo = async (user) => {
        const Nickname = user[1];
        const Birthday = user[2];
        const FirstName = user[3];
        const LastName = user[4];
        const Link = user[5];
        const { dispatch, actions } = this.props;

        await dispatch(actions.linkWithDeezer({ Nickname, Birthday, FirstName, LastName, Link }));
    };

    static navigationOptions = {
        header : null
    };

    render() {
        const { fbLinked, deezerLinked } = this.props.session;

        return (
            <View style={styles.container}>
                <View>
                    <Text h4 style={styles.white}>Link with social pages</Text>
                </View>
                {
                    !deezerLinked &&
                    <View style={styles.socialContainer}>
                        <Text style={{ ...styles.white, ...styles.title  }}>Deezer</Text>
                        {
                            os === 'android' && <DeezerButton onPress={this.handleAndroidDeezerAuth} />
                        }
                        {
                            os === 'ios' && <DeezerButton onPress={this.handleIOSDeezerAuth} />
                        }
                    </View>
                }
                {
                    !fbLinked &&
                    <View style={styles.socialContainer}>
                        <Text style={{ ...styles.white, ...styles.title  }}>Facebook</Text>
                        <LoginButton
                            style={styles.fbBtn}
                            onLoginFinished={this.handleFBLogin}
                            readPermissions={[ 'public_profile', 'email', 'user_birthday', 'user_link' ]}
                        />
                    </View>
                }
                <View style={styles.socialContainer}>
                    <Button
                        title='Continue'
                        buttonStyle={{ backgroundColor: 'rgba(213, 100, 140, 1)', marginTop: 50 }}
                        onPress={this.handleContinue}
                    />
                </View>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container : {
        backgroundColor: 'rgba(47,44,60,1)',
        minHeight: SCREEN_HEIGHT,
        flex : 1,
        alignItems      : 'center',
        justifyContent  : 'center'
    },
    socialContainer: {
        width: '70%',
        textAlign: 'center',
        marginTop: 30
    },
    heading: {
        marginLeft: 10,
        color: '#fff'
    },
    white: {
        color: '#fff'
    },
    title: {
        textAlign: 'center',
        marginBottom: 10,
        fontSize: 20
    },
    fbBtn : {
        width  : '100%',
        height : 30
    }
});
