import React, { Component } from 'react';
import PropTypes from 'prop-types';
import LIVR from 'livr';
import { StackActions, NavigationActions } from 'react-navigation';

import {
    StyleSheet,
    View,
    ScrollView,
    Dimensions
} from 'react-native';
import { LoginButton, AccessToken } from 'react-native-fbsdk';
import {
    Input,
    Icon,
    ThemeProvider,
    Button
} from 'react-native-elements';

const SCREEN_WIDTH = Dimensions.get('window').width;

export default class Login extends Component {
    static propTypes = {
        navigation : PropTypes.object.isRequired,
        actions    : PropTypes.object.isRequired,
        dispatch   : PropTypes.func.isRequired,
        session    : PropTypes.object.isRequired
    };

    state = {
        errors : {
            FB_AUTH_EXCEPTION : 'Something went wrong with FB auth'
        },
        currErr    : null,
        email      : '',
        password   : '',
        validation : {
            fields : {},
            errors : {
                REQUIRED    : 'This field is required',
                WRONG_EMAIL : 'Wrong email format',
                TOO_SHORT   : 'Password must be at least 6 characters',
                NOT_FOUND   : 'User with this email not found'
            }
        }
    }

    componentDidUpdate() {
        const { navigation, session } = this.props;

        if (session.data) {
            const resetAction = StackActions.reset({
                index: 0,
                actions: [ NavigationActions.navigate({ routeName: session.firstLogin ? 'PostRegistration' : 'Home' }) ]
            });

            navigation.dispatch(resetAction);
        }
    }

    componentWillUnmount() {
        this.setState({ email: '', password: '' });
    }

    handleEmailLogin = async () => {
        const { dispatch, actions } = this.props;
        const { email, password } = this.state;

        if (this.validate({ email, password })) {
            this.setState({ validation: { ...this.state.validation, fields: {} } });

            await dispatch(actions.loginViaEmail({ email, password }));
        }
    }

    handleFBLogin = async (error) => {
        if (error) {
            this.setState({ currErr: 'FB_AUTH_EXCEPTION' });

            return;
        }
        const { actions, dispatch } = this.props;
        const token = await AccessToken.getCurrentAccessToken();

        await dispatch(actions.loginViaFB({ accessToken: token.accessToken }));
    };

    handleInput = (field, value) => {
        this.setState({ [field]: value });
    }

    handleSignUp = () => {
        this.props.navigation.navigate('Registration');
    }

    validate(data) {
        const validationRules = {
            email    : [ 'trim', 'required', 'email', 'to_lc', { 'max_length': 128 } ],
            password : [ 'required', { 'min_length': 6 } ]
        };

        const validator = new LIVR.Validator(validationRules);
        const validateResult = validator.validate(data);

        if (!validateResult) {
            this.setState({
                validation : {
                    ...this.state.validation,
                    fields : validator.getErrors()
                }
            });
        }

        return validateResult;
    }

    static navigationOptions = {
        header : null
    };

    render() {
        const { session } = this.props;
        const { email, password, validation } = this.state;
        const errFields = { email: '', password: '', ...validation.fields, ...session.error.Fields };

        return (
            <ScrollView
                style={styles.container}
                keyboardShouldPersistTaps='handled'
                contentContainerStyle={{
                    flex: 1,
                    alignItems: 'center',
                    justifyContent: 'center'
                }}
            >
                <ThemeProvider
                    theme={{
                        Input: {
                            containerStyle: {
                                width: SCREEN_WIDTH - 50
                            },
                            inputContainerStyle: {
                                borderRadius: 40,
                                borderWidth: 1,
                                borderColor: '#fff',
                                height: 50,
                                marginVertical: 10
                            },
                            placeholderTextColor: '#fff',
                            inputStyle: {
                                marginLeft: 10,
                                color: 'white'
                            },
                            keyboardAppearance: 'light',
                            blurOnSubmit: false
                        }
                    }}
                >
                    <View>
                        <View
                            style={{
                                backgroundColor: 'rgba(47,44,60,1)',
                                width: SCREEN_WIDTH,
                                alignItems: 'center',
                                paddingBottom: 30
                            }}
                        >
                            <Input
                                leftIcon={
                                    <Icon
                                        name='email-outline'
                                        type='material-community'
                                        size={25}
                                        color='#fff'
                                    />
                                }
                                placeholder='Email'
                                autoCapitalize='none'
                                autoCorrect={false}
                                keyboardType='email-address'
                                returnKeyType='next'
                                ref={input => (this.email2Input = input)}
                                onSubmitEditing={() => {
                                    this.password2Input.focus();
                                }}
                                onChangeText={this.handleInput.bind(this, 'email')}
                                value={email}
                                errorStyle={styles.error}
                                errorMessage={validation.errors[errFields.email]}
                            />
                            <Input
                                leftIcon={
                                    <Icon
                                        name='lock'
                                        type='simple-line-icon'
                                        size={25}
                                        color='#fff'
                                    />
                                }
                                placeholder='Password'
                                autoCapitalize='none'
                                secureTextEntry={true}
                                autoCorrect={false}
                                keyboardType='default'
                                returnKeyType='done'
                                blurOnSubmit
                                ref={input => (this.password2Input = input)}
                                onChangeText={this.handleInput.bind(this, 'password')}
                                value={password}
                                errorStyle={styles.error}
                                errorMessage={validation.errors[errFields.password]}
                            />
                        </View>
                        <View
                            style={{
                                marginTop: 20,
                                width: SCREEN_WIDTH - 70,
                                marginLeft: 40
                            }}
                        >
                            <Button
                                title='Continue'
                                buttonStyle={{ backgroundColor: 'rgba(213, 100, 140, 1)' }}
                                onPress={this.handleEmailLogin}
                            />
                            <Button
                                containerStyle={{ marginTop: 20 }}
                                buttonStyle={{ width: '100%', backgroundColor: 'rgba(213, 100, 140, 1)' }}
                                onPress={this.handleSignUp}
                                title='Sign up'
                            />
                        </View>
                        <View
                            style={{
                                marginTop: 20,
                                width: SCREEN_WIDTH - 70,
                                marginLeft: 40
                            }}
                        >
                            <LoginButton
                                style={{ width: '100%', height: 30 }}
                                onLoginFinished={this.handleFBLogin}
                                readPermissions={[ 'public_profile', 'email', 'user_birthday', 'user_link' ]}
                            />
                        </View>
                    </View>
                </ThemeProvider>
            </ScrollView>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        backgroundColor: 'rgba(47,44,60,1)'
    },
    headerContainer: {
        justifyContent: 'center',
        alignItems: 'center',
        padding: 40,
        backgroundColor: 'rgba(47,44,60,1)'
    },
    heading: {
        color: 'white',
        marginTop: 10,
        fontSize: 22,
        fontWeight: 'bold'
    },
    contentView: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center'
    },
    triangleLeft: {
        position: 'absolute',
        left: -20,
        bottom: 0,
        width: 0,
        height: 0,
        borderRightWidth: 20,
        borderRightColor: 'white',
        borderBottomWidth: 25,
        borderBottomColor: 'transparent',
        borderTopWidth: 25,
        borderTopColor: 'transparent'
    },
    triangleRight: {
        position: 'absolute',
        right: -20,
        top: 0,
        width: 0,
        height: 0,
        borderLeftWidth: 20,
        borderLeftColor: 'white',
        borderBottomWidth: 25,
        borderBottomColor: 'transparent',
        borderTopWidth: 25,
        borderTopColor: 'transparent'
    },
    inputContainerStyle: {
        marginTop: 16,
        width: '90%'
    }
});
