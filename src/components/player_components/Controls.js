import React, { Component } from 'react';
import PropTypes from 'prop-types';

import {
    View,
    StyleSheet,
    Image,
    TouchableOpacity
} from 'react-native';

export default class Controls extends Component {
    static propTypes = {
        paused          : PropTypes.bool,
        onPressPlay     : PropTypes.func,
        onPressPause    : PropTypes.func,
        onBack          : PropTypes.func,
        onForward       : PropTypes.func,
        forwardDisabled : PropTypes.bool
    };

    static defaultProps = {
        paused          : false,
        onPressPlay     : () => {},
        onPressPause    : () => {},
        onBack          : () => {},
        onForward       : () => {},
        forwardDisabled : false
    }

    render() {
        const {
            paused,
            onPressPlay,
            onPressPause,
            onBack,
            onForward,
            forwardDisabled
        } = this.props;

        return (
            <View style={styles.container}>
                <View style={{ width: 40 }} />
                <TouchableOpacity onPress={onBack}>
                    <Image source={require('./../../../img/ic_skip_previous_white_36pt.png')} />
                </TouchableOpacity>
                <View style={{ width: 20 }} />
                {!paused ?
                    <TouchableOpacity onPress={onPressPause}>
                        <View style={styles.playButton}>
                            <Image source={require('./../../../img/ic_pause_white_48pt.png')} />
                        </View>
                    </TouchableOpacity> :
                    <TouchableOpacity onPress={onPressPlay}>
                        <View style={styles.playButton}>
                            <Image source={require('./../../../img/ic_play_arrow_white_48pt.png')} />
                        </View>
                    </TouchableOpacity>
                }
                <View style={{ width: 20 }} />
                <TouchableOpacity
                    onPress={onForward}
                    disabled={forwardDisabled}
                >
                    <Image
                        style={[ forwardDisabled && { opacity: 0.3 } ]}
                        source={require('./../../../img/ic_skip_next_white_36pt.png')}
                    />
                </TouchableOpacity>
                <View style={{ width: 40 }} />
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container : {
        flexDirection  : 'row',
        alignItems     : 'center',
        justifyContent : 'center',
        paddingTop     : 8
    },
    playButton : {
        height         : 72,
        width          : 72,
        borderWidth    : 1,
        borderColor    : 'white',
        borderRadius   : 72 / 2,
        alignItems     : 'center',
        justifyContent : 'center'
    },
    secondaryControl : {
        height : 20,
        width  : 20
    },
    off : {
        opacity : 0.30
    }
});
