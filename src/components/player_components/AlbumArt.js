import React, { Component } from 'react';
import PropTypes from 'prop-types';

import {
    View,
    StyleSheet,
    Image,
    TouchableOpacity,
    Dimensions
} from 'react-native';

export default class AlbumArt extends Component {
    static propTypes = {
        url     : PropTypes.string.isRequired,
        onPress : PropTypes.func
    };

    static defaultProps = {
        onPress : () => {}
    }

    render() {
        const {
            url,
            onPress
        } = this.props;

        return (
            <View style={styles.container}>
                <TouchableOpacity onPress={onPress}>
                    <Image
                        style={styles.image}
                        source={{ uri: url }}
                    />
                </TouchableOpacity>
            </View>
        );
    }
}

const { width } = Dimensions.get('window');
const imageSize = width - 48;

const styles = StyleSheet.create({
    container : {
        paddingLeft  : 24,
        paddingRight : 24
    },
    image : {
        width  : imageSize,
        height : imageSize
    }
});
