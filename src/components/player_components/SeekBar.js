import React, { Component } from 'react';
import PropTypes from 'prop-types';
import Slider from 'react-native-slider';

import {
    View,
    Text,
    StyleSheet
} from 'react-native';

export default class SeekBar extends Component {
    static propTypes = {
        trackLength     : PropTypes.number.isRequired,
        currentPosition : PropTypes.number.isRequired,
        onSeek          : PropTypes.func,
        onSlidingStart  : PropTypes.func
    };

    static defaultProps = {
        onSeek         : () => {},
        onSlidingStart : () => {}
    }

    pad = (n, width, z = 0) => {
        const v = `${n}`;

        return v.length >= width ? v : new Array(width - v.length + 1).join(z) + v;
    }

    minutesAndSeconds = (pos) => {
        return [
            this.pad(Math.floor(pos / 60), 2),
            this.pad(pos % 60, 2)
        ];
    }

    render() {
        const { trackLength, currentPosition, onSeek, onSlidingStart } = this.props;
        const elapsed = this.minutesAndSeconds(currentPosition);
        const remaining = this.minutesAndSeconds(trackLength - currentPosition);

        return (
            <View style={styles.container}>
                <View style={{ flexDirection: 'row' }}>
                    <Text style={styles.text}>
                        {`${elapsed[0]}:${elapsed[1]}`}
                    </Text>
                    <View style={{ flex: 1 }} />
                    <Text style={[ styles.text, { width: 40 } ]}>
                        {trackLength > 1 && `-${remaining[0]}:${remaining[1]}`}
                    </Text>
                </View>
                <Slider
                    maximumValue={Math.max(trackLength, 1, currentPosition + 1)}
                    onSlidingStart={onSlidingStart}
                    onSlidingComplete={onSeek}
                    value={currentPosition}
                    style={styles.slider}
                    minimumTrackTintColor='#fff'
                    maximumTrackTintColor='rgba(255, 255, 255, 0.14)'
                    thumbStyle={styles.thumb}
                    trackStyle={styles.track}
                />
            </View>
        );
    }
}

const styles = StyleSheet.create({
    slider : {
        marginTop : -12
    },
    container : {
        paddingLeft  : 16,
        paddingRight : 16,
        paddingTop   : 16,
        width        : '100%'
    },
    track : {
        height       : 2,
        borderRadius : 1
    },
    thumb : {
        width           : 10,
        height          : 10,
        borderRadius    : 5,
        backgroundColor : 'white'
    },
    text : {
        color     : 'rgba(255, 255, 255, 0.72)',
        fontSize  : 12,
        textAlign : 'center'
    }
});
