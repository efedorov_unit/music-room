import React, { Component } from 'react';
import PropTypes from 'prop-types';

import {
    View,
    Text,
    StyleSheet
} from 'react-native';

export default class TrackDetails extends Component {
    static propTypes = {
        title : PropTypes.string.isRequired
    };

    render() {
        const {
            title
        } = this.props;

        return (
            <View style={styles.container}>
                <View style={styles.detailsWrapper}>
                    <Text style={styles.title}>
                        {title}
                    </Text>
                </View>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container : {
        paddingTop    : 24,
        flexDirection : 'row',
        paddingLeft   : 20,
        alignItems    : 'center',
        paddingRight  : 20
    },
    detailsWrapper : {
        justifyContent : 'center',
        alignItems     : 'center',
        flex           : 1
    },
    title : {
        fontSize   : 16,
        fontWeight : 'bold',
        color      : 'white',
        textAlign  : 'center'
    },
    artist : {
        color     : 'rgba(255, 255, 255, 0.72)',
        fontSize  : 12,
        marginTop : 4
    },
    button : {
        opacity      : 0.72,
        borderRadius : 10,
        width        : 25,
        height       : 25
    },
    moreButton : {
        borderColor    : 'rgb(255, 255, 255)',
        borderWidth    : 2,
        opacity        : 0.72,
        borderRadius   : 10,
        width          : 20,
        height         : 20,
        alignItems     : 'center',
        justifyContent : 'center'
    },
    moreButtonIcon : {
        height : 17,
        width  : 17
    }
});
