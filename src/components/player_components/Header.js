import React, { Component } from 'react';
import PropTypes from 'prop-types';

import {
    View,
    Text,
    StyleSheet,
    Image,
    TouchableOpacity
} from 'react-native';

export default class Header extends Component {
    static propTypes = {
        onDownPress : PropTypes.func.isRequired
    };

    render() {
        const {
            onDownPress
        } = this.props;

        return (
            <View style={styles.container}>
                <TouchableOpacity onPress={onDownPress}>
                    <Image
                        style={styles.button}
                        source={require('./../../../img/ic_keyboard_arrow_down_white.png')}
                    />
                </TouchableOpacity>
                <Text
                    style={styles.message}
                >
                    {'MUSIC ROOM'}
                </Text>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container : {
        position      : 'absolute',
        top           : 0,
        width         : '100%',
        height        : 72,
        paddingTop    : 15,
        paddingLeft   : 12,
        paddingRight  : 12,
        flexDirection : 'row'
    },
    message : {
        flex       : 1,
        textAlign  : 'center',
        color      : '#ffffff',
        fontWeight : 'bold',
        fontSize   : 16
    },
    button : {
        width   : 25,
        height  : 25,
        opacity : 0.72
    }
});
