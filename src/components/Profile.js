import React, { Component } from 'react';
import LIVR from 'livr';
import PropTypes from 'prop-types';
import {
    StyleSheet,
    Text,
    View,
    SafeAreaView,
    ScrollView,
    Dimensions
} from 'react-native';
import { Avatar, Input, Overlay, Button, Icon } from 'react-native-elements';
import ImagePicker from 'react-native-image-picker';

import CustomButton from './base/CustomButton';
import Info from './profile/Info';

const SCREEN_WIDTH = Dimensions.get('window').width;
const IMAGE_SIZE = SCREEN_WIDTH / 2;

export default class Profile extends Component {
    static propTypes = {
        actions : PropTypes.object.isRequired,
        dispatch : PropTypes.func.isRequired,
        user : PropTypes.object.isRequired
    }

    state = {
        isVisible  : false,
        currErr    : null,
        password   : '',
        rePass     : '',
        validation : {
            fields : {},
            errors : {
                REQUIRED    : 'This field is required',
                TOO_SHORT   : 'Password must be at least 6 characters',
                FIELDS_NOT_EQUAL : 'Fields not equal'
            }
        },
        userImage: ''
    }

    componentDidMount() {
        const { dispatch, actions } = this.props;

        dispatch(actions.getUser());
    }

    handleImageChange = () => {
        ImagePicker.showImagePicker(null, (response) => {
            if (response.error) {
                return;
            }

            const source = response.uri;

            this.setState({ userImage: source });
        });
    }

    handleSavePass = async () => {
        const { dispatch, actions } = this.props;
        const { password, rePass } = this.state;

        if (this.validate({ password, rePass })) {
            const res = await dispatch(actions.changePass(password));

            if (res) {
                this.setState({ isVisible: false });
            }
        }
    }

    handleEmailChange = email => {
        this.setState({ email });
    }

    handlePassChange = (field, value) => {
        this.setState({ [field]: value });
    }

    handleOpenPassForm = () => {
        this.setState({ isVisible: true });
    }

    handleCloseOverlay = () => {
        this.setState({ isVisible: false });
    }

    handleSelectPreference = pref => {
        const { dispatch, actions } = this.props;

        dispatch(actions.setPref(pref));
    }

    validate(data) {
        const validationRules = {
            password : [ 'trim', 'required',
                { 'min_length': 6 },
                { 'equal_to_field': 'rePass' }
            ],
            rePass : [ 'trim', 'required',
                { 'min_length': 6 },
                { 'equal_to_field': 'password' }
            ]
        };

        const validator = new LIVR.Validator(validationRules);
        const validateResult = validator.validate(data);

        if (!validateResult) {
            this.setState({
                validation : {
                    ...this.state.validation,
                    fields : validator.getErrors()
                }
            });
        }

        return validateResult;
    }

    static navigationOptions = {
        headerStyle : {
            backgroundColor: 'rgba(47,44,60,1)'
        }
    };

    render() {
        const { user: { data: { Nickname, Email, Preferences }, fbInfo, deezerInfo } } = this.props;
        const { validation, isVisible, password, rePass } = this.state;
        const errFields = { password: '', ...validation.fields };
        const avatarProps = {
            rounded: true,
            title: Nickname.substr(0, 2),
            size: IMAGE_SIZE
        };

        return (
            <SafeAreaView style={{ flex: 1 }}>
                <View style={{ flex: 1, backgroundColor: 'rgba(47,44,60,1)' }}>
                    <View style={styles.statusBar} />
                    <View style={styles.navBar}>
                        <Text style={styles.nameHeader}>{Nickname}</Text>
                    </View>
                    <ScrollView style={{ flex: 1 }}>
                        <View style={{ justifyContent: 'center', alignItems: 'center' }}>
                            <Avatar {...avatarProps} />
                        </View>
                        <View
                            style={{
                                flex: 1,
                                flexDirection: 'row',
                                marginTop: 20,
                                marginHorizontal: 30,
                                justifyContent: 'center',
                                alignItems: 'center'
                            }}
                        >
                            <Text style={styles.nameHeader}>{Email}</Text>
                        </View>
                        <View
                            style={{
                                flex: 1,
                                marginTop: 20,
                                width: SCREEN_WIDTH - 80,
                                marginLeft: 40
                            }}
                        >
                            <Button
                                title='Change password'
                                buttonStyle={{ backgroundColor: 'rgba(213, 100, 140, 1)' }}
                                onPress={this.handleOpenPassForm}
                            />
                        </View>
                        <View style={{ flex: 1, marginTop: 30 }}>
                            <Text
                                style={{
                                    flex: 1,
                                    fontSize: 15,
                                    color: 'rgba(216, 121, 112, 1)',
                                    marginLeft: 40
                                }}
                            >
                                Musical preferences
                            </Text>
                            <View style={{ flex: 1, width: SCREEN_WIDTH, marginTop: 20 }}>
                                <ScrollView
                                    style={{ flex: 1 }}
                                    horizontal
                                    showsHorizontalScrollIndicator={false}
                                >
                                    <View
                                        style={{
                                            flex: 1,
                                            flexDirection: 'column',
                                            marginLeft: 40,
                                            marginRight: 10
                                        }}
                                    >
                                        <View style={{ flex: 1, flexDirection: 'row', marginBottom: 10 }}>
                                            <CustomButton title='Pop' selected={Preferences.includes('pop')} onPress={this.handleSelectPreference.bind(null, 'pop')} />
                                            <CustomButton title='Rock' selected={Preferences.includes('rock')} onPress={this.handleSelectPreference.bind(null, 'rock')} />
                                        </View>
                                        <View style={{ flex: 1, flexDirection: 'row', marginBottom: 10 }}>
                                            <CustomButton title='Electro' selected={Preferences.includes('electro')} onPress={this.handleSelectPreference.bind(null, 'electro')} />
                                            <CustomButton title="R'n'B" selected={Preferences.includes('rnb')} onPress={this.handleSelectPreference.bind(null, 'rnb')} />
                                        </View>
                                        <View style={{ flex: 1, flexDirection: 'row', marginBottom: 10 }}>
                                            <CustomButton title='Metal' selected={Preferences.includes('metal')} onPress={this.handleSelectPreference.bind(null, 'metal')} />
                                            <CustomButton title='Rap' selected={Preferences.includes('rap')} onPress={this.handleSelectPreference.bind(null, 'rap')} />
                                        </View>
                                        <View style={{ flex: 1, flexDirection: 'row', marginBottom: 10 }}>
                                            <CustomButton title='Chill' selected={Preferences.includes('chill')} onPress={this.handleSelectPreference.bind(null, 'chill')} />
                                            <CustomButton title='Jazz' selected={Preferences.includes('jazz')} onPress={this.handleSelectPreference.bind(null, 'jazz')} />
                                        </View>
                                        <View style={{ flex: 1, flexDirection: 'row' }}>
                                            <CustomButton title='Indie' selected={Preferences.includes('indie')} onPress={this.handleSelectPreference.bind(null, 'indie')} />
                                            <CustomButton title='Folk' selected={Preferences.includes('folk')} onPress={this.handleSelectPreference.bind(null, 'folk')} />
                                        </View>
                                    </View>
                                </ScrollView>
                            </View>
                        </View>
                        {
                            fbInfo && <Info data={fbInfo} title={'Facebook info'} />
                        }
                        {
                            deezerInfo && <Info data={deezerInfo} title={'Deezer info'} />
                        }
                    </ScrollView>
                </View>
                <Overlay
                    isVisible={isVisible}
                    onBackdropPress={this.handleCloseOverlay}
                    windowBackgroundColor='rgba(0, 0, 0, .8)'
                    overlayBackgroundColor='rgba(47,44,60,1)'
                    height={300}
                >
                    <View
                        style={{
                            flex: 1,
                            alignItems: 'center',
                            justifyContent: 'center'
                        }}
                    >
                        <Input
                            onChangeText={this.handlePassChange.bind(this, 'password')}
                            placeholder='New password'
                            errorStyle={styles.error}
                            errorMessage={validation.errors[errFields.password]}
                            value={password}
                            secureTextEntry={true}
                            inputStyle={{ color: '#fff', marginLeft: 10 }}
                            placeholderTextColor='#fff'
                            leftIcon={
                                <Icon
                                    name='lock'
                                    type='font-awesome'
                                    color='#fff'
                                    size={20}
                                />
                            }
                        />
                        <Input
                            onChangeText={this.handlePassChange.bind(this, 'rePass')}
                            placeholder='Repeat new password'
                            errorStyle={styles.error}
                            secureTextEntry={true}
                            errorMessage={validation.errors[errFields.rePass]}
                            value={rePass}
                            inputStyle={{ color: '#fff', marginLeft: 10 }}
                            placeholderTextColor='#fff'
                            leftIcon={
                                <Icon
                                    name='lock'
                                    type='font-awesome'
                                    color='#fff'
                                    size={20}
                                />
                            }
                        />
                        <View
                            style={{
                                marginTop: 30,
                                width: SCREEN_WIDTH - 120
                            }}
                        >
                            <Button
                                title='Save'
                                buttonStyle={{ backgroundColor: 'rgba(213, 100, 140, 1)' }}
                                onPress={this.handleSavePass}
                            />
                        </View>
                    </View>
                </Overlay>
            </SafeAreaView>
        );
    }
}

const styles = StyleSheet.create({
    statusBar: {
        height: 10
    },
    navBar: {
        height: 60,
        width: SCREEN_WIDTH,
        justifyContent: 'center',
        alignContent: 'center'
    },
    nameHeader: {
        color: 'white',
        fontSize: 22,
        textAlign: 'center'
    }
});
