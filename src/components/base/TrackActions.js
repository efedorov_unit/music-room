/* eslint-disable more/no-then */
/* eslint-disable camelcase */
import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { View } from 'react-native';
import { Icon, ListItem, Overlay } from 'react-native-elements';
import { ShareDialog } from 'react-native-fbsdk';

export default class TrackActions extends Component {
    static propTypes = {
        visible : PropTypes.bool,
        onClose : PropTypes.func.isRequired,
        track : PropTypes.object.isRequired,
        room : PropTypes.object.isRequired,
        actions : PropTypes.object.isRequired,
        dispatch : PropTypes.func.isRequired
    };

    static defaultProps = {
        visible : false
    }

    state = {
        type: 'actions'
    }

    handleShowRoom = () => {
        const { dispatch, actions } = this.props;

        dispatch(actions.getRooms());

        this.setState({ type: 'rooms' });
    }

    handleAddTrack = (roomId) => {
        const { dispatch, actions, track: { id, link, preview, title_short, album: { cover_medium } } } = this.props;
        const trackInfo = {
            RoomId: roomId,
            TrackId: id,
            Link: link,
            Preview: preview,
            Title: title_short,
            Image: cover_medium
        };

        dispatch(actions.addToRoom(trackInfo));
        this.setState({ type: 'actions' });
    }

    handleClose = () => {
        this.props.onClose();

        this.setState({ type: 'actions' });
    }

    handleShare = () => {
        const { track: { link, title_short } } = this.props;

        const shareLinkContent = {
            contentType: 'link',
            contentUrl: link,
            contentDescription: title_short
        };

        ShareDialog.canShow(shareLinkContent).then(
            (canShow) => {
                if (canShow) {
                    return ShareDialog.show(shareLinkContent);
                }
            }
        );
    }

    render() {
        const {
            visible,
            track,
            room
        } = this.props;
        const { type } = this.state;

        return (
            <Overlay
                isVisible={visible}
                onBackdropPress={this.handleClose}
                windowBackgroundColor='rgba(0, 0, 0, .8)'
                overlayBackgroundColor='rgba(47,44,60,1)'
            >
                <View>
                    {
                        type === 'actions' &&
                        <View>
                            <ListItem
                                title={track.title_short}
                                leftAvatar={{ rounded: Boolean(true), source: { uri: track.album.cover_small } }}
                                containerStyle={{ backgroundColor: 'rgba(47,44,60,1)' }}
                                titleStyle={{ color: '#fff' }}
                            />
                            <ListItem
                                title={'Add to playlist'}
                                leftIcon={
                                    <Icon
                                        name='plus'
                                        type='font-awesome'
                                        size={20}
                                        color={'#fff'}
                                    />
                                }
                                containerStyle={{ backgroundColor: 'rgba(47,44,60,1)' }}
                                titleStyle={{ color: '#fff' }}
                                onPress={this.handleShowRoom}
                            />
                            <ListItem
                                title={'Share'}
                                leftIcon={
                                    <Icon
                                        name='share'
                                        type='font-awesome'
                                        size={20}
                                        color={'#fff'}
                                    />
                                }
                                onPress={this.handleShare}
                                containerStyle={{ backgroundColor: 'rgba(47,44,60,1)' }}
                                titleStyle={{ color: '#fff' }}
                            />
                        </View>
                    }
                    {
                        type === 'rooms' &&
                            <View>
                                {
                                    room.data && room.data.map(elem => {
                                        return (
                                            <ListItem
                                                key={elem.Id}
                                                title={elem.Name}
                                                containerStyle={{ backgroundColor: 'rgba(47,44,60,1)' }}
                                                titleStyle={{ color: '#fff' }}
                                                rightIcon={
                                                    <Icon
                                                        name='plus-circle'
                                                        type='font-awesome'
                                                        size={20}
                                                        color={'#fff'}
                                                    />
                                                }
                                                onPress={this.handleAddTrack.bind(null, elem.Id)}
                                            />
                                        );
                                    })
                                }
                            </View>
                    }
                </View>
            </Overlay>
        );
    }
}
