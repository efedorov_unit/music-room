import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Button } from 'react-native-elements';

export default class CustomButton extends Component {
    static propTypes = {
        title: PropTypes.string.isRequired,
        selected: PropTypes.bool.isRequired,
        onPress: PropTypes.func.isRequired
    }

    render() {
        const { title, onPress, selected } = this.props;

        return (
            <Button
                title={title}
                titleStyle={{ fontSize: 15, color: 'white' }}
                buttonStyle={
                    selected
                        ? {
                            backgroundColor: 'rgba(213, 100, 140, 1)',
                            borderRadius: 100,
                            width: 127
                        }
                        : {
                            borderWidth: 1,
                            borderColor: 'white',
                            borderRadius: 30,
                            width: 127,
                            backgroundColor: 'transparent'
                        }
                }
                containerStyle={{ marginRight: 10 }}
                onPress={onPress}
            />
        );
    }
}
