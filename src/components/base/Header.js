import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { StyleSheet, View, Platform } from 'react-native';
import { Icon, SearchBar, Overlay, ListItem } from 'react-native-elements';
import { LoginManager } from 'react-native-fbsdk';
import { StackActions, NavigationActions } from 'react-navigation';

export default class CustomHeader extends Component {
    static propTypes = {
        search     : PropTypes.object.isRequired,
        actions    : PropTypes.object.isRequired,
        dispatch   : PropTypes.func.isRequired,
        navigation : PropTypes.object.isRequired
    }

    state = {
        isVisible: false
    }

    handleNavigate = page => {
        this.props.navigation.navigate(page);
    }

    handleOpenOverlay = () => {
        this.setState({ isVisible: true });
    }

    handleCloseOverlay = () => {
        this.setState({ isVisible: false });
    }

    handleUpdateSearch = search => {
        const { actions, dispatch } = this.props;

        dispatch(actions.searchQuery(search));
    };

    handleCreateNewRoom = () => {
        this.setState({ isVisible: false });
        this.props.navigation.navigate('NewRoom');
    }

    handleManageRoom = () => {
        this.setState({ isVisible: false });
        this.props.navigation.navigate('ManageRoom');
    }

    handleSelectRoom = () => {
        this.setState({ isVisible: false });
        this.props.navigation.navigate('SelectRoom');
    }

    handleLogout = async () => {
        const { dispatch, actions, navigation } = this.props;

        this.setState({ isVisible: false });
        await dispatch(actions.logout());

        LoginManager.logOut();

        const resetAction = StackActions.reset({
            index: 0,
            actions: [ NavigationActions.navigate({ routeName: 'Login' }) ]
        });

        navigation.dispatch(resetAction);
    }

    handleSubmit = async () => {
        const { actions, dispatch, navigation, search: { query } } = this.props;

        if (!query) {
            return;
        }

        await dispatch(actions.searchCall(query));

        navigation.navigate('SearchResults');
    }

    render() {
        const { isVisible } = this.state;
        const { search: { query } } = this.props;

        return (
            <View>
                <View style={styles.container}>
                    <View>
                        <Icon
                            containerStyle={{ marginLeft: 10 }}
                            name='home'
                            onPress={this.handleNavigate.bind(null, 'Home')}
                            color={'#fff'}
                        />
                    </View>
                    <View style={styles.rightSide}>
                        <SearchBar
                            placeholder='Search music'
                            onChangeText={this.handleUpdateSearch}
                            onSubmitEditing={this.handleSubmit}
                            value={query}
                            containerStyle={{ width: '80%', backgroundColor: 'transparent', borderTopWidth: 0, borderBottomWidth: 0, paddingTop: 10 }}
                            inputContainerStyle={{ backgroundColor: 'transparent' }}
                        />
                        <Icon
                            containerStyle={{ marginRight: 10 }}
                            name='account-circle'
                            onPress={this.handleNavigate.bind(null, 'Profile')}
                            color={'#fff'}
                        />
                        <Icon
                            containerStyle={{ marginRight: 10 }}
                            type='font-awesome'
                            name='cog'
                            onPress={this.handleOpenOverlay}
                            color={'#fff'}
                        />
                    </View>
                </View>
                <Overlay
                    isVisible={isVisible}
                    onBackdropPress={this.handleCloseOverlay}
                    windowBackgroundColor='rgba(0, 0, 0, .8)'
                    overlayBackgroundColor='rgba(47,44,60,1)'
                    height={300}
                >
                    <View>
                        <ListItem
                            title={'Create new room'}
                            leftIcon={
                                <Icon
                                    name='plus'
                                    type='font-awesome'
                                    size={20}
                                    color={'#fff'}
                                />
                            }
                            containerStyle={{ backgroundColor: 'rgba(47,44,60,1)' }}
                            titleStyle={{ color: '#fff' }}
                            onPress={this.handleCreateNewRoom}
                        />
                        <ListItem
                            title={'Manage rooms'}
                            leftIcon={
                                <Icon
                                    name='edit'
                                    size={20}
                                    color={'#fff'}
                                />
                            }
                            containerStyle={{ backgroundColor: 'rgba(47,44,60,1)' }}
                            titleStyle={{ color: '#fff' }}
                            onPress={this.handleManageRoom}
                        />
                        <ListItem
                            title={'Select room'}
                            leftIcon={
                                <Icon
                                    name='check'
                                    type='font-awesome'
                                    size={20}
                                    color={'#fff'}
                                />
                            }
                            containerStyle={{ backgroundColor: 'rgba(47,44,60,1)' }}
                            titleStyle={{ color: '#fff' }}
                            onPress={this.handleSelectRoom}
                        />
                        <ListItem
                            title={'Logout'}
                            leftIcon={
                                <Icon
                                    name='sign-out'
                                    type='font-awesome'
                                    size={20}
                                    color={'#fff'}
                                />
                            }
                            containerStyle={{ backgroundColor: 'rgba(47,44,60,1)' }}
                            titleStyle={{ color: '#fff' }}
                            onPress={this.handleLogout}
                        />
                    </View>
                </Overlay>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container : {
        height         : 50,
        flexDirection  : 'row',
        justifyContent : 'space-between',
        alignItems     : 'center',
        backgroundColor: 'rgba(47,44,60,1)',
        paddingTop: Platform.OS === 'ios' ? 20 : 0
    },
    rightSide : {
        flex           : 1,
        flexDirection  : 'row',
        justifyContent : 'flex-end',
        alignItems     : 'center'
    }
});
