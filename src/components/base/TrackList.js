import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { FlatList } from 'react-native';
import { Icon, ListItem } from 'react-native-elements';

export default class TrackList extends Component {
    static propTypes = {
        navigation : PropTypes.object.isRequired,
        actions : PropTypes.object.isRequired,
        dispatch : PropTypes.func.isRequired,
        tracks : PropTypes.array.isRequired,
        onPress: PropTypes.func.isRequired
    };

    handlePlayTrack = index => {
        const { dispatch, actions, navigation, tracks } = this.props;

        const convertedTracks = tracks.map(track => {
            if (!track.readable) {
                return;
            }

            return {
                Title : track.title_short,
                Image : track.album.cover_medium,
                Preview : track.preview
            };
        }).filter(track => track);

        dispatch(actions.playTrack(convertedTracks, index));
        navigation.navigate('Player');
    }

    _keyExtractor = (item, index) => `${index}`;

    renderListElem = ({ item, index }) => {
        return (
            <ListItem
                title={item.title_short}
                leftAvatar={{ rounded: Boolean(true), source: { uri: item.album.cover_small } }}
                rightIcon={
                    <Icon
                        name='ellipsis-h'
                        type='font-awesome'
                        size={20}
                        color={'#fff'}
                        onPress={this.props.onPress.bind(null, index)}
                    />
                }
                onPress={this.handlePlayTrack.bind(null, index)}
                containerStyle={{ backgroundColor: 'rgba(47,44,60,1)' }}
                titleStyle={{ color: '#fff' }}
            />
        );
    }

    render() {
        return (
            <FlatList
                data={this.props.tracks}
                renderItem={this.renderListElem}
                keyExtractor={this._keyExtractor}
            />
        );
    }
}
