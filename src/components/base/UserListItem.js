import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { ListItem, Avatar } from 'react-native-elements';

export default class UserListItem extends Component {
    static propTypes = {
        user : PropTypes.object.isRequired,
        onPress: PropTypes.func.isRequired,
        selected: PropTypes.bool
    };

    static defaultProps = {
        selected: false
    }

    render() {
        const { user, onPress, selected } = this.props;
        const avatarProps = {
            rounded: true,
            title: user.Nickname.substr(0, 2),
            size: 50
        };

        if (user.img) {
            avatarProps.source = { uri: user.img };
        }

        return (
            <ListItem
                title={user.Nickname}
                leftIcon={
                    <Avatar
                        {...avatarProps}
                    />
                }
                containerStyle={{ borderRadius: 5, backgroundColor: selected ? 'rgba(32, 220, 163, 0.49)' : 'rgba(47,44,60,1)' }}
                titleStyle={{ color: '#fff' }}
                onPress={onPress}
            />
        );
    }
}
