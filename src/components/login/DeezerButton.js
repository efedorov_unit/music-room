import React, { Component } from 'react';
import PropTypes from 'prop-types';

import {
    View
} from 'react-native';
import { Button } from 'react-native-elements';

export default class DeezerButton extends Component {
    static propTypes = {
        onPress : PropTypes.func
    };

    static defaultProps = {
        onPress : () => {}
    }

    render() {
        return (
            <View>
                <Button
                    {...this.props}
                    title={'Link account with deezer'}
                    buttonStyle={{ backgroundColor: 'rgba(213, 100, 140, 1)' }}
                />
            </View>
        );
    }
}
