/* eslint-disable react/sort-comp */
import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { View, ScrollView, StyleSheet, FlatList } from 'react-native';
import {
    ListItem,
    Overlay,
    Icon,
    Text
} from 'react-native-elements';

import UserListItem from './base/UserListItem';

export default class ManageRoom extends Component {
    static propTypes = {
        actions    : PropTypes.object.isRequired,
        dispatch   : PropTypes.func.isRequired,
        room       : PropTypes.object.isRequired
    };

    state = {
        selectedRoom: null,
        isVisible: false
    }

    componentDidMount() {
        const { dispatch, actions } = this.props;

        dispatch(actions.getRooms());
        dispatch(actions.getUsers());
    }

    handleOpenOverlay = selectedRoom => {
        this.setState({ isVisible: true, selectedRoom });
    }

    handleCloseOverlay = () => {
        this.setState({ isVisible: false, selectedRoom: null });
    }

    handleSelectUser = (UserId, RoomId) => {
        const { dispatch, actions } = this.props;

        dispatch(actions.updateRoomForUser({ UserId, RoomId }));
    }

    _keyExtractor = (item, index) => `${item.Id}`;

    renderListElem = ({ item, index }) => {
        return (
            <ListItem
                title={item.Name}
                rightIcon={
                    <Icon
                        name='ellipsis-h'
                        type='font-awesome'
                        size={20}
                        color={'#fff'}
                    />
                }
                onPress={this.handleOpenOverlay.bind(null, index)}
                containerStyle={{ backgroundColor: 'rgba(47,44,60,1)' }}
                titleStyle={{ color: '#fff' }}
            />
        );
    }

    static navigationOptions = {
        headerStyle : {
            backgroundColor: 'rgba(47,44,60,1)',
            borderBottomWidth: 0
        },
        headerTitle: 'Manage rooms',
        headerTitleStyle: {
            flex: 1,
            textAlign: 'center',
            color: '#fff',
            fontSize: 20,
            fontWeight: 'normal'
        }
    };

    render() {
        const { room } = this.props;
        const { isVisible, selectedRoom } = this.state;
        const currRoom = selectedRoom !== null ? room.data[selectedRoom] : { Name: '', Id: 0 };

        return (
            <ScrollView style={styles.container} keyboardShouldPersistTaps='handled'>
                <FlatList
                    data={room.data}
                    renderItem={this.renderListElem}
                    keyExtractor={this._keyExtractor}
                />
                <Overlay
                    isVisible={isVisible}
                    onBackdropPress={this.handleCloseOverlay}
                    windowBackgroundColor='rgba(0, 0, 0, .8)'
                    overlayBackgroundColor='rgba(47,44,60,1)'
                >
                    <View>
                        <ScrollView style={styles.container} keyboardShouldPersistTaps='handled'>
                            <ListItem
                                title={currRoom ? `Add users to ${currRoom.Name}` : ''}
                                containerStyle={{ backgroundColor: 'rgba(47,44,60,1)' }}
                                titleStyle={{ color: '#fff' }}
                            />
                            {
                                room.users.length ?
                                    room.users.map(user => {
                                        return (
                                            <UserListItem
                                                key={user.Id}
                                                user={user}
                                                onPress={
                                                    this.handleSelectUser.bind(null, user.Id, currRoom.Id)
                                                }
                                                // eslint-disable-next-line eqeqeq
                                                selected={Boolean(user.Rooms.includes(currRoom.Id))}
                                            />
                                        );
                                    }) : <Text h4 h4Style={{ color: '#fff', textAlign: 'center', marginTop: 20 }}>No users available</Text>
                            }
                        </ScrollView>
                    </View>
                </Overlay>
            </ScrollView>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        backgroundColor: 'rgba(47,44,60,1)'
    },
    headerContainer: {
        justifyContent: 'center',
        alignItems: 'center',
        padding: 40,
        backgroundColor: 'rgba(47,44,60,1)'
    },
    heading: {
        color: 'white',
        marginTop: 10,
        fontSize: 22,
        fontWeight: 'bold'
    },
    contentView: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center'
    },
    inputContainerStyle: {
        marginTop: 16,
        width: '90%'
    },
    checkbox : {
        backgroundColor: 'rgba(47,44,60,1)',
        borderWidth: 0
    }
});

