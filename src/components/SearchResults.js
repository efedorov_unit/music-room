/* eslint-disable react/jsx-max-props-per-line */
import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { StyleSheet, View, Dimensions } from 'react-native';
import { Text } from 'react-native-elements';

import Header from './base/Header';
import TrackActions from './base/TrackActions';
import TrackList from './base/TrackList';

const SCREEN_HEIGHT = Dimensions.get('window').height;

export default class SearchResults extends Component {
    static propTypes = {
        search   : PropTypes.object.isRequired,
        actions  : PropTypes.object.isRequired,
        dispatch : PropTypes.func.isRequired,
        navigation: PropTypes.object.isRequired, 
        room : PropTypes.object.isRequired
    }

    state = {
        isVisible   : false,
        currTrackId : 0
    }

    handleCloseOverlay = () => {
        this.setState({ isVisible: false });
    }

    handleOpenOverlay = index => {
        this.setState({ isVisible: Boolean(true), currTrackId: index });
    }

    static navigationOptions = {
        header : null
    };

    render() {
        const { isVisible, currTrackId } = this.state;
        const { search: { music }, room, dispatch, actions, navigation } = this.props;
        const isRes = Boolean(music.length);
        const selectedTrack = music[currTrackId] || { title_short : '', album: { cover_small : '' } }; // eslint-disable-line

        return (
            <View>
                <Header {...this.props} />
                <View style={styles.container}>
                    <Text h2 style={{ marginLeft: 10, color: '#fff' }}>Results:</Text>
                    {
                        isRes &&
                        <TrackList
                            tracks={music}
                            onPress={this.handleOpenOverlay}
                            actions={actions}
                            dispatch={dispatch}
                            navigation={navigation}
                        />
                    }
                    {
                        !isRes &&
                            <Text style={{ textAlign: 'center', color: '#fff' }} h4>Oops! No results found :(</Text>
                    }
                </View>
                <TrackActions visible={isVisible} onClose={this.handleCloseOverlay} track={selectedTrack} room={room} actions={actions} dispatch={dispatch} />
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container : {
        backgroundColor: 'rgba(47,44,60,1)',
        minHeight: SCREEN_HEIGHT
    }
});
