import React from 'react';
import PropTypes from 'prop-types';
import { View, Text, StyleSheet, Linking } from 'react-native';

export default class FbInfo extends React.Component {
    static propTypes = {
        data : PropTypes.object.isRequired,
        title : PropTypes.string.isRequired
    }

    handleLinkPress = link => {
        Linking.openURL(link);
    }

    _calculateAge(date) {
        const birthday = new Date(date);
        const ageDifMs = Date.now() - birthday.getTime();
        const ageDate = new Date(ageDifMs);

        return Math.abs(ageDate.getUTCFullYear() - 1970);
    }

    render() {
        const { data: { Birthday, Email, Nickname, FirstName, LastName, Link }, title } = this.props;
        const birthday = this._calculateAge(Birthday);

        return (
            <View style={{ flex: 1, marginTop: 30 }}>
                <Text
                    style={{
                        flex: 1,
                        fontSize: 15,
                        color: 'rgba(216, 121, 112, 1)',
                        marginLeft: 40
                    }}
                >
                    {title}
                </Text>
                <View
                    style={{
                        marginTop: 20,
                        marginLeft: 20
                    }}
                >
                    <View style={{ flex: 1, flexDirection: 'row' }}>
                        <View>
                            { birthday ? <Text style={styles.infoTypeLabel}>Age</Text> : null }
                            { Email ? <Text style={styles.infoTypeLabel}>Email</Text> : null }
                            { Nickname ? <Text style={styles.infoTypeLabel}>Nickname</Text> : null }
                            { FirstName ? <Text style={styles.infoTypeLabel}>FirstName</Text> : null }
                            { LastName ? <Text style={styles.infoTypeLabel}>LastName</Text> : null }
                            { Link ? <Text style={styles.infoTypeLabel}>Link</Text> : null }
                        </View>
                        <View style={{ marginLeft: 10 }}>
                            { birthday ? <Text style={styles.infoAnswerLabel}>{birthday}</Text> : null }
                            { Email ? <Text style={styles.infoAnswerLabel}>{Email}</Text> : null }
                            { Nickname ? <Text style={styles.infoAnswerLabel}>{Nickname}</Text> : null }
                            { FirstName ? <Text style={styles.infoAnswerLabel}>{FirstName}</Text> : null }
                            { LastName ? <Text style={styles.infoAnswerLabel}>{LastName}</Text> : null }
                            {
                                Link ?
                                    <Text
                                        style={{ ...styles.infoAnswerLabel, textDecorationLine: 'underline' }}
                                        numberOfLines={1}
                                        onPress={this.handleLinkPress.bind(null, Link)}
                                    >
                                        {Link}
                                    </Text> : null
                            }
                        </View>
                    </View>
                </View>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    infoTypeLabel: {
        fontSize: 15,
        textAlign: 'right',
        color: 'rgba(126,123,138,1)',
        paddingBottom: 10
    },
    infoAnswerLabel: {
        fontSize: 15,
        color: 'white',
        paddingBottom: 10
    }
});
