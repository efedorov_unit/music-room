/* eslint-disable eqeqeq */
/* eslint-disable func-names */
/* eslint-disable more/no-then */
/* eslint-disable react/sort-comp */
import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { StyleSheet, View, Dimensions } from 'react-native';
import SortableListView from 'react-native-sortable-listview';
import {
    ListItem,
    Icon,
    Overlay,
    Text
} from 'react-native-elements';
import { ShareDialog } from 'react-native-fbsdk';

const conn = new WebSocket('ws://localhost:8080');

export default class MusicRoom extends Component {
    static propTypes = {
        actions : PropTypes.object.isRequired,
        dispatch : PropTypes.func.isRequired,
        room : PropTypes.object.isRequired,
        navigation : PropTypes.object.isRequired
    };

    state = {
        isVisible: false,
        currTrackId: 0
    }

    componentDidMount() {
        const { room, actions, dispatch } = this.props;

        dispatch(actions.getRoomTracks(room.currRoom));

        conn.onopen = () => {
            console.log('Connection established!');
        };

        conn.onmessage = (e) => {
            this.handleMessage(e.data);
        };
    }

    handleMessage = data => {
        const { room: { currRoom }, dispatch, actions, navigation } = this.props;
        const { Type, Data, Data: { RoomId } } = JSON.parse(data);

        if (currRoom !== RoomId) {
            return;
        }

        switch (Type) {
            case 'sort_tracks':
                this.setState({ isVisible: false });
                dispatch(actions.listSort(Data.Tracks));
                break;
            case 'delete_track':
                this.setState({ isVisible: false });
                dispatch(actions.removeTrackByEvent(Data.TrackId));
                break;
            case 'play_tracks':
                this.setState({ isVisible: false });
                dispatch(actions.playTrack(Data.Tracks, Data.StartFrom));
                navigation.navigate('Player');
                break;
            default:
                break;
        }
    }

    handleCloseOverlay = () => {
        this.setState({ isVisible: false });
    }

    handleOpenOverlay = index => {
        this.setState({ isVisible: Boolean(true), currTrackId: index });
    }

    handleRemoveTrack = trackId => {
        const { room: { currRoom }, dispatch, actions } = this.props;

        conn.send(JSON.stringify({
            Type: 'delete_track',
            Data: {
                TrackId: trackId,
                RoomId: currRoom
            }
        }));

        dispatch(actions.removeTrack({ RoomId: currRoom, TrackId: trackId }));
        this.setState({ isVisible: false });
    }

    handleRowMove = (e) => {
        const { dispatch, actions, room: { currRoom, tracks } } = this.props;
        const { to, from } = e;

        tracks.splice(to, 0, tracks.splice(from, 1)[0]);

        conn.send(JSON.stringify({
            Type: 'sort_tracks',
            Data: {
                Tracks: tracks,
                RoomId: currRoom
            }
        }));

        dispatch(actions.listSort(tracks));
    }

    handlePlayTrack = index => {
        const { dispatch, actions, navigation, room: { tracks, currRoom } } = this.props;

        conn.send(JSON.stringify({
            Type: 'play_tracks',
            Data: {
                Tracks: tracks,
                RoomId: currRoom,
                StartFrom: index
            }
        }));

        dispatch(actions.playTrack(tracks, index));
        navigation.navigate('Player');
    }

    handleShare = () => {
        const { room: { tracks } } = this.props;
        const track = tracks[this.state.currTrackId];

        const shareLinkContent = {
            contentType: 'link',
            contentUrl: track.Link,
            contentDescription: track.Title
        };

        ShareDialog.canShow(shareLinkContent).then(
            (canShow) => {
                if (canShow) {
                    return ShareDialog.show(shareLinkContent);
                }
            }
        );
    }

    _keyExtractor = (item) => `draggable-item-${item.TrackId}`;

    renderListElem = (data, t, index) => {
        return (
            <ListItem
                title={data.Title}
                leftIcon={
                    <Icon
                        name='play'
                        type='font-awesome'
                        size={20}
                        color={'#fff'}
                        onPress={this.handlePlayTrack.bind(null, index)}
                    />
                }
                leftAvatar={{ rounded: Boolean(true), source: { uri: data.Image } }}
                rightIcon={
                    <Icon
                        name='ellipsis-h'
                        type='font-awesome'
                        size={20}
                        color={'#fff'}
                        onPress={this.handleOpenOverlay.bind(null, index)}
                    />
                }
                containerStyle={{ backgroundColor: 'rgba(47,44,60,1)' }}
                titleStyle={{ color: '#fff' }}
            />
        );
    }

    static navigationOptions = {
        headerStyle : {
            backgroundColor: 'rgba(47,44,60,1)',
            borderBottomWidth: 0
        },
        headerTitle: 'Music room',
        headerTitleStyle: {
            flex: 1,
            textAlign: 'center',
            color: '#fff',
            fontSize: 20,
            fontWeight: 'normal'
        }
    };

    render() {
        const { isVisible, currTrackId } = this.state;
        const { room, room: { tracks } } = this.props;
        const track = tracks[currTrackId] || { Title: '', Image: '', TrackId: 0 };
        const cr = room.data.find(r => r.Id === room.currRoom);

        return (
            <View style={styles.container}>
                {
                    !tracks.length && <Text h4 h4Style={{ color: '#fff', textAlign: 'center', marginTop: 20 }}>No tracks available</Text>
                }
                <SortableListView
                    data={tracks}
                    renderRow={this.renderListElem}
                    onRowMoved={this.handleRowMove}
                    disableAnimatedScrolling={true}
                />
                <Overlay
                    isVisible={isVisible}
                    onBackdropPress={this.handleCloseOverlay}
                    windowBackgroundColor='rgba(0, 0, 0, .8)'
                    overlayBackgroundColor='rgba(47,44,60,1)'
                >
                    <View>
                        <ListItem
                            title={track.Title}
                            leftAvatar={{ rounded: Boolean(true), source: { uri: track.Image } }}
                            containerStyle={{ backgroundColor: 'rgba(47,44,60,1)' }}
                            titleStyle={{ color: '#fff' }}
                        />
                        {
                            cr.IsOwner == 1 &&
                            <ListItem
                                title={'Remove from playlist'}
                                leftIcon={
                                    <Icon
                                        name='remove'
                                        type='font-awesome'
                                        size={20}
                                        color={'#fff'}
                                    />
                                }
                                containerStyle={{ backgroundColor: 'rgba(47,44,60,1)' }}
                                titleStyle={{ color: '#fff' }}
                                onPress={this.handleRemoveTrack.bind(null, track.TrackId)}
                            />
                        }
                        <ListItem
                            title={'Share'}
                            leftIcon={
                                <Icon
                                    name='share'
                                    type='font-awesome'
                                    size={20}
                                    color={'#fff'}
                                />
                            }
                            onPress={this.handleShare}
                            containerStyle={{ backgroundColor: 'rgba(47,44,60,1)' }}
                            titleStyle={{ color: '#fff' }}
                        />
                    </View>
                </Overlay>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        backgroundColor: 'rgba(47,44,60,1)',
        height: Dimensions.get('window').height
    },
    headerContainer: {
        justifyContent: 'center',
        alignItems: 'center',
        padding: 40,
        backgroundColor: 'rgba(47,44,60,1)'
    },
    heading: {
        color: 'white',
        marginTop: 10,
        fontSize: 22,
        fontWeight: 'bold'
    },
    contentView: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center'
    },
    inputContainerStyle: {
        marginTop: 16,
        width: '90%'
    },
    checkbox : {
        backgroundColor: 'rgba(47,44,60,1)',
        borderWidth: 0
    }
});

