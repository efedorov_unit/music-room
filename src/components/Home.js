/* eslint-disable max-len */
/* eslint-disable react/jsx-max-props-per-line */
import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { StyleSheet, View, Dimensions, ScrollView } from 'react-native';
import { Text } from 'react-native-elements';

import Header from './base/Header';
import TrackActions from './base/TrackActions';
import TrackList from './base/TrackList';

const SCREEN_HEIGHT = Dimensions.get('window').height;

export default class Home extends Component {
    static propTypes = {
        search     : PropTypes.object.isRequired,
        actions    : PropTypes.object.isRequired,
        dispatch   : PropTypes.func.isRequired,
        navigation : PropTypes.object.isRequired,
        mainPage   : PropTypes.object.isRequired,
        room       : PropTypes.object.isRequired
    }

    state = {
        isVisible   : false,
        currTrackId : 0
    }

    componentDidMount() {
        const { dispatch, actions } = this.props;

        dispatch(actions.defaultTracks());
    }

    handleCloseOverlay = () => {
        this.setState({ isVisible: false });
    }

    handleOpenOverlay = index => {
        this.setState({ isVisible: Boolean(true), currTrackId: index });
    }

    static navigationOptions = {
        header : null
    };

    render() {
        const { isVisible, currTrackId } = this.state;
        const { mainPage: { defaultTracks }, room, actions, dispatch, navigation } = this.props;
        const selectedTrack = defaultTracks[currTrackId] || { title_short : '', album: { cover_small : '' } }; // eslint-disable-line

        return (
            <View>
                <Header {...this.props} />
                <ScrollView style={styles.container}>
                    <Text h3 style={styles.heading}>Start listening right now</Text>
                    <TrackList
                        tracks={defaultTracks}
                        onPress={this.handleOpenOverlay}
                        actions={actions}
                        dispatch={dispatch}
                        navigation={navigation}
                    />
                </ScrollView>
                <TrackActions visible={isVisible} onClose={this.handleCloseOverlay} track={selectedTrack} room={room} actions={actions} dispatch={dispatch} />
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container : {
        flex: 1,
        backgroundColor: 'rgba(47,44,60,1)',
        minHeight: SCREEN_HEIGHT
    },
    heading: {
        marginLeft: 10,
        color: '#fff'
    }
});
