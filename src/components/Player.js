/* eslint react/no-string-refs: 0 */
/* eslint react/jsx-no-bind: 0 */
/* eslint camelcase: 0 */

import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { StyleSheet, View, StatusBar } from 'react-native';
import Video from 'react-native-video';

import Header from './player_components/Header.js';
import AlbumArt from './player_components/AlbumArt.js';
import TrackDetails from './player_components/TrackDetails.js';
import SeekBar from './player_components/SeekBar.js';
import Controls from './player_components/Controls.js';

export default class Player extends Component {
    static propTypes = {
        player : PropTypes.object.isRequired,
        dispatch : PropTypes.func.isRequired,
        actions : PropTypes.object.isRequired,
        navigation : PropTypes.object.isRequired
    };

    handleSeek = (time) => {
        const { dispatch, actions } = this.props;
        const { audioElement } = this.refs;
        const t = Math.round(time);

        if (audioElement) {
            audioElement.seek(t);
        }

        dispatch(actions.seek({ currentPosition: t, paused: false }));
    }

    handleBack = () => {
        const { dispatch, actions, player: { selectedTrack, currentPosition } } = this.props;
        const { audioElement } = this.refs;

        if (currentPosition < 10 && selectedTrack > 0) {
            if (audioElement) {
                audioElement.seek(0);
            }

            dispatch(actions.back({
                currentPosition : 0,
                paused : false,
                totalLength : 1,
                selectedTrack : selectedTrack - 1
            }));

            return;
        }

        audioElement.seek(0);

        dispatch(actions.back({ currentPosition : 0 }));
    }

    handleForward = () => {
        const { dispatch, actions, player: { tracks, selectedTrack } } = this.props;
        const { audioElement } = this.refs;

        if (selectedTrack < tracks.length - 1) {
            if (audioElement) {
                audioElement.seek(0);
            }

            dispatch(actions.forward({
                currentPosition : 0,
                paused : false,
                totalLength : 1,
                selectedTrack : selectedTrack + 1
            }));
        }
    }

    handleSlidingStart = () => {
        const { dispatch, actions } = this.props;

        dispatch(actions.pause({ paused: true }));
    }

    handleStop = () => {
        const { dispatch, actions } = this.props;

        dispatch(actions.pause({ paused: true }));
    }

    handlePlay = () => {
        const { dispatch, actions } = this.props;

        dispatch(actions.pause({ paused: false }));
    }

    handleNavigateBack = () => {
        this.props.navigation.goBack();
    }

    setDuration = (data) => {
        const { dispatch, actions } = this.props;

        dispatch(actions.setDuration({ totalLength: Math.floor(data.duration) }));
    }

    setTime = (data) => {
        const { dispatch, actions, player: { totalLength } } = this.props;
        const currTime = Math.floor(data.currentTime);

        if (currTime >= totalLength) {
            this.handleForward();

            return;
        }

        dispatch(actions.setTime({ currentPosition: Math.floor(data.currentTime) }));
    }

    static navigationOptions = {
        header : null
    };

    renderVideo = (track) => {
        const { paused } = this.props.player;

        return (
            <Video
                source={{ uri: track.audioUrl }} // Can be a URL or a local file.
                ref='audioElement'
                paused={paused}               // Pauses playback entirely.
                onLoad={this.setDuration.bind(this)}    // Callback when video loads
                onProgress={this.setTime.bind(this)}    // Callback every ~250ms with currentTime
                style={styles.audioElement}
                volume={1.0}
                muted={false}
                playInBackground={Boolean(true)}
                playWhenInactive={Boolean(true)}
                repeat={false}
            />
        );
    }

    render() {
        const {
            tracks,
            selectedTrack,
            totalLength,
            currentPosition,
            paused
        } = this.props.player;

        let track = tracks[selectedTrack];

        if (!track) {
            return null;
        }

        track = {
            title : track.Title,
            albumArtUrl : track.Image,
            audioUrl : track.Preview
        };

        const video = this.renderVideo(track);

        return (
            <View style={styles.container}>
                <StatusBar hidden={Boolean(true)} />
                <Header onDownPress={this.handleNavigateBack} />
                <AlbumArt url={track.albumArtUrl} />
                <TrackDetails title={track.title} />
                <SeekBar
                    onSeek={this.handleSeek}
                    trackLength={totalLength}
                    onSlidingStart={this.handleSlidingStart}
                    currentPosition={currentPosition}
                />
                <Controls
                    forwardDisabled={selectedTrack === tracks.length - 1}
                    onPressPlay={this.handlePlay}
                    onPressPause={this.handleStop}
                    onBack={this.handleBack.bind(this)}
                    onForward={this.handleForward.bind(this)}
                    paused={paused}
                />
                {video}
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container : {
        flex            : 1,
        alignItems      : 'center',
        justifyContent  : 'center',
        backgroundColor: 'rgba(47,44,60,1)'
    },
    audioElement : {
        height : 0,
        width  : 0
    }
});
