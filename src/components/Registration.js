import React, { Component } from 'react';
import LIVR from 'livr';
import PropTypes from 'prop-types';

import { View, ScrollView, StyleSheet, Dimensions } from 'react-native';
import {
    Input,
    Icon,
    ThemeProvider,
    Button
} from 'react-native-elements';

const SCREEN_WIDTH = Dimensions.get('window').width;

export default class Registration extends Component {
    static propTypes = {
        navigation : PropTypes.object.isRequired,
        actions    : PropTypes.object.isRequired,
        dispatch   : PropTypes.func.isRequired,
        session    : PropTypes.object.isRequired
    };

    state = {
        Nickname: '',
        Email: '',
        Password: '',
        RePass: '',
        validation : {
            fields : {},
            errors : {
                REQUIRED : 'This field is required',
                WRONG_EMAIL : 'Wrong email format',
                TOO_SHORT : 'Must be at least 6 characters',
                FIELDS_NOT_EQUAL : 'Fields not equal'
            }
        }
    }

    componentDidUpdate() {
        const { navigation, session } = this.props;

        if (session.data) {
            navigation.navigate('PostRegistration');
        }
    }

    handleInput = (field, value) => {
        this.setState({ [field]: value });
    }

    handleCreate = async () => {
        const { dispatch, actions } = this.props;
        const { Nickname, Email, Password, RePass } = this.state;
        const user = { Nickname, Email, Password, RePass };

        if (this.validate(user)) {
            this.setState({ validation: { ...this.state.validation, fields: {} } });

            await dispatch(actions.registration(user));
        }
    }

    validate(data) {
        const validationRules = {
            Nickname : [ 'trim', 'required',
                { 'max_length': 128 },
                { 'like': '^[a-zA-Z0-9_]*$' }
            ],
            Password : [ 'trim', 'required',
                { 'min_length': 6 },
                { 'equal_to_field': 'RePass' }
            ],
            RePass : [ 'trim', 'required',
                { 'min_length': 6 },
                { 'equal_to_field': 'Password' }
            ],
            Email : [ 'trim', 'required', 'email', 'to_lc', { 'max_length': 128 } ]
        };

        const validator = new LIVR.Validator(validationRules);
        const validateResult = validator.validate(data);

        if (!validateResult) {
            this.setState({
                validation : {
                    ...this.state.validation,
                    fields : validator.getErrors()
                }
            });
        }

        return validateResult;
    }

    static navigationOptions = {
        headerStyle : {
            backgroundColor: 'rgba(47,44,60,1)',
            borderBottomWidth: 0
        },
        headerTitle: 'Registration',
        headerTitleStyle: {
            flex: 1,
            textAlign: 'center',
            color: '#fff',
            fontSize: 20,
            fontWeight: 'normal'
        }
    };

    render() {
        const { Nickname, Email, Password, RePass, validation } = this.state;
        const errFields = { Nickname: '', Email: '', Password: '', RePass: '', ...validation.fields };

        return (
            <ScrollView style={styles.container} keyboardShouldPersistTaps='handled'>
                <ThemeProvider
                    theme={{
                        Input: {
                            containerStyle: {
                                width: SCREEN_WIDTH - 50
                            },
                            inputContainerStyle: {
                                borderRadius: 40,
                                borderWidth: 1,
                                borderColor: '#fff',
                                height: 50,
                                marginVertical: 10
                            },
                            placeholderTextColor: '#fff',
                            inputStyle: {
                                marginLeft: 10,
                                color: 'white'
                            },
                            keyboardAppearance: 'light',
                            blurOnSubmit: false
                        }
                    }}
                >
                    <View
                        style={{
                            backgroundColor: 'rgba(47,44,60,1)',
                            width: SCREEN_WIDTH,
                            alignItems: 'center',
                            paddingBottom: 30
                        }}
                    >
                        <Input
                            leftIcon={
                                <Icon
                                    name='user'
                                    type='simple-line-icon'
                                    size={25}
                                    color='#fff'
                                />
                            }
                            placeholder='Username'
                            autoCapitalize='none'
                            autoCorrect={false}
                            keyboardType='email-address'
                            returnKeyType='next'
                            ref={input => (this.usernameInput = input)}
                            onSubmitEditing={() => {
                                this.email2Input.focus();
                            }}
                            onChangeText={this.handleInput.bind(this, 'Nickname')}
                            value={Nickname}
                            errorStyle={styles.error}
                            errorMessage={validation.errors[errFields.Nickname]}
                        />
                        <Input
                            leftIcon={
                                <Icon
                                    name='email-outline'
                                    type='material-community'
                                    size={25}
                                    color='#fff'
                                />
                            }
                            placeholder='Email'
                            autoCapitalize='none'
                            autoCorrect={false}
                            keyboardType='email-address'
                            returnKeyType='next'
                            ref={input => (this.email2Input = input)}
                            onSubmitEditing={() => {
                                this.password2Input.focus();
                            }}
                            onChangeText={this.handleInput.bind(this, 'Email')}
                            value={Email}
                            errorStyle={styles.error}
                            errorMessage={validation.errors[errFields.Email]}
                        />
                        <Input
                            leftIcon={
                                <Icon
                                    name='lock'
                                    type='simple-line-icon'
                                    size={25}
                                    color='#fff'
                                />
                            }
                            placeholder='Password'
                            autoCapitalize='none'
                            secureTextEntry={true}
                            autoCorrect={false}
                            keyboardType='default'
                            returnKeyType='next'
                            ref={input => (this.password2Input = input)}
                            onSubmitEditing={() => {
                                this.confirmPassword2Input.focus();
                            }}
                            onChangeText={this.handleInput.bind(this, 'Password')}
                            value={Password}
                            errorStyle={styles.error}
                            errorMessage={validation.errors[errFields.Password]}
                        />
                        <Input
                            leftIcon={
                                <Icon
                                    name='lock'
                                    type='simple-line-icon'
                                    size={25}
                                    color='#fff'
                                />
                            }
                            placeholder='Confirm Password'
                            autoCapitalize='none'
                            keyboardAppearance='light'
                            secureTextEntry={true}
                            autoCorrect={false}
                            keyboardType='default'
                            returnKeyType='done'
                            blurOnSubmit
                            ref={input => (this.confirmPassword2Input = input)}
                            onChangeText={this.handleInput.bind(this, 'RePass')}
                            value={RePass}
                            errorStyle={styles.error}
                            errorMessage={validation.errors[errFields.RePass]}
                        />
                    </View>
                    <View
                        style={{
                            flex: 1,
                            marginTop: 20,
                            width: SCREEN_WIDTH - 70,
                            marginLeft: 40
                        }}
                    >
                        <Button
                            title='Create'
                            buttonStyle={{ backgroundColor: 'rgba(213, 100, 140, 1)' }}
                            onPress={this.handleCreate}
                        />
                    </View>
                </ThemeProvider>
            </ScrollView>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        backgroundColor: 'rgba(47,44,60,1)'
    },
    headerContainer: {
        justifyContent: 'center',
        alignItems: 'center',
        padding: 40,
        backgroundColor: 'rgba(47,44,60,1)'
    },
    heading: {
        color: 'white',
        marginTop: 10,
        fontSize: 22,
        fontWeight: 'bold'
    },
    contentView: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center'
    },
    triangleLeft: {
        position: 'absolute',
        left: -20,
        bottom: 0,
        width: 0,
        height: 0,
        borderRightWidth: 20,
        borderRightColor: 'white',
        borderBottomWidth: 25,
        borderBottomColor: 'transparent',
        borderTopWidth: 25,
        borderTopColor: 'transparent'
    },
    triangleRight: {
        position: 'absolute',
        right: -20,
        top: 0,
        width: 0,
        height: 0,
        borderLeftWidth: 20,
        borderLeftColor: 'white',
        borderBottomWidth: 25,
        borderBottomColor: 'transparent',
        borderTopWidth: 25,
        borderTopColor: 'transparent'
    },
    inputContainerStyle: {
        marginTop: 16,
        width: '90%'
    }
});

