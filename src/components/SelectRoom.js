/* eslint-disable react/sort-comp */
import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { ScrollView, StyleSheet, FlatList } from 'react-native';
import {
    ListItem,
    Icon
} from 'react-native-elements';

export default class ManageRoom extends Component {
    static propTypes = {
        actions    : PropTypes.object.isRequired,
        dispatch   : PropTypes.func.isRequired,
        room       : PropTypes.object.isRequired,
        navigation : PropTypes.object.isRequired
    };

    componentDidMount() {
        const { dispatch, actions } = this.props;

        dispatch(actions.showRooms());
    }

    componentWillUnmount() {
        const { dispatch, actions } = this.props;

        dispatch(actions.clear());
    }

    handleSelectRoom = RoomId => {
        const { dispatch, actions, navigation } = this.props;

        dispatch(actions.goToRoom(RoomId));
        navigation.navigate('MusicRoom');
    }

    _keyExtractor = (item, index) => `${index}`;

    renderListElem = ({ item }) => {
        return (
            <ListItem
                title={item.Name}
                rightIcon={
                    <Icon
                        name='arrow-right'
                        type='font-awesome'
                        size={20}
                        color={'#fff'}
                    />
                }
                onPress={this.handleSelectRoom.bind(null, item.Id)}
                containerStyle={{ backgroundColor: 'rgba(47,44,60,1)' }}
                titleStyle={{ color: '#fff' }}
            />
        );
    }

    static navigationOptions = {
        headerStyle : {
            backgroundColor: 'rgba(47,44,60,1)',
            borderBottomWidth: 0
        },
        headerTitle: 'Select room',
        headerTitleStyle: {
            flex: 1,
            textAlign: 'center',
            color: '#fff',
            fontSize: 20,
            fontWeight: 'normal'
        }
    };

    render() {
        const { room } = this.props;

        return (
            <ScrollView style={styles.container} keyboardShouldPersistTaps='handled'>
                <FlatList
                    data={room.data}
                    renderItem={this.renderListElem}
                    keyExtractor={this._keyExtractor}
                />
            </ScrollView>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        backgroundColor: 'rgba(47,44,60,1)'
    },
    headerContainer: {
        justifyContent: 'center',
        alignItems: 'center',
        padding: 40,
        backgroundColor: 'rgba(47,44,60,1)'
    },
    heading: {
        color: 'white',
        marginTop: 10,
        fontSize: 22,
        fontWeight: 'bold'
    },
    contentView: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center'
    },
    inputContainerStyle: {
        marginTop: 16,
        width: '90%'
    },
    checkbox : {
        backgroundColor: 'rgba(47,44,60,1)',
        borderWidth: 0
    }
});

