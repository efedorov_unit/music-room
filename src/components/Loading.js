import React from 'react';
import PropTypes from 'prop-types';
import { View, Text, ActivityIndicator, StyleSheet, AsyncStorage } from 'react-native';
import { StackActions, NavigationActions } from 'react-navigation';

export default class Loading extends React.Component {
    static propTypes = {
        navigation : PropTypes.object.isRequired,
        actions    : PropTypes.object.isRequired,
        dispatch   : PropTypes.func.isRequired
    }

    componentDidMount() {
        this.getUserData();
    }

    getUserData = async () => {
        const { navigation, dispatch, actions } = this.props;

        try {
            const value = await AsyncStorage.getItem('JWT');

            if (value) {
                await dispatch(actions.restoreFromJWT(value));

                const resetAction = StackActions.reset({
                    index: 0,
                    actions: [ NavigationActions.navigate({ routeName: 'Home' }) ]
                });

                navigation.dispatch(resetAction);

                return;
            }

            const resetAction = StackActions.reset({
                index: 0,
                actions: [ NavigationActions.navigate({ routeName: 'Login' }) ]
            });

            navigation.dispatch(resetAction);
        } catch (error) {
            console.error({ error });
        }
    }

    static navigationOptions = {
        header: null
    };

    render() {
        return (
            <View style={styles.container}>
                <Text>Loading</Text>
                <ActivityIndicator size='large' />
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container : {
        flex           : 1,
        alignItems     : 'center',
        justifyContent : 'center',
        backgroundColor: 'rgba(47,44,60,1)',
        color: '#ffffff'
    }
});
