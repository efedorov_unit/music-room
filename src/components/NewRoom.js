import React, { Component } from 'react';
import LIVR from 'livr';
import PropTypes from 'prop-types';
import { View, ScrollView, StyleSheet, Dimensions } from 'react-native';
import {
    Input,
    Icon,
    ThemeProvider,
    CheckBox,
    Button
} from 'react-native-elements';

const SCREEN_WIDTH = Dimensions.get('window').width;

export default class NewRoom extends Component {
    static propTypes = {
        navigation : PropTypes.object.isRequired,
        actions    : PropTypes.object.isRequired,
        dispatch   : PropTypes.func.isRequired
    };

    state = {
        Type: {
            public: true
        },
        Name: '',
        validation : {
            fields : {},
            errors : {
                REQUIRED : 'This field is required',
                TOO_SHORT : 'Must be at least 6 characters'
            }
        }
    }

    handleCheckbox = val => {
        this.setState({
            Type: {
                [val]: true
            }
        });
    }

    handleInputChange = name => {
        this.setState({ Name: name });
    }

    handleCreate = async () => {
        const { dispatch, actions, navigation } = this.props;

        let { Name, Type } = this.state; // eslint-disable-line

        Type = Object.keys(Type)[0];

        if (this.validate({ Name, Type })) {
            const res = await dispatch(actions.createRoom({ Name, Type }));

            res ? navigation.goBack() : null; // eslint-disable-line
        }
    }

    validate(data) {
        const validationRules = {
            Name : [ 'trim', 'required', { 'min_length': 6 } ],
            Type : [ 'required' ]
        };

        const validator = new LIVR.Validator(validationRules);
        const validateResult = validator.validate(data);

        if (!validateResult) {
            this.setState({
                validation : {
                    ...this.state.validation,
                    fields : validator.getErrors()
                }
            });
        }

        return validateResult;
    }

    static navigationOptions = {
        headerStyle : {
            backgroundColor: 'rgba(47,44,60,1)',
            borderBottomWidth: 0
        },
        headerTitle: 'Create new room',
        headerTitleStyle: {
            flex: 1,
            textAlign: 'center',
            color: '#fff',
            fontSize: 20,
            fontWeight: 'normal'
        }
    };

    render() {
        const { Type, Name, validation } = this.state;
        const errFields = { Name: '', ...validation.fields };

        return (
            <ScrollView style={styles.container} keyboardShouldPersistTaps='handled'>
                <ThemeProvider
                    theme={{
                        Input: {
                            containerStyle: {
                                width: SCREEN_WIDTH - 50
                            },
                            inputContainerStyle: {
                                borderRadius: 40,
                                borderWidth: 1,
                                borderColor: '#fff',
                                height: 50,
                                marginVertical: 10
                            },
                            placeholderTextColor: '#fff',
                            inputStyle: {
                                marginLeft: 10,
                                color: 'white'
                            },
                            keyboardAppearance: 'light',
                            blurOnSubmit: false
                        }
                    }}
                >
                    <View
                        style={{
                            backgroundColor: 'rgba(47,44,60,1)',
                            width: SCREEN_WIDTH,
                            alignItems: 'center'
                        }}
                    >
                        <Input
                            leftIcon={
                                <Icon
                                    name='user'
                                    type='simple-line-icon'
                                    size={25}
                                    color='#fff'
                                />
                            }
                            placeholder='Room name'
                            onChangeText={this.handleInputChange}
                            value={Name}
                            errorStyle={styles.error}
                            errorMessage={validation.errors[errFields.Name]}
                        />
                    </View>
                    <View
                        style={{
                            flex: 1,
                            flexDirection: 'row',
                            marginLeft: 20
                        }}
                    >
                        <CheckBox
                            title='Public'
                            checked={Type.public}
                            onPress={this.handleCheckbox.bind(null, 'public')}
                            containerStyle={styles.checkbox}
                            textStyle={{ color: '#fff' }}
                        />
                        <CheckBox
                            title='Private'
                            checked={Type.private}
                            onPress={this.handleCheckbox.bind(null, 'private')}
                            containerStyle={styles.checkbox}
                            textStyle={{ color: '#fff' }}
                        />
                    </View>
                    <View
                        style={{
                            flex: 1,
                            marginTop: 20,
                            width: SCREEN_WIDTH - 70,
                            marginLeft: 40
                        }}
                    >
                        <Button
                            title='Create'
                            buttonStyle={{ backgroundColor: 'rgba(213, 100, 140, 1)' }}
                            onPress={this.handleCreate}
                        />
                    </View>
                </ThemeProvider>
            </ScrollView>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        backgroundColor: 'rgba(47,44,60,1)'
    },
    headerContainer: {
        justifyContent: 'center',
        alignItems: 'center',
        padding: 40,
        backgroundColor: 'rgba(47,44,60,1)'
    },
    heading: {
        color: 'white',
        marginTop: 10,
        fontSize: 22,
        fontWeight: 'bold'
    },
    contentView: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center'
    },
    inputContainerStyle: {
        marginTop: 16,
        width: '90%'
    },
    checkbox : {
        backgroundColor: 'rgba(47,44,60,1)',
        borderWidth: 0
    }
});

