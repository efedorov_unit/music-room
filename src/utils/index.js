import { AsyncStorage } from 'react-native';
import jwtDecode from 'jwt-decode';

export async function saveJwt(dispatch, jwt) {
    const decoded = jwtDecode(jwt);

    await AsyncStorage.setItem('JWT', jwt);

    dispatch({
        type: 'SESSION_CREATE_SUCCESS',
        data: {
            jwt,
            ...decoded
        }
    });
}
